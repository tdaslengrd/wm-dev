﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using TDServicesApp.Models.AssetRegObjects;

namespace TDServicesApp.Models
{
    public class AddEditMachineModel
    {
        public int MachineID { get; set; }

        [Required]
        public string Machine_Name { get; set; }

        public string Comments { get; set; }
        public string HostName { get; set; }
        public string PowRail { get; set; }
        public string Rak { get; set; }
        public string DataCenterName { get; set; }
        public List<DataCenterList> DataCenters { get; set; }

        public List<HostList> HostMachines { get; set; }
        public List<PowerRailList> PowerRails { get; set; }
        public List<RackList> Racks { get; set; }
        public List<InterfaceList> InterfaceNames { get; set; }
        public Nullable<double> VlanID { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual Datacenter Datacenter { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Interface> Interfaces { get; set; }
        public virtual Support Support1 { get; set; }
        public virtual PowerRail PowerRail1 { get; set; }
        public virtual Rack Rack1 { get; set; }

        public Boolean IsNew { get; set; } = true;
        public Nullable<Boolean> IsVirtualServer { get; set; }
        public Nullable<Boolean> IsVirtualMachine { get; set; }

        public string SelectedIp { get; set; }
    }
    public class AddInterfaceViewModel
    {
        public int MachineID { get; set; }

        [Required]
        public string InterfaceName { get; set; }

        public List<SubnetTypeList> SubnetTypes { get; set; }

        [Required(ErrorMessage = "Please select a subnet type from the dropdownlist")]
        public string SelectedSubnetType { get; set; }

        [Required(ErrorMessage = "Please select an IP subnet from the dropdownlist")]
        public string IpLists { get; set; }

        [Required(ErrorMessage = "Please select an IP from the dropdownlist")]
        public string UnMappedIPs { get; set; }

        public string MappedIP { get; set; }

        public AddInterfaceViewModel()
        {
            SubnetTypes = new List<SubnetTypeList>();
        }
    }
    public class AddIPViewModel
    {
        public int InterfaceID { get; set; }
        public List<SubnetTypeList> SubnetTypes { get; set; }

        [Required(ErrorMessage = "Please select a subnet type from the dropdownlist")]
        public string SelectedSubnetType { get; set; }

        [Required(ErrorMessage = "Please select an IP subnet from the dropdownlist")]
        public string IpLists { get; set; }

        [Required(ErrorMessage = "Please select an IP from the dropdownlist")]
        public string UnMappedIPs { get; set; }

        public string MappedIP { get; set; }

        public AddIPViewModel()
        {
            SubnetTypes = new List<SubnetTypeList>();
        }
    }
    public class AddNewCustomerModel
    {
        [Required]
        public string CustomerName { get; set; }

        [Required]
        [StringLength(5, ErrorMessage = "The {0} must be 4 or 5 digit numbers.", MinimumLength = 4)]
        public string CustomerNumber { get; set; }

        public string UserName { get; set; }

        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

    }
    public class AddServerViewModel
    {
        public int MachineID { get; set; }

        [Required]
        public string MachineName { get; set; }

        [Required]
        public string CustomerNumber { get; set; }
        public string LoginUser { get; set; }
        public string LoginDomain { get; set; }

        [Required(ErrorMessage = "A serial number is required for this asset")]
        public string SerialNumber { get; set; }
        public string Comments { get; set; }

        [Required(ErrorMessage = "Please select one power rail from the list")]
        public string PowRail { get; set; }

        [Required(ErrorMessage = "Please select one rack from the list")]
        public string Rak { get; set; }

        [Required(ErrorMessage = "Please select one data center from the list")]
        public string DataCenterName { get; set; }
        public List<DataCenterList> DataCenters { get; set; }
        public List<PowerRailList> PowerRails { get; set; }
        public List<RackList> Racks { get; set; }

        //public List<InterfaceList> InterfaceNames { get; set; }
        public Nullable<Boolean> IsVirtualMachine { get; set; } = false;

    }
    public class AddUserViewModel
    {
        public string CustomerNumber { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        [StringLength(10, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }
    public class AddVirtualViewModel
    {
        public int MachineID { get; set; }

        [Required]
        public string MachineName { get; set; }

        [Required]
        public string CustomerNumber { get; set; }
        public string LoginUser { get; set; }
        public string LoginDomain { get; set; }

        //[Required]
        //public string SerialNumber { get; set; }
        public string Comments { get; set; }

        [Required(ErrorMessage = "Please select one host from the list")]
        public string HostName { get; set; }

        public List<HostList> HostMachines { get; set; }
        public Nullable<double> VlanID { get; set; }
        public Nullable<Boolean> IsVirtualMachine { get; set; } = true;
    }
}