#!/bin/bash

### NetflowBulkExporter Shell Script v2 ###

echo "NetflowBulkExporter Shell Script v2"

####### NOTES #######
#This script will run once per day, for the previous day results.
#i.e. Set a cron job to run each day at 11pm, this will run the import job for the previous day, between 00:00:00 to 23:59:59.
#####################

#Script variables, dont change unless you know what your doing
LOCALENVPATH=/usr/local/bin
ENVPATH=/usr/bin
SYSENVPATH=/bin

STARTDAY=1
MONTH=08
YEAR=2017
DAYSINMONTH=`cal $(date +${MONTH}) | awk 'NF {DAYS = $NF}; END {print DAYS}'`
CURRENTDATETIME=`date +"%Y-%m-%d %H:%M:%S"`

RECORDCOUNT="10000"
SERVERNAME="1000Flanders01"
TRAFFICDIRECTION=( 'in,inweb' 'out,outweb' )

FTP_HOST="10.100.0.249"
FTP_USER="netflowftpuser"
FTP_PASSWD="Passw0rd"

#Get all the subnets that SiLK is configured to sense
SUBNETS_STRING=`egrep '^[[:space:]]*ipblocks' /data/sensor.conf | awk '{ printf $2 "\," }'`
SUBNETS_STRING=${SUBNETS_STRING%?}
IFS=',' read -a SUBNETS <<<"$SUBNETS_STRING"
echo "Found the following subnets:"
for i in "${SUBNETS[@]}"
do
	echo $i
done

#Loop through each day in the given month
for (( DAY=$STARTDAY; DAY<=$DAYSINMONTH; DAY++ ))
do
	#This will format the day correctly if the looped variable is 1 character long
	size=${#DAY}
	if [ $size -eq 1 ];
	then
		DAY="0${DAY}"
	fi

	YESTERDAYDATETIME=${YEAR}-${MONTH}-${DAY}

	echo "Starting job ${CURRENTDATETIME}"

${ENVPATH}/ftp -n $FTP_HOST << END_SCRIPT
quote USER $FTP_USER
quote PASS $FTP_PASSWD
mkdir $YESTERDAYDATETIME
quit
END_SCRIPT

	for DIRECTION in "${TRAFFICDIRECTION[@]}"
	do
		for SUBNET in "${SUBNETS[@]}"
		do
			echo "Processing ${DIRECTION} data for ${YEAR}/${MONTH}/${DAY} on ${SERVERNAME} for the ${SUBNET} subnet."

			#Start and end hour to form STARTDATE and ENDDATE variable for rwfilter
		        START=00
		        END=23

		        #Main loop to get data from Netflow to MySQL
		        for (( c=$START; c<=$END; c++ ))
		        do
				#Start and end dates for rwfilter
				STARTDATE="${YEAR}/${MONTH}/${DAY}:${c}"
		                ENDDATE="${YEAR}/${MONTH}/${DAY}:${c}"

				echo "Working on hour ${c}"

				#Get last hours transit information and save to a temp file
                                if [ "$DIRECTION" = "in,inweb" ]
                                then
					FILENAME_DIRECTION="in"
					TEMPFILE="netflow_${FILENAME_DIRECTION}_${DAY}_${MONTH}_${YEAR}.csv"
	                                REPORTPATH="/data/tmp/$TEMPFILE"
	                                touch $REPORTPATH

					${LOCALENVPATH}/rwfilter --type=${DIRECTION} --proto=0- --pass=stdout --daddress=${SUBNET} --start-date=$STARTDATE | rwstats --count=$RECORDCOUNT --fields=dIP,dPort,sTime,eTime --values=bytes --temp-directory=/data/tmp --no-titles --no-columns --no-percents --no-final-delimiter --column-separator=',' --output-path=$REPORTPATH

					FTP_TEMPFILE2="netflow_${FILENAME_DIRECTION}_${DAY}_${MONTH}_${YEAR}_${c}_ftp2_${SERVERNAME}.csv"
	                                FTP_TEMPPATH2="/data/tmp/$FTP_TEMPFILE2"
	                                touch $FTP_TEMPPATH2

					string="dIP,dPort,sTime,eTime,bytes"; ${SYSENVPATH}/awk -v n=1 -v s="$string" 'NR == n {print s} {print}' $REPORTPATH > $FTP_TEMPPATH2;
				elif [ "$DIRECTION" = "out,outweb" ]
				then
					FILENAME_DIRECTION="out"
					TEMPFILE="netflow_${FILENAME_DIRECTION}_${DAY}_${MONTH}_${YEAR}.csv"
	                                REPORTPATH="/data/tmp/$TEMPFILE"
	                                touch $REPORTPATH

					${LOCALENVPATH}/rwfilter --type=${DIRECTION} --proto=0- --pass=stdout --saddress=${SUBNET} --start-date=$STARTDATE | rwstats --count=$RECORDCOUNT --fields=sIP,sPort,sTime,eTime --values=bytes --temp-directory=/data/tmp --no-titles --no-columns --no-percents --no-final-delimiter --column-separator=',' --output-path=$REPORTPATH

					FTP_TEMPFILE2="netflow_${FILENAME_DIRECTION}_${DAY}_${MONTH}_${YEAR}_${c}_ftp2_${SERVERNAME}.csv"
                                        FTP_TEMPPATH2="/data/tmp/$FTP_TEMPFILE2"
                                        touch $FTP_TEMPPATH2

					string="sIP,sPort,sTime,eTime,bytes"; ${SYSENVPATH}/awk -v n=1 -v s="$string" 'NR == n {print s} {print}' $REPORTPATH > $FTP_TEMPPATH2;
                                else
                                        echo "Direction is not set correctly, exiting."
                                        exit 1
                                fi

				#Export to FTP, requires remote processing.
				FILE=$FTP_TEMPPATH2

${ENVPATH}/ftp -n $FTP_HOST << END_SCRIPT
quote USER $FTP_USER
quote PASS $FTP_PASSWD
cd $YESTERDAYDATETIME
binary
put $FILE $FTP_TEMPFILE2
quit
END_SCRIPT

				#Remove any temp files generated above
				${SYSENVPATH}/rm -f /data/tmp/netflow_*
			done
		done
	done
done
FINISHTIME=`date +"%Y-%m-%d %H:%M:%S"`
echo "Finished job ${FINISHTIME}"
