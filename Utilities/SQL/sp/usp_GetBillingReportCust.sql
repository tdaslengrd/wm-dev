USE [NetflowResultsData]
GO

/****** Object:  StoredProcedure [dbo].[usp_GetBillingReportCust]    Script Date: 12/12/2017 8:07:25 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GetBillingReportCust] @Month nvarchar(30), @CustomerName nvarchar(100)
AS
DECLARE @cmd_genReport nvarchar(max)
SET @cmd_genReport = '
SELECT A.IPAddress, C.MachineID, C.[Machine Name], D.CustomerNumber, D.[Customer Name], c.contract_num_sap, E.inbytes, E.outbytes,
CONVERT(datetime, SWITCHOFFSET(CONVERT(datetimeoffset, E.dt), DATENAME(TzOffset, SYSDATETIMEOFFSET()))) AS dt
FROM assetregisterSQL.dbo.IPAddressesNew A
inner join assetregisterSQL.dbo.Interfaces B on A.InterfaceID = B.InterfaceID
inner join assetregisterSQL.dbo.Machines C on C.MachineID = B.MachineID
inner join assetregisterSQL.dbo.Customers D on D.CustomerNumber = C.CustomerNumber
inner join NetflowResultsData.dbo.' + @Month + ' E on A.IPAddress = E.ipaddr and D.[Customer Name] = ''' + @CustomerName + '''
ORDER BY E.dt ASC'
EXEC sp_executesql @cmd_genReport
GO

