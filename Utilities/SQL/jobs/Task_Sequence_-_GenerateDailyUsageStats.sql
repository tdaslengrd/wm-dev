--SQL Job to automatically generate per day usage stats and store within a table

--Start the main process--
BEGIN
	BEGIN TRY
		-- Declare the variable to be used.--
		DECLARE @t1 DATETIME;
		DECLARE @t2 DATETIME;
		DECLARE @tdiff varchar(100);
		DECLARE @createTable nvarchar(max)
		DECLARE @removeTable nvarchar(max)
		DECLARE @monthName nvarchar(30)
		DECLARE @cmd_genReport nvarchar(max)
		declare @Year nvarchar(max)
		DECLARE @Table_Name nvarchar(max)
		
		SET @t1 = GETDATE();

		SET @monthName = DATENAME(month, @t1)
		SET @year = DATENAME(year, @t1)
		SET @Table_Name = @monthName + '_' + @year;
		
		SET @cmd_genReport = '
		SELECT DISTINCT (FORMAT(DailyUsage.dt, ''yyyy-MM-dd'')) AS dt, ROUND(CAST(SUM(DailyUsage.inbytes) AS Float)/1073741824,2) AS total_inGBytes, ROUND(CAST(SUM(DailyUsage.outbytes) AS Float)/1073741824,2) AS total_outGBytes
		FROM
			(SELECT	DISTINCT (CAST(CONVERT(datetime, SWITCHOFFSET(CONVERT(datetimeoffset, dt), DATENAME(TzOffset, SYSDATETIMEOFFSET()))) AS DATE)) AS dt, inbytes, outbytes
			FROM NetflowResultsData.dbo.' + @Table_Name + '
			GROUP BY dt, inbytes, outbytes) DailyUsage
		GROUP BY dt;'
		
		SET @removeTable = 'IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[dbo].[qs_TotalUsagePerDay]'') AND type in (N''U''))
		BEGIN
			DROP TABLE qs_TotalUsagePerDay;
		END'
		EXEC (@removeTable)

		SET @createTable = 'IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[dbo].[qs_TotalUsagePerDay]'') AND type in (N''U''))
		BEGIN
			CREATE TABLE [NetflowResultsData].[dbo].[qs_TotalUsagePerDay] (Id INT NOT NULL PRIMARY KEY IDENTITY(1,1), dt DateTime, total_inGBytes float, total_outGBytes float);
		END'
		EXEC (@createTable)

		--EXEC sp_executesql @cmd_genReport
		INSERT INTO qs_TotalUsagePerDay EXEC (@cmd_genReport);

		SET @t2 = GETDATE();
		SET @tdiff = DATEDIFF(MINUTE,@t1,@t2);
		PRINT 'Time elapsed - ' + @tdiff + ' minutes.';
	END TRY
	BEGIN CATCH  
		SELECT   
			ERROR_NUMBER() AS ErrorNumber  
			,ERROR_MESSAGE() AS ErrorMessage;
			DROP TABLE qs_TotalUsagePerDay;
	END CATCH
END;