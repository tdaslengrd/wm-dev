USE [NetflowResultsData]
GO

/****** Object:  StoredProcedure [dbo].[usp_GetTotalUsagePerDay]    Script Date: 12/12/2017 8:06:26 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GetTotalUsagePerDay] @Month nvarchar(30)
AS
DECLARE @cmd_genReport nvarchar(max)
SET @cmd_genReport = '
SELECT DISTINCT (FORMAT(DailyUsage.dt, ''yyyy-MM-dd'')) AS dt, ROUND(CAST(SUM(DailyUsage.inbytes) AS Float)/1073741824,2) AS total_inGBytes, ROUND(CAST(SUM(DailyUsage.outbytes) AS Float)/1073741824,2) AS total_outGBytes
FROM
	(SELECT	DISTINCT (CAST(CONVERT(datetime, SWITCHOFFSET(CONVERT(datetimeoffset, dt), DATENAME(TzOffset, SYSDATETIMEOFFSET()))) AS DATE)) AS dt, inbytes,	outbytes
	FROM NetflowResultsData.dbo.' + @Month + '
	GROUP BY dt, inbytes, outbytes) DailyUsage
GROUP BY dt;'
EXEC sp_executesql @cmd_genReport
GO

