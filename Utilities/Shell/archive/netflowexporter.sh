#!/bin/bash

### NetflowExporter Shell Script v1.0 ###

echo "NetflowExporter Shell Script v1.0"

####### NOTES #######
#This script will run once per day, for the previous day results.
#i.e. Set a cron job to run each day at 11pm, this will run the import job for the previous day, between 00:00:00 to 23:59:59.
#####################

#Script variables, dont change unless you know what your doing
LOCALENVPATH=/usr/local/bin
ENVPATH=/usr/bin
SYSENVPATH=/bin
DAY=`date -d '-1 day' '+%d'` #This gets yesterdays day
MONTH=`date +"%m"`
YEAR=`date +"%Y"`
CURRENTDATETIME=`date +"%Y-%m-%d %H:%M:%S"`
YESTERDAYDATETIME=`date -d '-1 day' +"%Y-%m-%d %H:%M:%S"`
RECORDCOUNT="1000000"

MYSQLORFTP=FTP
echo "${MYSQLORFTP} export enabled."

MYSQL_USER="netflowshelluser"
MYSQL_PASSWORD="Passw0rd"
MYSQL_SERVER="10.100.0.59"
SQLDBNAME="netflow"

FTP_HOST="10.100.0.249"
FTP_USER="netflowftpuser"
FTP_PASSWD="Passw0rd"

echo "Starting job ${CURRENTDATETIME}"

#We will process both inbound and outbound results
array=( "in" "out" )
for element in ${array[@]}
do
	echo "Processing ${element}bound traffic."

	TRAFFICDIRECTION=$element

	if [ ${MYSQLORFTP} == 'MYSQL' ];
        then
		SQLDBNAME="netflownew"
		SQLTABLENAME="${TRAFFICDIRECTION}_${DAY}_${MONTH}_${YEAR}"

		CHECKSQLTABLE="select count(*) from information_schema.tables where table_schema='${SQLDBNAME}' and table_name='${SQLTABLENAME}';"
		CREATESQLTABLE="USE $SQLDBNAME; \
				CREATE TABLE $SQLTABLENAME( \
				id INT NOT NULL AUTO_INCREMENT, \
				src VARCHAR(40) NOT NULL, \
				dest VARCHAR(40) NOT NULL, \
				bytes VARCHAR(40) NOT NULL, \
				bytespercent VARCHAR(40) NOT NULL, \
				cumulativepercent VARCHAR(40) NOT NULL, \
				datetime DATETIME NOT NULL, \
				PRIMARY KEY ( id ) \
				);"

		#Check if the table exists in the database, if not lets create it
		if [ $(${ENVPATH}/mysql -N -s -h$MYSQL_SERVER -u$MYSQL_USER -p$MYSQL_PASSWORD -e"$CHECKSQLTABLE") -eq 1 ];
		then
			echo "Table ${SQLTABLENAME} alread exists.";
		else
			echo "Table ${SQLTABLENAME} does not exist! Creating..."
			${ENVPATH}/mysql -h$MYSQL_SERVER -u$MYSQL_USER -p$MYSQL_PASSWORD -e"$CREATESQLTABLE" > /tmp/error1
		fi
	else
${ENVPATH}/ftp -n $FTP_HOST << END_SCRIPT
quote USER $FTP_USER
quote PASS $FTP_PASSWD
mkdir $YESTERDAYDATETIME
quit
END_SCRIPT
        fi

	#Start and end hour to form STARTDATE and ENDDATE variable for rwfilter
	START=00
	END=23

	#Main loop to get data from Netflow to MySQL
	for (( c=$START; c<=$END; c++ ))
		do
                        #Start and end dates for rwfilter
                        STARTDATE="${YEAR}/${MONTH}/${DAY}:${c}"
                        if [ ${c} == 23 ];
                        then
                                break
                        else
                                i=0
                                i=$((c+1))
                        fi
                        ENDDATE="${YEAR}/${MONTH}/${DAY}:${i}"

                        echo "Processing data between ${c} and ${i} for ${YEAR}/${MONTH}/${DAY}"

			TEMPFILE="netflow_${TRAFFICDIRECTION}_${DAY}_${MONTH}_${YEAR}_${c}.csv"
			REPORTPATH="/data/tmp/$TEMPFILE"
			touch $REPORTPATH

			#Get last hours transit information and save to a temp file
			${LOCALENVPATH}/rwfilter --type=$TRAFFICDIRECTION --proto=0- --pass=stdout --start-date=$STARTDATE --end-date=$ENDDATE | ${LOCALENVPATH}/rwstats --bytes --count=$RECORDCOUNT --sip --dip --no-titles --no-columns --no-final-delimiter --column-separator=',' --output-path=$REPORTPATH

			if [ ${MYSQLORFTP} == 'MYSQL' ];
		        then
				#Get the temp file read it as a CSV file and send it to MySQL
				IFS=,

				while read column1 column2 column3 column4 column5
				  do
					echo "INSERT INTO $SQLTABLENAME (src,dest,bytes,bytespercent,cumulativepercent,datetime) VALUES ('$column1', '$column2', '$column3', '$column4', '$column5', '$YESTERDAYDATETIME');"
				done < $REPORTPATH | ${ENVPATH}/mysql -h$MYSQL_SERVER -u$MYSQL_USER -p$MYSQL_PASSWORD $SQLDBNAME;
		        else
				FTP_TEMPFILE="netflow_${TRAFFICDIRECTION}_${DAY}_${MONTH}_${YEAR}_${c}_ftp.csv"
				FTP_TEMPPATH="/data/tmp/$FTP_TEMPFILE"				
				touch $FTP_TEMPPATH

				${SYSENVPATH}/sed "s/$/,${YESTERDAYDATETIME}/" $REPORTPATH > $FTP_TEMPPATH

				FTP_TEMPFILE2="netflow_${TRAFFICDIRECTION}_${DAY}_${MONTH}_${YEAR}_${c}_ftp2_flanders01.csv"
                                FTP_TEMPPATH2="/data/tmp/$FTP_TEMPFILE2"
                                touch $FTP_TEMPPATH2

				string="src,dest,bytes,bytespercent,cumulativepercent,datetime"; ${SYSENVPATH}/awk -v n=1 -v s="$string" 'NR == n {print s} {print}' $FTP_TEMPPATH > $FTP_TEMPPATH2;

				#Export to FTP, requires remote processing.
				FILE=$FTP_TEMPPATH2

${ENVPATH}/ftp -n $FTP_HOST << END_SCRIPT
quote USER $FTP_USER
quote PASS $FTP_PASSWD
cd $YESTERDAYDATETIME
binary
put $FILE $FTP_TEMPFILE2
quit
END_SCRIPT
			fi
	done
	echo "Finished ${TRAFFICDIRECTION}bound records."
done

#Remove any temp files generated above
${SYSENVPATH}/rm -f /data/tmp/netflow_*

FINISHTIME=`date +"%Y-%m-%d %H:%M:%S"`
echo "Finished job ${FINISHTIME}"
