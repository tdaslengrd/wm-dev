﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TDServicesApp.Models.AssetRegObjects
{
    public class _Machine
    {
        public string MachineName { get; set; }
        public string MachineID { get; set; }
        public List<_Interface> InterfaceNames { get; set; }
    }
}