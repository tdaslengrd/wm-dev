﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System;
using TDServicesApp.Models.AssetRegObjects;

namespace TDServicesApp.Models
{
    public class DeleteCustomerModel
    {
        public string CustomerNumber { get; set; }
        public string CustomerName { get; set; }
    }
    public class DeleteInterfaceViewModel
    {
        public int InterfaceID { get; set; }
        public string InterfaceName { get; set; }
        public Nullable<int> MachineID { get; set; }
        public List<IPAddressList> IpAddresses { get; set; }
    }
    public class DeleteIpViewModel
    {
        public string IPAddress { get; set; }
    }
    public class DeleteMachineModel
    {
        public int MachineID { get; set; }
        public string MachineName { get; set; }
        public string Type { get; set; }
        public string Cage { get; set; }

        public DeleteMachineModel() { }
    }

}