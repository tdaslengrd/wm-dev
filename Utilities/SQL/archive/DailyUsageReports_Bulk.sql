--PRIOR TO RUNNING THIS SCRIPT, MAKE SURE MAXDOP IS SET CORRECTLY
--FAILING TO DO SO WILL CAUSE THREAD EXECUTION EXHAUSTION WHICH WILL CAUSE THE SCRIPT TO HALT!

--Before running this script, set Line 32 and 34;
--Line 37 is the date from when you want to start parsing records from
--Line 40 controls how many days to go in reverse from the date set on Line 32
--For example;
--If you set line 32 to 2017-08-01 and line 34 to 31, this will parse all data for the month of July.
--Line 39 is the destination table to write out to.

--Start the main process--
BEGIN
	BEGIN TRY
		-- Declare the variable to be used.--
		DECLARE @currentDate varchar(20);
		DECLARE @yesterdayDate varchar(20);
		DECLARE @ipaddr varchar(50);
		DECLARE @dateTime datetime;
		DECLARE @cmd_createTable varchar(255);
		DECLARE @cmd_fillOutTable nvarchar(max);
		DECLARE @cmd_insertTable nvarchar(max);
		DECLARE @site_value INT;
		DECLARE @t1 DATETIME;
		DECLARE @t2 DATETIME;
		DECLARE @tdiff varchar(100);
		DECLARE @static_time datetime;
		DECLARE @static_time_yesterday varchar(10);
		DECLARE @time_convert datetime;
		DECLARE @yyyyMM_TableName nvarchar(50);
		DECLARE @createTable nvarchar(max);
		DECLARE @cmd_something nvarchar(max);
		DECLARE @monthendDay nvarchar(50);

		SET @site_value = 0;
		SET @time_convert = (SELECT CONVERT(date, '2017-08-31', 126));
		SET @monthendDay = ((SELECT DAY(EOMONTH(@time_convert)))-1);

		--Create the destination table to store the processed results--
		SET @yyyyMM_TableName = (SELECT DATENAME(month,@time_convert) + '_' + DATENAME(year,GETDATE()));
		SET @createTable = 'IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[dbo].['+@yyyyMM_TableName+']'') AND type in (N''U''))
		BEGIN
			CREATE TABLE [NetflowResultsData].[dbo].['+@yyyyMM_TableName+'] (Id INT NOT NULL PRIMARY KEY IDENTITY(1,1),ipaddr varchar(50),inbytes bigint,outbytes bigint,dt DATETIME);
		END'
		EXEC (@createTable)

		WHILE @site_value <= @monthendDay
			BEGIN
			SET @t1 = GETDATE();

			SET @static_time = (SELECT CONVERT(date,@time_convert-@site_value));
			SET @static_time_yesterday = (SELECT CONVERT(VARCHAR(8), @static_time, 112));
			SET @currentDate = (SELECT CONVERT(date, @time_convert, 120));

			--The resolution of reporting is daily, as such we will take the opportunity to set the dateTime variable to a static figure for the rest of the script.
			SET @dateTime = @static_time;
			SET @yesterdayDate = 'nfd' + @static_time_yesterday;

			PRINT 'Processing ' + @yesterdayDate + ' data.';

			CREATE TABLE #NetflowTable_tmp(ID int IDENTITY(1,1) NOT NULL, ipaddr nvarchar(50), inbytes bigint, outbytes bigint, dt nvarchar(50), UNIQUE NONCLUSTERED (ipaddr, ID));

			SET @cmd_fillOutTable = '
									SELECT
									CASE
										WHEN InOutData.outipaddr IS NULL
											THEN InOutData.inipaddr
											ELSE InOutData.outipaddr
										END
									AS ipaddr,
									ISNULL(InOutData.outbytes, 0) AS outbytes,
									ISNULL(InOutData.inbytes, 0) AS inbytes,
									CASE
										WHEN InOutData.outdt IS NULL
											THEN InOutData.indt
											ELSE InOutData.outdt
										END
									AS dt
									FROM
										(SELECT sIPdata2.outipaddr as outipaddr, dIPdata2.inipaddr AS inipaddr, sIPdata2.outbytes AS outbytes, dIPdata2.inbytes AS inbytes, sIPdata2.outdt AS outdt, dIPdata2.indt AS indt
										FROM
											(SELECT distinct(dIPdata.ipaddr) AS inipaddr, sum(dIPdata.inbytes) AS inbytes, dIPdata.dt AS indt
											FROM
												(SELECT DISTINCT sTime AS dt, dIP AS ipaddr, SUM(bytes) AS inbytes
													FROM [NetflowData].[dbo].[' + @yesterdayDate + ']
													WHERE dIP LIKE ''202.191.[48-55]%'' OR dIP LIKE ''119.161.[32-47]%''
													GROUP BY dIP, sTime) dIPdata
												GROUP BY ipaddr, dt) dIPdata2
											full join
											(SELECT distinct(sIPdata.ipaddr) AS outipaddr, sum(sIPdata.outbytes) AS outbytes, sIPdata.dt AS outdt
											FROM
												(SELECT DISTINCT sTime AS dt, sIP AS ipaddr, SUM(bytes) AS outbytes
												FROM [NetflowData].[dbo].[' + @yesterdayDate + ']
													WHERE sIP LIKE ''202.191.[48-55]%'' OR sIP LIKE ''119.161.[32-47]%''
													GROUP BY sIP, sTime) sIPdata
												GROUP BY ipaddr, dt) sIPdata2
											ON sIPdata2.outipaddr = dIPdata2.inipaddr AND sIPdata2.outdt = dIPdata2.indt) InOutData
										GROUP BY inipaddr, outipaddr, inbytes, outbytes, outdt, indt
									ORDER BY dt';

			INSERT INTO #NetflowTable_tmp EXEC (@cmd_fillOutTable);

			SET @cmd_something = 'INSERT INTO [NetflowResultsData].[dbo].[' + @yyyyMM_TableName + '] (ipaddr, inbytes, outbytes, dt)
			SELECT T.ipaddr, T.inbytes, T.outbytes, (CONVERT(DATETIME, T.dt, 120)) AS dt
			FROM #NetflowTable_tmp T;';

			EXEC sp_executesql @cmd_something;

			DROP TABLE #NetflowTable_tmp;

			SET @t2 = GETDATE();
			SET @tdiff = DATEDIFF(MINUTE,@t1,@t2);
			PRINT 'Time elapsed - ' + @tdiff + ' minutes.';

			SET @site_value = @site_value + 1;
		END
	END TRY
	BEGIN CATCH  
		SELECT   
			ERROR_NUMBER() AS ErrorNumber  
			,ERROR_MESSAGE() AS ErrorMessage;
			DROP TABLE #NetflowTable_tmp;
	END CATCH
END;