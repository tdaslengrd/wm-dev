USE [VeeamUsageData]
GO

/****** Object:  StoredProcedure [dbo].[usp_GetVeeamBadJobs]    Script Date: 12/12/2017 8:22:58 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GetVeeamBadJobs] @tableName nvarchar(30)
AS
DECLARE @cmd_genReport nvarchar(max)
SET @cmd_genReport = '
SELECT COUNT(DISTINCT jobName) AS ''Veeam Jobs with no endpoints'' FROM VeeamUsageData.dbo.' + @tableName + ' WHERE vms = '''';'
EXEC sp_executesql @cmd_genReport
GO

