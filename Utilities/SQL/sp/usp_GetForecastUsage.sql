USE [NetflowResultsData]
GO

/****** Object:  StoredProcedure [dbo].[usp_GetForecastUsage]    Script Date: 20/10/2017 10:38:25 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_GetForecastUsage] @Month nvarchar(30)
AS
--DECLARE @Month_Name nvarchar(30);
DECLARE @Year nvarchar(30);
DECLARE @Table_Name nvarchar(30);
DECLARE @queryCmd nvarchar(max);
DECLARE @DaysInMonth bigint
--SET @Month_Name = DATENAME(month, GETDATE())
SET @Year = YEAR(GETDATE())
SET @Table_Name = @Month + '_' + @Year
SET @DaysInMonth = (DAY(EOMONTH(GETDATE())))
SET @queryCmd = '
SELECT AVG(AverageUsage.total_inGBytes) AS averageDaily_inGBytes, AVG(AverageUsage.total_outGBytes) AS averageDaily_outGBytes, (AVG(AverageUsage.total_inGBytes) + AVG(AverageUsage.total_outGBytes))*' + CAST(@DaysinMonth AS nvarchar(30)) + ' AS MonthForecast
FROM
(SELECT DISTINCT DailyUsage.dt, ROUND(CAST(SUM(DailyUsage.inbytes) AS Float)/1073741824,2) AS total_inGBytes, ROUND(CAST(SUM(DailyUsage.outbytes) AS Float)/1073741824,2) AS total_outGBytes
FROM
	(SELECT	DISTINCT (FORMAT(dt, ''yyyy-MM-dd'')) AS dt, inbytes,	outbytes
	FROM NetflowResultsData.dbo.' + @Table_Name + '
	GROUP BY dt, inbytes, outbytes) DailyUsage
GROUP BY dt) AverageUsage;'
EXEC sp_executesql @queryCmd;
GO

