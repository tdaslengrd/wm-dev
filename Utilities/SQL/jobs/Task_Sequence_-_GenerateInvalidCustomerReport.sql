--SQL Job to automatically generate report on customer who dont have a SAP control number and store within a table

--Start the main process--
BEGIN
	BEGIN TRY
		-- Declare the variable to be used.--
		DECLARE @t1 DATETIME;
		DECLARE @t2 DATETIME;
		DECLARE @tdiff varchar(100);
		DECLARE @createTable nvarchar(max)
		DECLARE @removeTable nvarchar(max)
		DECLARE @monthName nvarchar(30)
		DECLARE @cmd_genReport nvarchar(max)
		declare @Year nvarchar(max)
		DECLARE @Table_Name nvarchar(max)
		
		SET @t1 = GETDATE();

		SET @monthName = DATENAME(month, @t1)
		SET @year = DATENAME(year, @t1)
		SET @Table_Name = @monthName + '_' + @year;
		
		SET @cmd_genReport = '
		SELECT DISTINCT D.CustomerNumber, D.[Customer Name], C.[Machine Name], ROUND(CAST(SUM(E.inbytes) AS Float)/1073741824,5) AS total_inGBytes, ROUND(CAST(SUM(E.outbytes) AS Float)/1073741824,5) AS total_outGBytes, (ROUND(CAST(SUM(E.inbytes) AS Float)/1073741824,5) + ROUND(CAST(SUM(E.outbytes) AS Float)/1073741824,5)) AS total_GBytes
		FROM assetregisterSQL.dbo.IPAddressesNew A
		inner join assetregisterSQL.dbo.Interfaces B on A.InterfaceID = B.InterfaceID
		inner join assetregisterSQL.dbo.Machines C on C.MachineID = B.MachineID
		inner join assetregisterSQL.dbo.Customers D on D.CustomerNumber = C.CustomerNumber
		inner join NetflowResultsData.dbo.' + @Table_Name + ' E on A.IPAddress = E.ipaddr AND C.contract_num_sap IS NULL AND D.[Customer Name] NOT LIKE ''ICO%''
		GROUP BY D.CustomerNumber, D.[Customer Name], C.[Machine Name]
		ORDER BY total_inGBytes DESC;'
		
		SET @removeTable = 'IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[dbo].[qs_InvalidCust]'') AND type in (N''U''))
		BEGIN
			DROP TABLE qs_InvalidCust;
		END'
		EXEC (@removeTable)

		SET @createTable = 'IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[dbo].[qs_InvalidCust]'') AND type in (N''U''))
		BEGIN
			CREATE TABLE [NetflowResultsData].[dbo].[qs_InvalidCust] (Id INT NOT NULL PRIMARY KEY IDENTITY(1,1), CustomerNumber nvarchar(10), [Customer Name] nvarchar(max), [Machine Name] nvarchar(max), total_inGBytes bigint, total_outGBytes bigint, total_GBytes bigint);
		END'
		EXEC (@createTable)

		--EXEC sp_executesql @cmd_genReport
		INSERT INTO qs_InvalidCust EXEC (@cmd_genReport);

		SET @t2 = GETDATE();
		SET @tdiff = DATEDIFF(MINUTE,@t1,@t2);
		PRINT 'Time elapsed - ' + @tdiff + ' minutes.';
	END TRY
	BEGIN CATCH  
		SELECT   
			ERROR_NUMBER() AS ErrorNumber  
			,ERROR_MESSAGE() AS ErrorMessage;
			DROP TABLE qs_InvalidCust;
	END CATCH
END;