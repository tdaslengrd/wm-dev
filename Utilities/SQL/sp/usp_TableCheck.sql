USE [NetflowResultsData]
GO

/****** Object:  StoredProcedure [dbo].[usp_TableCheck]    Script Date: 20/10/2017 10:39:51 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_TableCheck] @tableName nvarchar(30)
AS
DECLARE @cmd_execCmd nvarchar(max)
SET @cmd_execCmd = '
IF EXISTS (SELECT 1 
           FROM NetflowResultsData.INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_TYPE=''BASE TABLE'' 
           AND TABLE_NAME=''' + @tableName + ''') 
   SELECT ''Yes'' AS IsExists ELSE SELECT ''No'' AS IsExists;'
EXEC sp_executesql @cmd_execCmd
GO

