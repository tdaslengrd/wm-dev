﻿using System.Data.Entity;
using System.Data;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web;
using System.IO;
using System.Data.OleDb;
using System.ComponentModel.DataAnnotations;

namespace TDServicesApp.Models
{
    public class OfferView
    {
        public DataTable offerTable { get; set; }
        public int offerCount { get; set; }
        public DateTime getDate { get; set; }


        public DataTable rateCardAU { get; set; }
        public int rateCardAUCount { get; set; }

        public DataTable customerList { get; set; }

        public DataTable officeStatus_healthStatus { get; set; }
        public DataTable azureStatus_healthStatus { get; set; }

        public string currentCharges { get; set; }

        public string officeUsage_itemCount { get; set; }
        public string azureUsage_itemCount { get; set; }
        public string azureDailyUsage_itemCount { get; set; }
        public DataTable officeLineItems { get; set; }
        public DataTable azureLineItems { get; set; }
        public DataTable azureDailyLineItems { get; set; }
        public decimal officeCosts { get; set; }
        public decimal azureCosts { get; set; }
        public decimal internalUseAzureCosts { get; set; }

        public DataTable CSPActivityLog { get; set; }

        public string domainName { get; set; }
        public string domainCheck { get; set; }

        public List<SubscriptionUsageList> SubscriptionList { get; set; }
        public DataTable SubTable { get; set; }
    }

    public class SubscriptionUsageList
    {
        public SubscriptionUsageList(string customerId, string subscriptionId, string subscriptionName, string subscriptionCost)
        {
            this.customerId = customerId;
            this.subscriptionId = subscriptionId;
            this.subscriptionName = subscriptionName;
            this.subscriptionCost = subscriptionCost;
        }

        public string customerId { get; set; }
        public string subscriptionId { get; set; }
        public string subscriptionName { get; set; }
        public string subscriptionCost { get; set; }
    }

    public class SCMProcessorView
    {
        public string Month { get; set; }
        public string Day { get; set; }
        public string Year { get; set; }
        public string currencyRate_User { get; set; }
        public string currencyRate_Live { get; set; }
        private List<SelectListItem> _regionSelection;
        public List<SelectListItem> regionSelection
        {
            get
            {
                List<SelectListItem> regionSelection = new List<SelectListItem>();
                regionSelection.Add(new SelectListItem { Text = "AMER", Value = "0" });
                regionSelection.Add(new SelectListItem { Text = "EMEA", Value = "1" });
                regionSelection.Add(new SelectListItem { Text = "APAC", Value = "2" });

                return regionSelection;
            }
            set
            {
                _regionSelection = value;
            }
        }
        public string regionSelected { get; set; }
        public DataTable currencyList { get; set; }
        private DataTable _currencyList_APAC;
        public DataTable currencyList_APAC
        {
            get
            {
                DataTable currencyList = new DataTable();
                currencyList.Columns.Add("CurrencyName");
                currencyList.Columns.Add("CurrencyNameISO");
                currencyList.Columns.Add("Rate");
                currencyList.Rows.Add("Vietnam", "USD/VND");

                return currencyList;
            }
            set
            {
                _currencyList_APAC = value;
            }
        }
        private DataTable _currencyList_AMER;
        public DataTable currencyList_AMER
        {
            get
            {
                DataTable currencyList = new DataTable();
                currencyList.Columns.Add("CurrencyName");
                currencyList.Columns.Add("CurrencyNameISO");
                currencyList.Columns.Add("Rate");
                currencyList.Rows.Add("United States", "USD");
                currencyList.Rows.Add("Canada", "USD/CAD");

                return currencyList;
            }
            set
            {
                _currencyList_AMER = value;
            }
        }
        private DataTable _currencyList_EMEA;
        public DataTable currencyList_EMEA
        {
            get
            {
                DataTable currencyList = new DataTable();
                currencyList.Columns.Add("CurrencyName");
                currencyList.Columns.Add("CurrencyNameISO");
                currencyList.Columns.Add("Rate");
                currencyList.Rows.Add("Turkey", "USD/TRY");

                return currencyList;
            }
            set
            {
                _currencyList_EMEA = value;
            }
        }

        //[Required(ErrorMessage = "Please select file.")]
        //[RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.xls|.xlsx)$", ErrorMessage = "Only Excel files allowed.")]
        public HttpPostedFileBase RI_file { get; set; }

        //[Required(ErrorMessage = "Please select file.")]
        //[RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.xls|.xlsx)$", ErrorMessage = "Only Excel files allowed.")]
        public HttpPostedFileBase Office_file { get; set; }
    }

    public class GAuthMFAClientView
    {
        public DataTable mfaList_View { get; set; }
        public string remainingTime { get; set; }
    }

    public static class Utility
    {
        public static DataTable ConvertCSVtoDataTable(string strFilePath)
        {
            DataTable dt = new DataTable();
            using (StreamReader sr = new StreamReader(strFilePath))
            {
                string[] headers = sr.ReadLine().Split(',');
                foreach (string header in headers)
                {
                    dt.Columns.Add(header);
                }

                while (!sr.EndOfStream)
                {
                    string[] rows = sr.ReadLine().Split(',');
                    if (rows.Length > 1)
                    {
                        DataRow dr = dt.NewRow();
                        for (int i = 0; i < headers.Length; i++)
                        {
                            dr[i] = rows[i].Trim();
                        }
                        dt.Rows.Add(dr);
                    }
                }
            }
            return dt;
        }

        public static DataTable ConvertXSLXtoDataTable(string strFilePath, string connString, string tableName, string cellRange)
        {
            OleDbConnection oledbConn = new OleDbConnection(connString);
            DataTable dt = new DataTable();
            try
            {
                oledbConn.Open();
                using (OleDbCommand cmd = new OleDbCommand("SELECT * FROM [" + tableName + "$" + cellRange + "]", oledbConn))
                {
                    OleDbDataAdapter oleda = new OleDbDataAdapter();
                    oleda.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    oleda.Fill(ds);

                    dt = ds.Tables[0];
                }
            }
            catch (Exception ex)
            {
                string test = ex.InnerException.ToString();
            }
            finally
            {
                oledbConn.Close();
            }

            return dt;
        }
    }
}