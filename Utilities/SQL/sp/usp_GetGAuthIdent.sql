USE [TDServicesWebApp]
GO

/****** Object:  StoredProcedure [dbo].[usp_GetGAuthIdent]    Script Date: 28/06/2018 6:50:46 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GetGAuthIdent] @companyName nvarchar(255) = NULL
AS
DECLARE @cmd_getGAuthIdent nvarchar(max)
IF (@companyName IS NULL)
BEGIN
	SET @cmd_getGAuthIdent = '
	SELECT * FROM TDServicesWebApp.dbo.GAuthIdents
	ORDER BY companyName ASC'
END
ELSE
BEGIN
	SET @cmd_getGAuthIdent = '
	SELECT * FROM TDServicesWebApp.dbo.GAuthIdents
	WHERE companyName = ' + @companyName + '
	ORDER BY companyName ASC'
END
EXEC sp_executesql @cmd_getGAuthIdent
GO

