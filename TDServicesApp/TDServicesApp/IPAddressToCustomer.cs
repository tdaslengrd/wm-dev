//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TDServicesApp
{
    using System;
    using System.Collections.Generic;
    
    public partial class IPAddressToCustomer
    {
        public string IPAddress { get; set; }
        public string Machine_Name { get; set; }
        public string CustomerNumber { get; set; }
        public string Customer_Name { get; set; }
    }
}
