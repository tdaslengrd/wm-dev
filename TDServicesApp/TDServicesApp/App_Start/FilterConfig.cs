﻿using System.Web;
using System.Web.Mvc;

namespace TDServicesApp
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new ErrorHandler.AiHandleErrorAttribute());
            filters.Add(new UseStopwatchAttribute()); //Added to use StopWatch globally - Information available here https://blogs.msdn.microsoft.com/webdev/2014/07/29/profile-and-time-your-asp-net-mvc-app-all-the-way-to-azure/
        }
    }
}
