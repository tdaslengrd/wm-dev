--SQL Job to automatically generate bandwidth usage stats and store within a table

--Start the main process--
BEGIN
	BEGIN TRY
		-- Declare the variable to be used.--
		DECLARE @t1 DATETIME;
		DECLARE @t2 DATETIME;
		DECLARE @tdiff varchar(100);
		DECLARE @createTable nvarchar(max)
		DECLARE @removeTable nvarchar(max)
		DECLARE @monthName nvarchar(30)
		DECLARE @cmd_genReport nvarchar(max)
		DECLARE @Divisor bigint
		DECLARE @DaysInMonth bigint
		declare @Year nvarchar(max)
		declare @MonthYearName nvarchar(max)
		declare @MonthNumber nvarchar(max)
		DECLARE @TableName nvarchar(max)
		
		SET @t1 = GETDATE();

		SET @monthName = DATENAME(month, GETDATE())
		SET @Year = YEAR(GETDATE())
		SET @TableName = @monthName + '_' + @Year
		SET @MonthYearName = @monthName + ' ' + @Year
		SET @MonthNumber = DATEPART(MM,@MonthYearName)
		SET @DaysInMonth = (DAY(EOMONTH(@Year + '-' + @MonthNumber + '-01')))
		SET @Divisor = (1024*1024*@DaysInMonth*24*60*60)
		SET @cmd_genReport = '
		SELECT ROUND(CAST(SUM(E.inbytes)/' + CAST(@Divisor AS NVARCHAR(100)) +' AS Float),2) AS average_inMbps, ROUND(CAST(SUM(E.outbytes)/' + CAST(@Divisor AS NVARCHAR(100)) +' AS Float),2) AS average_outMbps
		FROM NetflowResultsData.dbo.' + @TableName + ' E;'

		SET @removeTable = 'IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[dbo].[qs_BandwidthUsage]'') AND type in (N''U''))
		BEGIN
			DROP TABLE qs_BandwidthUsage;
		END'
		EXEC (@removeTable)

		SET @createTable = 'IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[dbo].[qs_BandwidthUsage]'') AND type in (N''U''))
		BEGIN
			CREATE TABLE [NetflowResultsData].[dbo].[qs_BandwidthUsage] (Id INT NOT NULL PRIMARY KEY IDENTITY(1,1), average_inMbps float, average_outMbps float);
		END'
		EXEC (@createTable)

		INSERT INTO qs_BandwidthUsage EXEC (@cmd_genReport);

		SET @t2 = GETDATE();
		SET @tdiff = DATEDIFF(MINUTE,@t1,@t2);
		PRINT 'Time elapsed - ' + @tdiff + ' minutes.';
	END TRY
	BEGIN CATCH  
		SELECT   
			ERROR_NUMBER() AS ErrorNumber  
			,ERROR_MESSAGE() AS ErrorMessage;
			DROP TABLE qs_BandwidthUsage;
	END CATCH
END;