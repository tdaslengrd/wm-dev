CREATE TABLE [dbo].[log4net_GeneralLogging](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[log_date] [DateTime] NULL,
	[thread] [nvarchar](255) NULL,
	[log_level] [nvarchar](255) NULL,
	[logger] [nvarchar](255) NULL,
	[message] [nvarchar](255) NULL,
	[exception] [nvarchar](255) NULL,
	[url] [nvarchar](255) NULL,
)