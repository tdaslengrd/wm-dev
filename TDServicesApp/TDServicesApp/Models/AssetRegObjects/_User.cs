﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TDServicesApp.Models.AssetRegObjects
{
    public class _User
    {
        public int UserNumber { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}