﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TDServicesApp.Models.AssetRegObjects
{
    public class IPAddressList
    {
        public string IPAddress { get; set; }
    }
}