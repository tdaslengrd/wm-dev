﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System;
using System.ComponentModel.DataAnnotations;
using TDServicesApp.Models.AssetRegObjects;

namespace TDServicesApp.Models
{
    public class CustomersModel
    {
        public Boolean IsNew { get; set; }

        [Required(ErrorMessage = "This Field Is Mandatory, it must be 4 or 5 numbers or letters")]
        public string CustomerNumber { get; set; }

        [Required(ErrorMessage = "This Field Is Mandatory")]
        public string CustomerName { get; set; }

        public List<_User> Users { get; set; }
    }
    public class SelectedCustomer
    {
        public string CustomerNumber { get; set; }
        public string CustomerName { get; set; }
        public SelectedMachine Machine { get; set; }
        public SelectedUser User { get; set; }
    }
    public class SelectedInterface
    {
        public string InterfaceName { get; set; }
        public int InterfaceID { get; set; }
        public SelectedIPAddress IPAddress { get; set; }
    }
    public class SelectedIPAddress
    {
        public string PublicIP { get; set; }
        public string PrivateIP { get; set; }
    }
    public class SelectedMachine
    {
        public string MachineName { get; set; }
        public int MachineID { get; set; }
        public SelectedInterface Inter { get; set; }
    }
    public class SelectedUser
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
    public class RdpViewModel
    {
        public string HostIP { get; set; }
        public string UserName { get; set; }
    }
}