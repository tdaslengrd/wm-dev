USE [VeeamUsageData]
GO

/****** Object:  StoredProcedure [dbo].[usp_GetVeeamServers]    Script Date: 12/12/2017 8:24:16 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GetVeeamServers] @tableName nvarchar(30)
AS
DECLARE @cmd_genReport nvarchar(max)
SET @cmd_genReport = '
SELECT DISTINCT veeamserver AS ''Reporting Veeam Servers'' FROM VeeamUsageData.dbo.' + @tableName + ';'
EXEC sp_executesql @cmd_genReport
GO

