USE [VeeamUsageData]
GO

/****** Object:  StoredProcedure [dbo].[usp_GetVeeamBadJobNames]    Script Date: 12/12/2017 8:22:35 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GetVeeamBadJobNames] @tableName nvarchar(30)
AS
DECLARE @cmd_genReport nvarchar(max)
SET @cmd_genReport = '
SELECT COUNT(DISTINCT jobName) AS ''Veeam Jobs with bad names'' FROM VeeamUsageData.dbo.' + @tableName + ' WHERE jobName NOT LIKE ''[0-9][0-9][0-9][0-9]%'' AND jobName NOT LIKE ''[0-9][0-9][0-9][0-9][0-9]%'';'
EXEC sp_executesql @cmd_genReport
GO

