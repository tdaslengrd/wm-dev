﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TDServicesApp.Controllers
{
    public class DemoController : Controller
    {
        // GET: Demo
        [LayoutInjecter("_Layout")]
        public ActionResult SESCustomerPortal()
        {
            return View();
        }
    }
}