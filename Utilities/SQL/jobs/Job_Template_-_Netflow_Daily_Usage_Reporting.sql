USE [msdb]
GO

/****** Object:  Job [Netflow Daily Data Usage Processing]    Script Date: 12/12/2017 8:11:09 AM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 12/12/2017 8:11:09 AM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Netflow Daily Data Usage Processing', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Processes data for the previous day passed.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'TDSD\tazr', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Run Daily Usage Report]    Script Date: 12/12/2017 8:11:09 AM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Run Daily Usage Report', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=4, 
		@on_success_step_id=2, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'--PRIOR TO RUNNING THIS SCRIPT, MAKE SURE MAXDOP IS SET CORRECTLY
--FAILING TO DO SO WILL CAUSE THREAD EXECUTION EXHAUSTION WHICH WILL CAUSE THE SCRIPT TO HALT!

--Before running this script, set Line 31 to the date from when you want to start parsing records from.
--For example,-If you set line 31 to 2017-08-01 this will parse all data for the month of August.

--Start the main process--
BEGIN
	BEGIN TRY
		-- Declare the variable to be used.--
		DECLARE @ipaddr varchar(50);
		DECLARE @dateTime datetime;
		DECLARE @cmd_createTable varchar(255);
		DECLARE @cmd_fillOutTable nvarchar(max);
		DECLARE @cmd_insertTable nvarchar(max);
		DECLARE @t1 DATETIME;
		DECLARE @t2 DATETIME;
		DECLARE @tdiff varchar(100);
		DECLARE @yyyyMM_TableName nvarchar(50);
		DECLARE @createTable nvarchar(max);
		DECLARE @inTable nvarchar(50);
		DECLARE @outTable nvarchar(50);
		DECLARE @setDate nvarchar(50);
		DECLARE @monthName nvarchar(50);
		DECLARE @year nvarchar(50);

		SET @setDate = dateadd(day,-1, CONVERT(date, getdate(), 120))
		SET @monthName = DATENAME(month, @setDate)
		SET @year = DATENAME(year, @setDate)

		SET @yyyyMM_TableName = @monthName + ''_'' + @year;
		SET @createTable = ''IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N''''[''+@yyyyMM_TableName+'']'''')
		BEGIN
			CREATE TABLE [NetflowResultsData].[dbo].[''+@yyyyMM_TableName+''] (Id INT NOT NULL PRIMARY KEY IDENTITY(1,1),ipaddr varchar(50),inbytes bigint,outbytes bigint,dt DATETIME);
		END''

		EXEC (@createTable)

		SET @t1 = GETDATE();
		SET @inTable = ''nfd_in_'' + @monthName + ''_'' + @year;
		SET @outTable = ''nfd_out_'' + @monthName + ''_'' + @year;

		CREATE TABLE #NetflowTable_tmp(ID int IDENTITY(1,1) NOT NULL, ipaddr nvarchar(50), inbytes bigint, outbytes bigint, dt nvarchar(50), UNIQUE NONCLUSTERED (ipaddr, ID));

		SET @cmd_fillOutTable = ''
								SELECT
								CASE
									WHEN InOutData.outipaddr IS NULL
										THEN InOutData.inipaddr
										ELSE InOutData.outipaddr
									END
								AS ipaddr,
								ISNULL(InOutData.outbytes, 0) AS outbytes,
								ISNULL(InOutData.inbytes, 0) AS inbytes,
								CASE
									WHEN InOutData.outdt IS NULL
										THEN InOutData.indt
										ELSE InOutData.outdt
									END
								AS dt
								FROM
									(SELECT sIPdata2.outipaddr as outipaddr, dIPdata2.inipaddr AS inipaddr, sIPdata2.outbytes AS outbytes, dIPdata2.inbytes AS inbytes, sIPdata2.outdt AS outdt, dIPdata2.indt AS indt
									FROM
										(SELECT distinct(dIPdata.ipaddr) AS inipaddr, dIPdata.dt AS indt, sum(dIPdata.inbytes) AS inbytes
										FROM
											(SELECT DISTINCT DATEADD(HH,DATEPART(HH,dt),CAST(CAST(dt AS DATE) AS DATETIME)) AS dt, dIP AS ipaddr, SUM(bytes) AS inbytes
											FROM [NetflowData].[dbo].['' + @inTable + '']
											WHERE CAST ([dt] as DATE) = convert(date,CONVERT(varchar(10),''''''+@setDate+'''''',120))
												GROUP BY dIP, dt) dIPdata
											GROUP BY ipaddr, dt) dIPdata2
										full join
										(SELECT distinct(sIPdata.ipaddr) AS outipaddr, sIPdata.dt AS outdt, sum(sIPdata.outbytes) AS outbytes
										FROM
											(SELECT DISTINCT DATEADD(HH,DATEPART(HH,dt),CAST(CAST(dt AS DATE) AS DATETIME)) AS dt, sIP AS ipaddr, SUM(bytes) AS outbytes
											FROM [NetflowData].[dbo].['' + @outTable + '']
											WHERE CAST ([dt] as DATE) = convert(date,CONVERT(varchar(10),''''''+@setDate+'''''',120))
												GROUP BY sIP, dt) sIPdata
											GROUP BY ipaddr, dt) sIPdata2
										ON sIPdata2.outipaddr = dIPdata2.inipaddr AND sIPdata2.outdt = dIPdata2.indt) InOutData
									GROUP BY inipaddr, outipaddr, inbytes, outbytes, outdt, indt
								ORDER BY dt'';

		INSERT INTO #NetflowTable_tmp EXEC (@cmd_fillOutTable);

		SET @cmd_insertTable = ''INSERT INTO [NetflowResultsData].[dbo].['' + @yyyyMM_TableName + ''] (ipaddr, inbytes, outbytes, dt)
		SELECT T.ipaddr, T.inbytes, T.outbytes, (CONVERT(DATETIME, T.dt, 120)) AS dt
		FROM #NetflowTable_tmp T;'';

		EXEC sp_executesql @cmd_insertTable;

		DROP TABLE #NetflowTable_tmp;

		SET @t2 = GETDATE();
		SET @tdiff = DATEDIFF(MINUTE,@t1,@t2);
		PRINT ''Time elapsed - '' + @tdiff + '' minutes.'';
	END TRY
	BEGIN CATCH  
		SELECT   
			ERROR_NUMBER() AS ErrorNumber  
			,ERROR_MESSAGE() AS ErrorMessage;
			DROP TABLE #NetflowTable_tmp;
	END CATCH
END;', 
		@database_name=N'NetflowResultsData', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Generate Forecast Data]    Script Date: 12/12/2017 8:11:09 AM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Generate Forecast Data', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=4, 
		@on_success_step_id=3, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'--SQL Job to automatically generate forecast usage stats and store within a table

--Start the main process--
BEGIN
	BEGIN TRY
		-- Declare the variable to be used.--
		DECLARE @ipaddr varchar(50);
		DECLARE @dateTime datetime;
		DECLARE @cmd_createTable varchar(255);
		DECLARE @cmd_fillOutTable nvarchar(max);
		DECLARE @cmd_insertTable nvarchar(max);
		DECLARE @t1 DATETIME;
		DECLARE @t2 DATETIME;
		DECLARE @tdiff varchar(100);
		DECLARE @Table_Name nvarchar(50);
		DECLARE @createTable nvarchar(max);
		DECLARE @removeTable nvarchar(max);
		DECLARE @inTable nvarchar(50);
		DECLARE @outTable nvarchar(50);
		DECLARE @setDate nvarchar(50);
		DECLARE @monthName nvarchar(50);
		DECLARE @year nvarchar(50);
		DECLARE @DaysInMonth bigint

		SET @setDate = GETDATE();

		SET @monthName = DATENAME(month, @setDate)
		SET @year = DATENAME(year, @setDate)
		SET @Table_Name = @monthName + ''_'' + @year;
		SET @DaysInMonth = (DAY(EOMONTH(@setDate)))

		SET @removeTable = ''IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''''[dbo].[qs_ForecastUsage]'''') AND type in (N''''U''''))
		BEGIN
			DROP TABLE qs_ForecastUsage;
		END''
		EXEC (@removeTable)

		SET @createTable = ''IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''''[dbo].[qs_ForecastUsage]'''') AND type in (N''''U''''))
		BEGIN
			CREATE TABLE [NetflowResultsData].[dbo].[qs_ForecastUsage] (Id INT NOT NULL PRIMARY KEY IDENTITY(1,1), averageDaily_inGBytes bigint, averageDaily_outGBytes bigint, MonthForecast bigint);
		END''
		EXEC (@createTable)

		SET @t1 = GETDATE();

		SET @cmd_fillOutTable = ''SELECT AVG(AverageUsage.total_inGBytes) AS averageDaily_inGBytes, AVG(AverageUsage.total_outGBytes) AS averageDaily_outGBytes, (AVG(AverageUsage.total_inGBytes) + AVG(AverageUsage.total_outGBytes))*'' + CAST(@DaysinMonth AS nvarchar(30)) + '' AS MonthForecast
								FROM
								(SELECT DISTINCT DailyUsage.dt, ROUND(CAST(SUM(DailyUsage.inbytes) AS Float)/1073741824,2) AS total_inGBytes, ROUND(CAST(SUM(DailyUsage.outbytes) AS Float)/1073741824,2) AS total_outGBytes
								FROM
									(SELECT	DISTINCT (FORMAT(dt, ''''yyyy-MM-dd'''')) AS dt, inbytes,	outbytes
									FROM NetflowResultsData.dbo.'' + @Table_Name + ''
									GROUP BY dt, inbytes, outbytes) DailyUsage
								GROUP BY dt) AverageUsage;'';

		INSERT INTO qs_ForecastUsage EXEC (@cmd_fillOutTable);

		SET @t2 = GETDATE();
		SET @tdiff = DATEDIFF(MINUTE,@t1,@t2);
		PRINT ''Time elapsed - '' + @tdiff + '' minutes.'';
	END TRY
	BEGIN CATCH  
		SELECT   
			ERROR_NUMBER() AS ErrorNumber  
			,ERROR_MESSAGE() AS ErrorMessage;
			DROP TABLE qs_ForecastUsage;
	END CATCH
END;', 
		@database_name=N'NetflowResultsData', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Generate Bandwidth Usage Stats]    Script Date: 12/12/2017 8:11:09 AM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Generate Bandwidth Usage Stats', 
		@step_id=3, 
		@cmdexec_success_code=0, 
		@on_success_action=4, 
		@on_success_step_id=4, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'--SQL Job to automatically generate bandwidth usage stats and store within a table

--Start the main process--
BEGIN
	BEGIN TRY
		-- Declare the variable to be used.--
		DECLARE @t1 DATETIME;
		DECLARE @t2 DATETIME;
		DECLARE @tdiff varchar(100);
		DECLARE @createTable nvarchar(max)
		DECLARE @removeTable nvarchar(max)
		DECLARE @monthName nvarchar(30)
		DECLARE @cmd_genReport nvarchar(max)
		DECLARE @Divisor bigint
		DECLARE @DaysInMonth bigint
		declare @Year nvarchar(max)
		declare @MonthYearName nvarchar(max)
		declare @MonthNumber nvarchar(max)
		DECLARE @TableName nvarchar(max)
		
		SET @t1 = GETDATE();

		SET @monthName = DATENAME(month, GETDATE())
		SET @Year = YEAR(GETDATE())
		SET @TableName = @monthName + ''_'' + @Year
		SET @MonthYearName = @monthName + '' '' + @Year
		SET @MonthNumber = DATEPART(MM,@MonthYearName)
		SET @DaysInMonth = (DAY(EOMONTH(@Year + ''-'' + @MonthNumber + ''-01'')))
		SET @Divisor = (1024*1024*@DaysInMonth*24*60*60)
		SET @cmd_genReport = ''
		SELECT ROUND(CAST(SUM(E.inbytes)/'' + CAST(@Divisor AS NVARCHAR(100)) +'' AS Float),2) AS average_inMbps, ROUND(CAST(SUM(E.outbytes)/'' + CAST(@Divisor AS NVARCHAR(100)) +'' AS Float),2) AS average_outMbps
		FROM NetflowResultsData.dbo.'' + @TableName + '' E;''

		SET @removeTable = ''IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''''[dbo].[qs_BandwidthUsage]'''') AND type in (N''''U''''))
		BEGIN
			DROP TABLE qs_BandwidthUsage;
		END''
		EXEC (@removeTable)

		SET @createTable = ''IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''''[dbo].[qs_BandwidthUsage]'''') AND type in (N''''U''''))
		BEGIN
			CREATE TABLE [NetflowResultsData].[dbo].[qs_BandwidthUsage] (Id INT NOT NULL PRIMARY KEY IDENTITY(1,1), average_inMbps float, average_outMbps float);
		END''
		EXEC (@createTable)

		INSERT INTO qs_BandwidthUsage EXEC (@cmd_genReport);

		SET @t2 = GETDATE();
		SET @tdiff = DATEDIFF(MINUTE,@t1,@t2);
		PRINT ''Time elapsed - '' + @tdiff + '' minutes.'';
	END TRY
	BEGIN CATCH  
		SELECT   
			ERROR_NUMBER() AS ErrorNumber  
			,ERROR_MESSAGE() AS ErrorMessage;
			DROP TABLE qs_BandwidthUsage;
	END CATCH
END;', 
		@database_name=N'NetflowResultsData', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Generate Billing Report]    Script Date: 12/12/2017 8:11:09 AM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Generate Billing Report', 
		@step_id=4, 
		@cmdexec_success_code=0, 
		@on_success_action=4, 
		@on_success_step_id=5, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'--SQL Job to automatically generate billing report data and store within a table

--Start the main process--
BEGIN
	BEGIN TRY
		-- Declare the variable to be used.--
		DECLARE @t1 DATETIME;
		DECLARE @t2 DATETIME;
		DECLARE @tdiff varchar(100);
		DECLARE @createTable nvarchar(max)
		DECLARE @removeTable nvarchar(max)
		DECLARE @monthName nvarchar(30)
		DECLARE @cmd_genReport nvarchar(max)
		declare @Year nvarchar(max)
		DECLARE @Table_Name nvarchar(max)
		
		SET @t1 = GETDATE();

		SET @monthName = DATENAME(month, @t1)
		SET @year = DATENAME(year, @t1)
		SET @Table_Name = @monthName + ''_'' + @year;
		
		SET @cmd_genReport = ''
		SELECT A.IPAddress, C.MachineID, C.[Machine Name], D.CustomerNumber, D.[Customer Name], c.contract_num_sap, E.inbytes, E.outbytes, CONVERT(datetime, SWITCHOFFSET(CONVERT(datetimeoffset, E.dt), DATENAME(TzOffset, SYSDATETIMEOFFSET()))) AS dt
		FROM assetregisterSQL.dbo.IPAddressesNew A
		inner join assetregisterSQL.dbo.Interfaces B on A.InterfaceID = B.InterfaceID
		inner join assetregisterSQL.dbo.Machines C on C.MachineID = B.MachineID
		inner join assetregisterSQL.dbo.Customers D on D.CustomerNumber = C.CustomerNumber
		inner join NetflowResultsData.dbo.'' + @Table_Name + '' E on A.IPAddress = E.ipaddr
		ORDER BY dt ASC''
		
		SET @removeTable = ''IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''''[dbo].[qs_BillingReport]'''') AND type in (N''''U''''))
		BEGIN
			DROP TABLE qs_BillingReport;
		END''
		EXEC (@removeTable)

		SET @createTable = ''IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''''[dbo].[qs_BillingReport]'''') AND type in (N''''U''''))
		BEGIN
			CREATE TABLE [NetflowResultsData].[dbo].[qs_BillingReport] (Id INT NOT NULL PRIMARY KEY IDENTITY(1,1), IPAddrsss nvarchar(20), MachineID nvarchar(10), MachineName nvarchar(max), CustomerNumber nvarchar(10), CustomerName nvarchar(max), ContractNumber nvarchar(20), BytesInbound bigint, BytedOutbound bigint, dt datetime);
		END''
		EXEC (@createTable)

		--EXEC sp_executesql @cmd_genReport
		INSERT INTO qs_BillingReport EXEC (@cmd_genReport);

		SET @t2 = GETDATE();
		SET @tdiff = DATEDIFF(MINUTE,@t1,@t2);
		PRINT ''Time elapsed - '' + @tdiff + '' minutes.'';
	END TRY
	BEGIN CATCH  
		SELECT   
			ERROR_NUMBER() AS ErrorNumber  
			,ERROR_MESSAGE() AS ErrorMessage;
			DROP TABLE qs_BillingReport;
	END CATCH
END;', 
		@database_name=N'NetflowResultsData', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Generate Daily Usage Stats]    Script Date: 12/12/2017 8:11:09 AM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Generate Daily Usage Stats', 
		@step_id=5, 
		@cmdexec_success_code=0, 
		@on_success_action=4, 
		@on_success_step_id=6, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'--SQL Job to automatically generate per day usage stats and store within a table

--Start the main process--
BEGIN
	BEGIN TRY
		-- Declare the variable to be used.--
		DECLARE @t1 DATETIME;
		DECLARE @t2 DATETIME;
		DECLARE @tdiff varchar(100);
		DECLARE @createTable nvarchar(max)
		DECLARE @removeTable nvarchar(max)
		DECLARE @monthName nvarchar(30)
		DECLARE @cmd_genReport nvarchar(max)
		declare @Year nvarchar(max)
		DECLARE @Table_Name nvarchar(max)
		
		SET @t1 = GETDATE();

		SET @monthName = DATENAME(month, @t1)
		SET @year = DATENAME(year, @t1)
		SET @Table_Name = @monthName + ''_'' + @year;
		
		SET @cmd_genReport = ''
		SELECT DISTINCT (FORMAT(DailyUsage.dt, ''''yyyy-MM-dd'''')) AS dt, ROUND(CAST(SUM(DailyUsage.inbytes) AS Float)/1073741824,2) AS total_inGBytes, ROUND(CAST(SUM(DailyUsage.outbytes) AS Float)/1073741824,2) AS total_outGBytes
		FROM
			(SELECT	DISTINCT (CAST(CONVERT(datetime, SWITCHOFFSET(CONVERT(datetimeoffset, dt), DATENAME(TzOffset, SYSDATETIMEOFFSET()))) AS DATE)) AS dt, inbytes, outbytes
			FROM NetflowResultsData.dbo.'' + @Table_Name + ''
			GROUP BY dt, inbytes, outbytes) DailyUsage
		GROUP BY dt;''
		
		SET @removeTable = ''IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''''[dbo].[qs_TotalUsagePerDay]'''') AND type in (N''''U''''))
		BEGIN
			DROP TABLE qs_TotalUsagePerDay;
		END''
		EXEC (@removeTable)

		SET @createTable = ''IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''''[dbo].[qs_TotalUsagePerDay]'''') AND type in (N''''U''''))
		BEGIN
			CREATE TABLE [NetflowResultsData].[dbo].[qs_TotalUsagePerDay] (Id INT NOT NULL PRIMARY KEY IDENTITY(1,1), dt DateTime, total_inGBytes float, total_outGBytes float);
		END''
		EXEC (@createTable)

		--EXEC sp_executesql @cmd_genReport
		INSERT INTO qs_TotalUsagePerDay EXEC (@cmd_genReport);

		SET @t2 = GETDATE();
		SET @tdiff = DATEDIFF(MINUTE,@t1,@t2);
		PRINT ''Time elapsed - '' + @tdiff + '' minutes.'';
	END TRY
	BEGIN CATCH  
		SELECT   
			ERROR_NUMBER() AS ErrorNumber  
			,ERROR_MESSAGE() AS ErrorMessage;
			DROP TABLE qs_TotalUsagePerDay;
	END CATCH
END;', 
		@database_name=N'NetflowResultsData', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Generate Invalid Customer Report]    Script Date: 12/12/2017 8:11:09 AM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Generate Invalid Customer Report', 
		@step_id=6, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'--SQL Job to automatically generate report on customer who dont have a SAP control number and store within a table

--Start the main process--
BEGIN
	BEGIN TRY
		-- Declare the variable to be used.--
		DECLARE @t1 DATETIME;
		DECLARE @t2 DATETIME;
		DECLARE @tdiff varchar(100);
		DECLARE @createTable nvarchar(max)
		DECLARE @removeTable nvarchar(max)
		DECLARE @monthName nvarchar(30)
		DECLARE @cmd_genReport nvarchar(max)
		declare @Year nvarchar(max)
		DECLARE @Table_Name nvarchar(max)
		
		SET @t1 = GETDATE();

		SET @monthName = DATENAME(month, @t1)
		SET @year = DATENAME(year, @t1)
		SET @Table_Name = @monthName + ''_'' + @year;
		
		SET @cmd_genReport = ''
		SELECT DISTINCT D.CustomerNumber, D.[Customer Name], C.[Machine Name], ROUND(CAST(SUM(E.inbytes) AS Float)/1073741824,5) AS total_inGBytes, ROUND(CAST(SUM(E.outbytes) AS Float)/1073741824,5) AS total_outGBytes, (ROUND(CAST(SUM(E.inbytes) AS Float)/1073741824,5) + ROUND(CAST(SUM(E.outbytes) AS Float)/1073741824,5)) AS total_GBytes
		FROM assetregisterSQL.dbo.IPAddressesNew A
		inner join assetregisterSQL.dbo.Interfaces B on A.InterfaceID = B.InterfaceID
		inner join assetregisterSQL.dbo.Machines C on C.MachineID = B.MachineID
		inner join assetregisterSQL.dbo.Customers D on D.CustomerNumber = C.CustomerNumber
		inner join NetflowResultsData.dbo.'' + @Table_Name + '' E on A.IPAddress = E.ipaddr AND C.contract_num_sap IS NULL AND D.[Customer Name] NOT LIKE ''''ICO%''''
		GROUP BY D.CustomerNumber, D.[Customer Name], C.[Machine Name]
		ORDER BY total_inGBytes DESC;''
		
		SET @removeTable = ''IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''''[dbo].[qs_InvalidCust]'''') AND type in (N''''U''''))
		BEGIN
			DROP TABLE qs_InvalidCust;
		END''
		EXEC (@removeTable)

		SET @createTable = ''IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''''[dbo].[qs_InvalidCust]'''') AND type in (N''''U''''))
		BEGIN
			CREATE TABLE [NetflowResultsData].[dbo].[qs_InvalidCust] (Id INT NOT NULL PRIMARY KEY IDENTITY(1,1), CustomerNumber nvarchar(10), [Customer Name] nvarchar(max), [Machine Name] nvarchar(max), total_inGBytes bigint, total_outGBytes bigint, total_GBytes bigint);
		END''
		EXEC (@createTable)

		--EXEC sp_executesql @cmd_genReport
		INSERT INTO qs_InvalidCust EXEC (@cmd_genReport);

		SET @t2 = GETDATE();
		SET @tdiff = DATEDIFF(MINUTE,@t1,@t2);
		PRINT ''Time elapsed - '' + @tdiff + '' minutes.'';
	END TRY
	BEGIN CATCH  
		SELECT   
			ERROR_NUMBER() AS ErrorNumber  
			,ERROR_MESSAGE() AS ErrorMessage;
			DROP TABLE qs_InvalidCust;
	END CATCH
END;', 
		@database_name=N'NetflowResultsData', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Daily Schedule - 8AM', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20170717, 
		@active_end_date=99991231, 
		@active_start_time=80000, 
		@active_end_time=235959, 
		@schedule_uid=N'6ee8d2fd-ae92-49ce-97cc-677c4c372970'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

