﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TDServicesApp.Controllers
{
    [LayoutInjecter("_LayoutDPMetering")]
    public class DPMeteringController : GlobalController
    {
        // GET: DPMetering
        public ActionResult Index()
        {
            //string Year = DateTime.Now.Year.ToString();
            //string Month = DateTime.Now.ToString("MMMM");

            string Year = DateTime.Now.Year.ToString();
            string previousMonth = DateTime.Now.AddMonths(-1).ToString("MMMM");
            string Month = DateTime.Now.ToString("MMMM");

            //Check if the current date is within the first week of the month, if it is, we need to look at the previous month
            //This is buggy and needs further development, however, a failsafe check has been constructed via a TableCheck later on which 
            //assumes that if the tableName cant be found, to go back a Month and check the tables.
            var firstWeek = weekRangesInMonth().First();
            if (InRange(DateTime.Now, firstWeek.Item1, firstWeek.Item2))
            {
                Month = previousMonth;
            }

            string WeekNumber = (GetWeekNumber(DateTime.Now) -1).ToString(); //We get the last weeks reported data
            string tableName = Month + "_" + Year + "_" + WeekNumber;

            string connectionString = "VeeamUsageDataConnectionString";
            string usp_TableCheck = "usp_TableCheck";
            string usp_GetVeeamStorageUsed = "usp_GetVeeamStorageUsed";
            string usp_GetVeeamBackupJobCount = "usp_GetVeeamBackupJobCount";
            string usp_GetVeeamBadJobNames = "usp_GetVeeamBadJobNames";
            string usp_GetVeeamBadJobs = "usp_GetVeeamBadJobs";
            string usp_GetVeeamJobCount = "usp_GetVeeamJobCount";
            string usp_GetVeeamOtherJobCount = "usp_GetVeeamOtherJobCount";
            string usp_GetVeeamReplicaJobCount = "usp_GetVeeamReplicaJobCount";
            string usp_GetVeeamServers = "usp_GetVeeamServers";
            string usp_GetVeeamUsageCustomerReport = "usp_GetVeeamUsageCustomerReport";

            try
            {
                string tableCheck = SQL_GetSingleResult(tableName, usp_TableCheck, connectionString);
                if (tableCheck == "No")
                {
                    tableName = previousMonth + "_" + Year + "_" + WeekNumber;
                }
                else
                {
                    ViewBag.error = "Something went wrong!";
                    return View();
                }

                ViewBag.VeeamStorageUsed = SQL_GetSingleResult(tableName, usp_GetVeeamStorageUsed, connectionString);
                ViewBag.VeeamBackupJobCount = SQL_GetSingleResult(tableName, usp_GetVeeamBackupJobCount, connectionString);
                ViewBag.VeeamBadJobNames = SQL_GetSingleResult(tableName, usp_GetVeeamBadJobNames, connectionString);
                ViewBag.VeeamBadJobs = SQL_GetSingleResult(tableName, usp_GetVeeamBadJobs, connectionString);
                ViewBag.VeeamJobCount = SQL_GetSingleResult(tableName, usp_GetVeeamJobCount, connectionString);
                ViewBag.VeeamOtherJobCount = SQL_GetSingleResult(tableName, usp_GetVeeamOtherJobCount, connectionString);
                ViewBag.VeeamReplicaJobCount = SQL_GetSingleResult(tableName, usp_GetVeeamReplicaJobCount, connectionString);
                ViewBag.VeeamStorageUsed = SQL_GetSingleResult(tableName, usp_GetVeeamStorageUsed, connectionString);
                DataTable VeeamServer = SQL_GetMultiResult(tableName, usp_GetVeeamServers, connectionString);
                DataTable VeVeeamUsageCustomerReporteamServer = SQL_GetMultiResult(tableName, usp_GetVeeamUsageCustomerReport, connectionString);
            }
            catch
            {
                ViewBag.error = "Something went wrong while attempting to get infomration from the database!";
            }

            return View();
        }

        public static int GetWeekNumber(DateTime dtPassed)
        {
            CultureInfo ciCurr = CultureInfo.CurrentCulture;
            int weekNum = ciCurr.Calendar.GetWeekOfYear(dtPassed, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
            return weekNum;
        }

        public static List<Tuple<DateTime, DateTime>> weekRangesInMonth()
        {
            DateTime reference = DateTime.Now;
            Calendar calendar = CultureInfo.CurrentCulture.Calendar;

            IEnumerable<int> daysInMonth = Enumerable.Range(1, calendar.GetDaysInMonth(reference.Year, reference.Month));

            List<Tuple<DateTime, DateTime>> weeks = daysInMonth.Select(day => new DateTime(reference.Year, reference.Month, day))
                .GroupBy(d => calendar.GetWeekOfYear(d, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday))
                .Select(g => new Tuple<DateTime, DateTime>(g.First(), g.Last()))
                .ToList();
            
            return weeks;
        }

        public static bool InRange(DateTime dateToCheck, DateTime startDate, DateTime endDate)
        {
            return dateToCheck >= startDate && dateToCheck < endDate;
        }

        public static string SQL_GetSingleResult(string tableName, string storedProc, string connectionString)
        {
            string singleResult = "";
            try
            {
                using (var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[connectionString].ConnectionString))
                using (var cmd = new SqlCommand(storedProc, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@tableName", tableName);
                    con.Open();
                    var result = cmd.ExecuteScalar();
                    con.Close();
                    singleResult = result.ToString();
                }
            }
            catch (Exception ex)
            {
                // log4net custom variables for log output
                log4net.GlobalContext.Properties["url"] = ex.TargetSite.ToString();

                // Use error or higher to send exception to logger
                log.Error(ex.Message.ToString());

                singleResult = ex.Message.ToString();
            }
            return singleResult;
        }

        public static DataTable SQL_GetMultiResult(string tableName, string storedProc, string connectionString)
        {
            DataTable results = new DataTable();

            try
            {
                using (var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[connectionString].ConnectionString))
                using (var cmd = new SqlCommand(storedProc, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@tableName", tableName);
                    con.Open();
                    da.Fill(results);
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                // log4net custom variables for log output
                log4net.GlobalContext.Properties["url"] = ex.TargetSite.ToString();

                // Use error or higher to send exception to logger
                log.Error(ex.Message.ToString());

                results = null; //Perform check in action to determine error state
            }

            return results;
        }
    }
}