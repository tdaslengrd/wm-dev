﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TDServicesApp.Startup))]
namespace TDServicesApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
