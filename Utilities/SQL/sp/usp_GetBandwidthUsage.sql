USE [NetflowResultsData]
GO

/****** Object:  StoredProcedure [dbo].[usp_GetBandwidthUsage]    Script Date: 20/10/2017 10:36:35 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_GetBandwidthUsage] @Month nvarchar(30)
AS

DECLARE @cmd_genReport nvarchar(max)
DECLARE @Divisor bigint
DECLARE @DaysInMonth bigint
declare @Year nvarchar(max)
declare @MonthYearName nvarchar(max)
declare @MonthNumber nvarchar(max)
DECLARE @TableName nvarchar(max)
SET @Year = YEAR(GETDATE())
SET @TableName = @Month + '_' + @Year
SET @MonthYearName = @Month + ' ' + @Year
SET @MonthNumber = DATEPART(MM,@MonthYearName)
SET @DaysInMonth = (DAY(EOMONTH(@Year + '-' + @MonthNumber + '-01')))
SET @Divisor = (1024*1024*@DaysInMonth*24*60*60)
SET @cmd_genReport = '
SELECT ROUND(CAST(SUM(E.inbytes)/' + CAST(@Divisor AS NVARCHAR(100)) +' AS Float),2) AS average_inMbps, ROUND(CAST(SUM(E.outbytes)/' + CAST(@Divisor AS NVARCHAR(100)) +' AS Float),2) AS average_outMbps
FROM NetflowResultsData.dbo.' + @TableName + ' E;'
EXEC sp_executesql @cmd_genReport




GO


