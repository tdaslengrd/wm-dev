#!/bin/bash

### NetflowBulkExporter Shell Script v2.1 ###

echo "NetflowBulkExporter Shell Script v2.1"

####### NOTES #######
#This script will run once per day, for the previous day results.
#i.e. Set a cron job to run each day at 11pm, this will run the import job for the previous day, between 00:00:00 to 23:59:59.
#####################

#Script variables, dont change unless you know what your doing
LOCALENVPATH=/usr/local/bin
ENVPATH=/usr/bin
SYSENVPATH=/bin
SENSORCFG='/data/sensor.conf'

DAY=1
MONTH=09
YEAR=2017
DAYSINMONTH=`cal $(date +"$MONTH $YEAR") | awk 'NF {DAYS = $NF}; END {print DAYS}'`
#DAYSINMONTH=5

CURRENTDATETIME=`date +"%Y-%m-%d %H:%M:%S"`

#Timezone corrector logic
TIMEZONE=`date +"%Z"`
if [ "$TIMEZONE" == "AEDT" ]
then
	echo "Current timezone is ASDT"
	LOCAL2UTC_STARTDATE=`date --date "${YEAR}/${MONTH}/${DAY} 00 -10 hours" +"%Y/%m/%d %H"`
	LOCAL2UTC_ENDDATE=`date --date "${YEAR}/${MONTH}/${DAYSINMONTH} 00 -10 hours" +"%Y/%m/%d %H"`
elif [ "$TIMEZONE" == "AEST" ]
then
	echo "Current timezone is AEST"
	LOCAL2UTC_STARTDATE=`date --date "${YEAR}/${MONTH}/${DAY} 00 -11 hours" +"%Y/%m/%d %H"`
	LOCAL2UTC_ENDDATE=`date --date "${YEAR}/${MONTH}/${DAYSINMONTH} 00 -11 hours" +"%Y/%m/%d %H"`
else
	echo "Timezone is not AEST or AEDT, exiting!"
	exit 1
fi

#Local timezone to UTC conversion, needed as SiLK is compiled to use UTC
STARTDAY=`date --date "$LOCAL2UTC_STARTDATE" +"%Y/%m/%d"`
ENDDAY=`date --date "$LOCAL2UTC_ENDDATE" +"%Y/%m/%d"`
STARTHOUR=`date --date "$LOCAL2UTC_STARTDATE" +"%H"`
ENDHOUR=`date --date "$LOCAL2UTC_ENDDATE" +"%H"`
echo "UTC Start Time - $LOCAL2UTC_STARTDATE"
echo "UTC End Time - $LOCAL2UTC_ENDDATE"

#Get all the dates we need to process for the month
for d in $(seq $(date -u +%s -d $ENDDAY) -86400 $(date -u +%s -d $STARTDAY)) ;do
        date_list+=(`date -u +%-Y/%-m/%d -d @$d`)
done

RECORDCOUNT="10000"
SERVERNAME=$HOSTNAME
TRAFFICDIRECTION=( 'in,inweb' 'out,outweb' )

FTP_HOST="10.100.0.249"
FTP_USER="netflowftpuser"
FTP_PASSWD="Passw0rd"

#Get all the subnets that SiLK is configured to sense
SUBNETS_STRING=`egrep '^[[:space:]]*ipblocks' ${SENSORCFG} | awk '{ printf $2 "\," }'`
SUBNETS_STRING=${SUBNETS_STRING%?}
IFS=',' read -a SUBNETS <<<"$SUBNETS_STRING"
echo "Found the following subnets:"
for i in "${SUBNETS[@]}"
do
	echo $i
done

#Loop through the dates and process netflow data, then upload to FTP
DAYS=${#date_list[@]}
for UTCDATE in "${date_list[@]}"
do
	echo "Working on $UTCDATE (UTC)"
        YESTERDAYDATETIME="${YEAR}-${MONTH}-${DAYS}"
        echo "Saving output to $YESTERDAYDATETIME"
        echo ""

	#Lets get the 24 hours for the above timezone stored into an array
	hour_list=()
	for i in {0..23} ;do
		FULLUTCDT="${UTCDATE} ${STARTHOUR}"		
	        hour_list+=(`date --date "${FULLUTCDT} +${i} hours" +"%Y/%m/%d:%H"`)
	done

${ENVPATH}/ftp -n $FTP_HOST << END_SCRIPT
quote USER $FTP_USER
quote PASS $FTP_PASSWD
mkdir $YESTERDAYDATETIME
quit
END_SCRIPT

	for DIRECTION in "${TRAFFICDIRECTION[@]}"
	do
		for SUBNET in "${SUBNETS[@]}"
		do
			echo "Processing ${DIRECTION} data for ${UTCDATE} on ${SERVERNAME} for the ${SUBNET} subnet."

		        #Main loop to get data from Netflow to MySQL
			for STARTDATE in "${hour_list[@]}"
		        do
				echo "Working on hour ${STARTDATE}"

				#Convert UTC DateTime to local timezone so we can inject it into the results tables
				UTC_START_DATE=${STARTDATE/:/ } #We replace the double periods with a space so date will recognise the variable as a valid date.
				#Lets check if the DateTime is valid, DST sanity checking.
				date -d "$UTC_START_DATE" 2>: 1>:
				if [ $? -eq '1' ]
				then
					echo "Skipping hour ${c}, looks like DST either ended or started."					
					continue #This will break the for loop and move on to the next hour for processing					
				fi
				LOCAL_DATE=`date -d "$UTC_START_DATE" +"%Y-%m-%d %H:%M:%S"`

				#Format STARTDATE into a filename friendly string
				DT_FILENAME=$(echo $STARTDATE | sed -e 's/\//-/g')
				DT_FILENAME=${DT_FILENAME/:/_}

				#Get last hours transit information and save to a temp file
                                if [ "$DIRECTION" = "in,inweb" ]
                                then
					FILENAME_DIRECTION="in"
					SUBNET_CLEAN=`echo $SUBNET | sed 's/[^a-zA-Z0-9]//g'`
					TEMPFILE="netflow_${FILENAME_DIRECTION}_${SUBNET_CLEAN}_${DT_FILENAME}.csv"
	                                REPORTPATH="/data/tmp/$TEMPFILE"
	                                touch $REPORTPATH

					${LOCALENVPATH}/rwfilter --type=${DIRECTION} --proto=0- --pass=stdout --daddress=${SUBNET} --start-date=$STARTDATE | rwstats --count=$RECORDCOUNT --fields=dIP --values=bytes --temp-directory=/data/tmp --no-titles --no-columns --no-percents --no-final-delimiter --column-separator=',' --output-path=$REPORTPATH

                                        FTP_TEMPFILE="netflow_${FILENAME_DIRECTION}_${SUBNET_CLEAN}_${DT_FILENAME}_ftp.csv"
                                        FTP_TEMPPATH="/data/tmp/$FTP_TEMPFILE"
                                        touch $FTP_TEMPPATH

					${SYSENVPATH}/sed "s/$/,${LOCAL_DATE}/" $REPORTPATH > $FTP_TEMPPATH

					FTP_TEMPFILE2="netflow_${FILENAME_DIRECTION}_${SUBNET_CLEAN}_${DT_FILENAME}_ftp2_${SERVERNAME}.csv"
	                                FTP_TEMPPATH2="/data/tmp/$FTP_TEMPFILE2"
	                                touch $FTP_TEMPPATH2

					string="dIP,bytes,dt"; ${SYSENVPATH}/awk -v n=1 -v s="$string" 'NR == n {print s} {print}' $FTP_TEMPPATH > $FTP_TEMPPATH2;
				elif [ "$DIRECTION" = "out,outweb" ]
				then
					FILENAME_DIRECTION="out"
					SUBNET_CLEAN=`echo $SUBNET | sed 's/[^a-zA-Z0-9]//g'`
					TEMPFILE="netflow_${FILENAME_DIRECTION}_${SUBNET_CLEAN}_${DT_FILENAME}.csv"
	                                REPORTPATH="/data/tmp/$TEMPFILE"
	                                touch $REPORTPATH

					${LOCALENVPATH}/rwfilter --type=${DIRECTION} --proto=0- --pass=stdout --saddress=${SUBNET} --start-date=$STARTDATE | rwstats --count=$RECORDCOUNT --fields=sIP --values=bytes --temp-directory=/data/tmp --no-titles --no-columns --no-percents --no-final-delimiter --column-separator=',' --output-path=$REPORTPATH

					FTP_TEMPFILE="netflow_${FILENAME_DIRECTION}_${SUBNET_CLEAN}_${DT_FILENAME}_ftp.csv"
			                FTP_TEMPPATH="/data/tmp/$FTP_TEMPFILE"
			                touch $FTP_TEMPPATH

					${SYSENVPATH}/sed "s/$/,${LOCAL_DATE}/" $REPORTPATH > $FTP_TEMPPATH

					FTP_TEMPFILE2="netflow_${FILENAME_DIRECTION}_${SUBNET_CLEAN}_${DT_FILENAME}_ftp2_${SERVERNAME}.csv"
                                        FTP_TEMPPATH2="/data/tmp/$FTP_TEMPFILE2"
                                        touch $FTP_TEMPPATH2

					string="sIP,bytes,dt"; ${SYSENVPATH}/awk -v n=1 -v s="$string" 'NR == n {print s} {print}' $FTP_TEMPPATH > $FTP_TEMPPATH2;
                                else
                                        echo "Direction is not set correctly, exiting."
                                        exit 1
                                fi

				#Export to FTP, requires remote processing.
				FILE=$FTP_TEMPPATH2

${ENVPATH}/ftp -n $FTP_HOST << END_SCRIPT
quote USER $FTP_USER
quote PASS $FTP_PASSWD
cd $YESTERDAYDATETIME
binary
put $FILE $FTP_TEMPFILE2
quit
END_SCRIPT

				#Remove any temp files generated above
				${SYSENVPATH}/rm -f /data/tmp/netflow_*
			done
		done
	done
	DAYS=$(( $DAYS-1 ))
done
FINISHTIME=`date +"%Y-%m-%d %H:%M:%S"`
echo "Finished job ${FINISHTIME}"
