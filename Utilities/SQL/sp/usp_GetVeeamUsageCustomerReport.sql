USE [VeeamUsageData]
GO

/****** Object:  StoredProcedure [dbo].[usp_GetVeeamUsageCustomerReport]    Script Date: 12/12/2017 8:24:36 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GetVeeamUsageCustomerReport] @tableName nvarchar(30)
AS
DECLARE @cmd_genReport nvarchar(max)
SET @cmd_genReport = '
SELECT DISTINCT E.jobname, C.[Machine Name], D.CustomerNumber, D.[Customer Name], c.contract_backup_sap, sum(E.totalbackupsize) AS tbs, E.jobtype
FROM assetregisterSQL.dbo.Machines C
inner join assetregisterSQL.dbo.Customers D on D.CustomerNumber = C.CustomerNumber
inner join (select jobtype, datetime, replace(vms, '','', '''') AS vms, totalbackupsize, jobname FROM VeeamUsageData.dbo.' + @tableName + ') E on C.[Machine Name] = E.vms
WHERE D.[Customer Name] != ''ICO'' AND D.[Customer Name] != ''Nexos''
GROUP BY E.jobName, C.[Machine Name], D.CustomerNumber, D.[Customer Name], c.contract_backup_sap, E.jobtype
ORDER BY E.jobName ASC'
EXEC sp_executesql @cmd_genReport
GO

