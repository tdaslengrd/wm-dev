USE [NetflowResultsData]
GO

/****** Object:  StoredProcedure [dbo].[usp_GetTotalUsage]    Script Date: 20/10/2017 10:38:54 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_GetTotalUsage] @Month nvarchar(30)
AS
DECLARE @cmd_genReport nvarchar(max)
SET @cmd_genReport = '
SELECT ROUND(CAST(SUM(E.inbytes) AS Float)/1099511627776,5) AS total_inTBytes, ROUND(CAST(SUM(E.outbytes) AS Float)/1099511627776,5) AS total_outTBytes, ROUND(CAST(SUM(E.inbytes) + SUM(E.outbytes) AS Float)/1099511627776,5) AS total_TBytes
FROM NetflowResultsData.dbo.' + @Month + ' E;'
EXEC sp_executesql @cmd_genReport
GO

