#!/bin/bash

### NetflowBulkExporter Shell Script v2.1 ###

echo "NetflowBulkExporter Shell Script v2.1"

####### NOTES #######
#This script will run once per day, for the previous day results.
#i.e. Set a cron job to run each day at 11pm, this will run the import job for the previous day, between 00:00:00 to 23:59:59.
#####################

#Script variables, dont change unless you know what your doing
LOCALENVPATH=/usr/local/bin
ENVPATH=/usr/bin
SYSENVPATH=/bin
SENSORCFG='/data/sensor.conf'

FOLDERDATE=`date +"%Y-%m-%d"`
DATE=`date +"%Y-%m-%d"`
#Local timezone to UTC conversion, needed as SiLK is compiled to use UTC
UTCDATE=`date --date "${DATE} -10 hours" +"%Y/%m/%d"`

RECORDCOUNT="10000"
SERVERNAME=$HOSTNAME
TRAFFICDIRECTION=( 'in,inweb' 'out,outweb' )

FTP_HOST="10.100.0.249"
FTP_USER="netflowftpuser"
FTP_PASSWD="Passw0rd"

#Get all the subnets that SiLK is configured to sense
SUBNETS_STRING=`egrep '^[[:space:]]*ipblocks' ${SENSORCFG} | awk '{ printf $2 "\," }'`
SUBNETS_STRING=${SUBNETS_STRING%?}
IFS=',' read -a SUBNETS <<<"$SUBNETS_STRING"
echo "Found the following subnets:"
for i in "${SUBNETS[@]}"
do
	echo $i
done

#Process netflow data, then upload to FTP
echo "Working on $UTCDATE (UTC)"
echo "Saving output to $FOLDERDATE"
echo ""

${ENVPATH}/ftp -n $FTP_HOST << END_SCRIPT
quote USER $FTP_USER
quote PASS $FTP_PASSWD
mkdir $FOLDERDATE
quit
END_SCRIPT

for DIRECTION in "${TRAFFICDIRECTION[@]}"
do
	for SUBNET in "${SUBNETS[@]}"
	do
		echo "Processing ${DIRECTION} data for ${UTCDATE} on ${SERVERNAME} for the ${SUBNET} subnet."

		#UTC hours which we need to cycle through for the full day's usage
		HOURS=('14' '15' '16' '17' '18' '19' '20' '21' '22' '23' '0' '1' '2' '3' '4' '5' '6' '7' '8' '9' '10' '11' '12' '13')

	        #Main loop to get data from Netflow to MySQL
		for c in "${HOURS[@]}"
	        do
			#Start and end dates for rwfilter
			STARTDATE="${UTCDATE}:${c}"

			#Convert UTC DateTime to local timezone so we can inject it into the results tables
			UTC_START_DATE=${STARTDATE/:/ } #We replace the double periods with a space so date will recognise the variable as a valid date.
			LOCAL_DATE=`date -d "$UTC_START_DATE" +"%Y-%m-%d %H:%M:%S"`

			echo "Working on hour ${c}"

			#Get last hours transit information and save to a temp file
                        if [ "$DIRECTION" = "in,inweb" ]
                        then
				FILENAME_DIRECTION="in"
				SUBNET_CLEAN=`echo $SUBNET | sed 's/[^a-zA-Z0-9]//g'`
				TEMPFILE="netflow_${FILENAME_DIRECTION}_${SUBNET_CLEAN}_${FOLDERDATE}.csv"
	                        REPORTPATH="/data/tmp/$TEMPFILE"
	                        touch $REPORTPATH

				${LOCALENVPATH}/rwfilter --type=${DIRECTION} --proto=0- --pass=stdout --daddress=${SUBNET} --start-date=$STARTDATE | ${LOCALENVPATH}/rwstats --count=$RECORDCOUNT --fields=dIP --values=bytes --temp-directory=/data/tmp --no-titles --no-columns --no-percents --no-final-delimiter --column-separator=',' --output-path=$REPORTPATH

                                FTP_TEMPFILE="netflow_${FILENAME_DIRECTION}_${SUBNET_CLEAN}_${FOLDERDATE}_${c}_ftp.csv"
                                FTP_TEMPPATH="/data/tmp/$FTP_TEMPFILE"
                                touch $FTP_TEMPPATH

				${SYSENVPATH}/sed "s/$/,${LOCAL_DATE}/" $REPORTPATH > $FTP_TEMPPATH

				FTP_TEMPFILE2="netflow_${FILENAME_DIRECTION}_${SUBNET_CLEAN}_${FOLDERDATE}_${c}_ftp2_${SERVERNAME}.csv"
                                FTP_TEMPPATH2="/data/tmp/$FTP_TEMPFILE2"
                                touch $FTP_TEMPPATH2

				string="dIP,bytes,dt"; ${SYSENVPATH}/awk -v n=1 -v s="$string" 'NR == n {print s} {print}' $FTP_TEMPPATH > $FTP_TEMPPATH2;
			elif [ "$DIRECTION" = "out,outweb" ]
			then
				FILENAME_DIRECTION="out"
				SUBNET_CLEAN=`echo $SUBNET | sed 's/[^a-zA-Z0-9]//g'`
				TEMPFILE="netflow_${FILENAME_DIRECTION}_${SUBNET_CLEAN}_${FOLDERDATE}.csv"
                                REPORTPATH="/data/tmp/$TEMPFILE"
                                touch $REPORTPATH

				${LOCALENVPATH}/rwfilter --type=${DIRECTION} --proto=0- --pass=stdout --saddress=${SUBNET} --start-date=$STARTDATE | ${LOCALENVPATH}/rwstats --count=$RECORDCOUNT --fields=sIP --values=bytes --temp-directory=/data/tmp --no-titles --no-columns --no-percents --no-final-delimiter --column-separator=',' --output-path=$REPORTPATH

				FTP_TEMPFILE="netflow_${FILENAME_DIRECTION}_${SUBNET_CLEAN}_${FOLDERDATE}_${c}_ftp.csv"
			        FTP_TEMPPATH="/data/tmp/$FTP_TEMPFILE"
			        touch $FTP_TEMPPATH

				${SYSENVPATH}/sed "s/$/,${LOCAL_DATE}/" $REPORTPATH > $FTP_TEMPPATH

				FTP_TEMPFILE2="netflow_${FILENAME_DIRECTION}_${SUBNET_CLEAN}_${FOLDERDATE}_${c}_ftp2_${SERVERNAME}.csv"
                                FTP_TEMPPATH2="/data/tmp/$FTP_TEMPFILE2"
                                touch $FTP_TEMPPATH2

				string="sIP,bytes,dt"; ${SYSENVPATH}/awk -v n=1 -v s="$string" 'NR == n {print s} {print}' $FTP_TEMPPATH > $FTP_TEMPPATH2;
                        else
                                echo "Direction is not set correctly, exiting."
                                exit 1
                        fi

			#Export to FTP, requires remote processing.
			FILE=$FTP_TEMPPATH2

${ENVPATH}/ftp -n $FTP_HOST << END_SCRIPT
quote USER $FTP_USER
quote PASS $FTP_PASSWD
cd $FOLDERDATE
binary
put $FILE $FTP_TEMPFILE2
quit
END_SCRIPT

			#Remove any temp files generated above
			${SYSENVPATH}/rm -f /data/tmp/netflow_*
		done
	done
done
FINISHTIME=`date +"%Y-%m-%d %H:%M:%S"`
echo "Finished job ${FINISHTIME}"
