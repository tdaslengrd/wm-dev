﻿using log4net;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Microsoft.Store.PartnerCenter;
using Microsoft.Store.PartnerCenter.Extensions;
using Microsoft.Store.PartnerCenter.Models.Auditing;
using Microsoft.Store.PartnerCenter.Models.Invoices;
using Microsoft.Store.PartnerCenter.Models.Query;
using Microsoft.Store.PartnerCenter.Models.ServiceCosts;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NtpClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using TDServicesApp.Models;

namespace TDServicesApp.Controllers
{
    [Authorize(Roles = "Admin, Demo")]
    public class CloudController : GlobalController
    {
        public ActionResult MSCSPRateCard()
        {
            IAggregatePartner partner = GetPartnerCenterTokenUsingAppCredentials();
            var model = new OfferView();
            List<object> azureRateCardAU = new List<object>();

            // Get the rate card (aka price list)
            var azureRateCard = partner.RateCards.Azure.Get();

            // Add columns to the DataTable object
            model.rateCardAU = new DataTable();
            model.rateCardAU.Columns.Add("Catagory", typeof(string));
            model.rateCardAU.Columns.Add("EffectiveDate", typeof(string));
            model.rateCardAU.Columns.Add("Id", typeof(string));
            model.rateCardAU.Columns.Add("IncludedQuantity", typeof(string));
            model.rateCardAU.Columns.Add("Names", typeof(string));
            model.rateCardAU.Columns.Add("Rates", typeof(string));
            model.rateCardAU.Columns.Add("Region", typeof(string));
            model.rateCardAU.Columns.Add("SubCatagory", typeof(string));
            model.rateCardAU.Columns.Add("Tags", typeof(string));
            model.rateCardAU.Columns.Add("Unit", typeof(string));

            foreach (var meter in azureRateCard.Meters)
            {
                if (meter.Region.StartsWith("AU"))
                {
                    azureRateCardAU.Add(meter);
                    var effectivedate = meter.EffectiveDate.ToString();
                    var rates = (String.Join(", ", meter.Rates.ToArray()).ToString());
                    var tags = (String.Join(", ", meter.Tags.ToArray()).ToString());
                    model.rateCardAU.Rows.Add(meter.Category, effectivedate, meter.Id, meter.IncludedQuantity, meter.Name, rates, meter.Region, meter.Subcategory, tags, meter.Unit);
                }
            }

            // Get all partner invoices
            var pagedInvoiceCollection = partner.Invoices.Query(QueryFactory.Instance.BuildIndexedQuery(5, 0));

            model.getDate = DateTime.Now.ToLocalTime();
            model.rateCardAUCount = azureRateCardAU.Count();

            return View(model);
        }

        public ActionResult MSCSPOffers()
        {
            IAggregatePartner partner = GetPartnerCenterTokenUsingAppCredentials();
            var model = new OfferView();
            List<object> azureRateCardAU = new List<object>();

            // Add columns to the DataTable object
            model.offerTable = new DataTable();
            model.offerTable.Columns.Add("Offer ID", typeof(string));
            model.offerTable.Columns.Add("Name", typeof(string));
            model.offerTable.Columns.Add("Description", typeof(string));
            model.offerTable.Columns.Add("Prerequisite Offers", typeof(string));
            model.offerTable.Columns.Add("Product ID", typeof(string));
            model.offerTable.Columns.Add("Product Name", typeof(string));
            model.offerTable.Columns.Add("UnitType", typeof(string));

            // Get a list of offers.
            var offers = partner.Offers.ByCountry("AU").Get();
            foreach (var offer in offers.Items)
            {
                var PrerequisiteOffers = String.Join(", ", offer.PrerequisiteOffers.ToArray());
                model.offerTable.Rows.Add(offer.Id, offer.Name, offer.Description, PrerequisiteOffers.ToString(), offer.Product.Id, offer.Product.Name, offer.UnitType);
            }

            // Get the rate card (aka price list)
            var azureRateCard = partner.RateCards.Azure.Get();

            // Add columns to the DataTable object
            model.rateCardAU = new DataTable();
            model.rateCardAU.Columns.Add("Catagory", typeof(string));
            model.rateCardAU.Columns.Add("EffectiveDate", typeof(string));
            model.rateCardAU.Columns.Add("Id", typeof(string));
            model.rateCardAU.Columns.Add("IncludedQuantity", typeof(string));
            model.rateCardAU.Columns.Add("Names", typeof(string));
            model.rateCardAU.Columns.Add("Rates", typeof(string));
            model.rateCardAU.Columns.Add("Region", typeof(string));
            model.rateCardAU.Columns.Add("SubCatagory", typeof(string));
            model.rateCardAU.Columns.Add("Tags", typeof(string));
            model.rateCardAU.Columns.Add("Unit", typeof(string));

            foreach (var meter in azureRateCard.Meters)
            {
                if (meter.Region.StartsWith("AU"))
                {
                    azureRateCardAU.Add(meter);
                    var effectivedate = meter.EffectiveDate.ToString();
                    var rates = (String.Join(", ", meter.Rates.ToArray()).ToString());
                    var tags = (String.Join(", ", meter.Tags.ToArray()).ToString());
                    model.rateCardAU.Rows.Add(meter.Category, effectivedate, meter.Id, meter.IncludedQuantity, meter.Name, rates, meter.Region, meter.Subcategory, tags, meter.Unit);
                }
            }

            model.offerCount = offers.TotalCount;
            model.getDate = DateTime.Now.ToLocalTime();
            model.rateCardAUCount = azureRateCardAU.Count();

            return View(model);
        }

        public ActionResult MSCSPCurrentInvoice()
        {
            IAggregatePartner partner = GetPartnerCenterTokenUsingAppCredentials();
            var model = new OfferView();
            model.officeLineItems = new DataTable();
            model.officeLineItems.Columns.Add("CustomerName");
            model.officeLineItems.Columns.Add("CustomerId");
            model.officeLineItems.Columns.Add("ChargeStartDate");
            model.officeLineItems.Columns.Add("ChargeEndDate");
            model.officeLineItems.Columns.Add("BillingProvider");
            model.officeLineItems.Columns.Add("ChargeType");
            model.officeLineItems.Columns.Add("BillingCycleType");
            model.officeLineItems.Columns.Add("OfferName");
            model.officeLineItems.Columns.Add("OfferId");
            model.officeLineItems.Columns.Add("serviceCost");
            model.azureLineItems = new DataTable();
            model.azureLineItems.Columns.Add("CustomerName");
            model.azureLineItems.Columns.Add("CustomerId");
            model.azureLineItems.Columns.Add("ChargeStartDate");
            model.azureLineItems.Columns.Add("ChargeEndDate");
            model.azureLineItems.Columns.Add("BillingProvider");
            model.azureLineItems.Columns.Add("ChargeType");
            model.azureLineItems.Columns.Add("BillingCycleType");
            model.azureLineItems.Columns.Add("ServiceName");
            model.azureLineItems.Columns.Add("Sku");
            model.azureLineItems.Columns.Add("pretaxCharges");
            model.azureDailyLineItems = new DataTable();
            model.azureDailyLineItems.Columns.Add("CustomerName");
            model.azureDailyLineItems.Columns.Add("CustomerId");
            model.azureDailyLineItems.Columns.Add("ChargeStartDate");
            model.azureDailyLineItems.Columns.Add("ChargeEndDate");
            model.azureDailyLineItems.Columns.Add("BillingProvider");
            model.azureDailyLineItems.Columns.Add("ConsumedQuantity");
            model.azureDailyLineItems.Columns.Add("BillingCycleType");
            model.azureDailyLineItems.Columns.Add("ServiceName");
            model.azureDailyLineItems.Columns.Add("ServiceType");
            model.azureDailyLineItems.Columns.Add("unit");

            // Get the last 5 invoices
            var pagedInvoiceCollection = partner.Invoices.Query(QueryFactory.Instance.BuildIndexedQuery(5, 0));

            // Select the latest invoice
            var latestInvoice = pagedInvoiceCollection.Items.First();
            model.currentCharges = latestInvoice.CurrencySymbol.ToString() + latestInvoice.TotalCharges.ToString() + "(" + latestInvoice.CurrencyCode + ")";
            
            var invoiceOperations = partner.Invoices.ById(latestInvoice.Id.ToString());
            var invoice = invoiceOperations.Get();

            model.officeCosts = 0;
            model.azureCosts = 0;
            model.internalUseAzureCosts = 0;

            foreach (var invoiceDetail in invoice.InvoiceDetails)
            {
                string BillingProvider = invoiceDetail.BillingProvider.ToString();
                string InvoiceLineItemType = invoiceDetail.InvoiceLineItemType.ToString();

                var invoiceLineItemsCollection = invoiceOperations.By(invoiceDetail.BillingProvider, invoiceDetail.InvoiceLineItemType).Get();
                var invoiceLineItemEnumerator = partner.Enumerators.InvoiceLineItems.Create(invoiceLineItemsCollection);

                if (BillingProvider == "Office")
                {
                    while (invoiceLineItemEnumerator.HasValue)
                    {
                        // invoiceLineItemEnumerator.Current contains a collection of line items.
                        model.officeUsage_itemCount = invoiceLineItemEnumerator.Current.TotalCount.ToString();

                        var items = invoiceLineItemEnumerator.Current.Items;

                        foreach (LicenseBasedLineItem item in items)
                        {
                            string custName = item.CustomerName.ToString();
                            string custId = item.CustomerId.ToString();
                            string startDate = item.ChargeStartDate.ToString();
                            string endDate = item.ChargeEndDate.ToString();
                            string billingProvider = item.BillingProvider.ToString();
                            string chargeType = item.ChargeType.ToString();
                            string billingCycleType = item.BillingCycleType.ToString();
                            string offerName = item.OfferName.ToString();
                            string offerId = item.OfferId.ToString();
                            string serviceCost = item.Subtotal.ToString();

                            model.officeCosts += item.Subtotal;

                            model.officeLineItems.Rows.Add(custName, custId, startDate, endDate, billingProvider, chargeType, billingCycleType, offerName, offerId, serviceCost);
                        }

                        // Get the next list of invoice line items.
                        invoiceLineItemEnumerator.Next();
                    }
                }
                else if (BillingProvider == "Azure" && InvoiceLineItemType == "BillingLineItems")
                {
                    while (invoiceLineItemEnumerator.HasValue)
                    {
                        // invoiceLineItemEnumerator.Current contains a collection of line items.
                        model.azureUsage_itemCount = invoiceLineItemEnumerator.Current.TotalCount.ToString();

                        var items = invoiceLineItemEnumerator.Current.Items;

                        foreach (UsageBasedLineItem item in items)
                        {
                            string custName = item.CustomerCompanyName.ToString();
                            string custId = item.CustomerId.ToString();
                            string startDate = item.ChargeStartDate.ToString();
                            string endDate = item.ChargeEndDate.ToString();
                            string billingProvider = item.BillingProvider.ToString();
                            string chargeType = item.ChargeType.ToString();
                            string billingCycleType = item.BillingCycleType.ToString();
                            string serviceName = item.ServiceName.ToString();
                            string Sku = item.Sku.ToString();
                            string pretaxCharges = item.PretaxCharges.ToString();

                            model.azureCosts += item.PretaxCharges;

                            if (item.CustomerCompanyName.ToLower().Contains("avnet") || item.CustomerCompanyName.ToLower().Contains("tech data"))
                            {
                                model.internalUseAzureCosts += item.PretaxCharges;
                            }

                            model.azureLineItems.Rows.Add(custName, custId, startDate, endDate, billingProvider, chargeType, billingCycleType, serviceName, Sku, pretaxCharges);
                        }

                        // Get the next list of invoice line items.
                        invoiceLineItemEnumerator.Next();
                    }
                }
                else
                {
                    while (invoiceLineItemEnumerator.HasValue)
                    {
                        // invoiceLineItemEnumerator.Current contains a collection of line items.
                        model.azureDailyUsage_itemCount = invoiceLineItemEnumerator.Current.TotalCount.ToString();

                        var items = invoiceLineItemEnumerator.Current.Items;

                        foreach (DailyUsageLineItem item in items)
                        {
                            string custName = item.CustomerCompanyName.ToString();
                            string custId = item.CustomerId.ToString();
                            string startDate = item.ChargeStartDate.ToString();
                            string endDate = item.ChargeEndDate.ToString();
                            string billingProvider = item.BillingProvider.ToString();
                            string consumedQuantity = item.ConsumedQuantity.ToString();
                            string billingCycleType = item.BillingCycleType.ToString();
                            string serviceName = item.ServiceName.ToString();
                            string serviceType = item.ServiceType.ToString();
                            string unit = item.Unit.ToString();

                            model.azureDailyLineItems.Rows.Add(custName, custId, startDate, endDate, billingProvider, consumedQuantity, billingCycleType, serviceName, serviceType, unit);
                        }

                        // Get the next list of invoice line items.
                        invoiceLineItemEnumerator.Next();
                    }
                }
            }
            return View(model);
        }

        public ActionResult MSCSPCustomers()
        {
            IAggregatePartner partner = GetPartnerCenterTokenWithUserCredentials();
            var model = new OfferView();
            //List<object> customerList = new List<object>();

            // Add columns to the DataTable object
            model.customerList = new DataTable();
            model.customerList.Columns.Add("CompanyName", typeof(string));
            model.customerList.Columns.Add("Domain", typeof(string));
            model.customerList.Columns.Add("TenantId", typeof(string));
            model.customerList.Columns.Add("CurrentCost", typeof(string));

            var customers = partner.Customers.Get();

            foreach (var customer in customers.Items)
            {
                string currentCost = "$" + MSCSPServiceCostSummary(customer.Id, partner);
                model.customerList.Rows.Add(customer.CompanyProfile.CompanyName, customer.CompanyProfile.Domain, customer.CompanyProfile.TenantId, currentCost);
            }

            return View(model);
        }

        public ActionResult MSCSPPartnerCenterActivityLogs()
        {
            IAggregatePartner partner = GetPartnerCenterTokenWithUserCredentials();
            var model = new OfferView();
            model.CSPActivityLog = new DataTable();
            model.CSPActivityLog.Columns.Add("CustomerName");
            model.CSPActivityLog.Columns.Add("OperationType");
            model.CSPActivityLog.Columns.Add("OperationDate");
            model.CSPActivityLog.Columns.Add("OperationStatus");
            model.CSPActivityLog.Columns.Add("UserPrincipalName");
            model.CSPActivityLog.Columns.Add("ResourceNewValue");
            model.CSPActivityLog.Columns.Add("ResourceOldValue");

            var startDate = new DateTime(DateTime.Now.Year, (DateTime.Now.Month)-3, DateTime.Now.Day);

            //// To retrieve audit records by company name substring (e.g. "bri" matches "Fabrikam, Inc.").
            //var searchSubstring = "bri";
            //var filter = new SimpleFieldFilter(AuditRecordSearchField.CompanyName.ToString(), FieldFilterOperation.Substring, searchSubstring);
            //var auditRecordsPage = partnerOperations.AuditRecords.Query(startDate.Date, query: QueryFactory.Instance.BuildSimpleQuery(filter));

            //// To retrieve audit records by customer ID.
            //var customerId = "0c39d6d5-c70d-4c55-bc02-f620844f3fd1";
            //var filter = new SimpleFieldFilter(AuditRecordSearchField.CustomerId.ToString(), FieldFilterOperation.Equals, customerId);
            //var auditRecordsPage = partnerOperations.AuditRecords.Query(startDate.Date, query: QueryFactory.Instance.BuildSimpleQuery(filter));

            // To retrieve audit records by resource type.
            //int resourceTypeInt = 3; // Subscription Resource. 
            //string searchField = Enum.GetName(typeof(ResourceType), resourceTypeInt);
            //var filter = new SimpleFieldFilter(AuditRecordSearchField.ResourceType.ToString(), FieldFilterOperation.Equals, searchField);
            //var auditRecordsPage = partner.AuditRecords.Query(startDate.Date, query: QueryFactory.Instance.BuildSimpleQuery(filter));

            var auditRecordsPage = partner.AuditRecords.Query(startDate.Date);

            var auditRecordEnumerator = partner.Enumerators.AuditRecords.Create(auditRecordsPage);

            while (auditRecordEnumerator.HasValue)
            {
                // Work with the current page.
                foreach (var c in auditRecordEnumerator.Current.Items)
                {
                    model.CSPActivityLog.Rows.Add(c.CustomerName, c.OperationType, c.OperationDate, c.OperationStatus, c.UserPrincipalName, c.ResourceNewValue, c.ResourceOldValue);
                }

                // Get the next page of audit records.
                auditRecordEnumerator.Next();
            }

            return View(model);
        }

        public ActionResult MicrosoftServiceStatus()
        {
            var model = new OfferView();

            // Get a user Azure AD Token.
            var aadAuthResult = UserLogin(ConfigurationManager.AppSettings["OfficeResourceUrl"]);
            string accessToken = aadAuthResult.AccessTokenType.ToString() + " " + aadAuthResult.AccessToken.ToString();

            var officeStatus_jsonString = string.Empty;
            var azureStatus_jsonString = string.Empty;
            string officeStatus = @"https://manage.office.com/api/v1.0/" + ConfigurationManager.AppSettings["AzureActiveDirectoryId"] + "/ServiceComms/CurrentStatus";
            string azureStatus = "https://www.windowsazurestatus.com/odata/ServiceCurrentIncidents?api-version=1.0";

            HttpWebRequest officeStatusRequest = (HttpWebRequest)WebRequest.Create(officeStatus);
            officeStatusRequest.Headers.Add("Authorization", accessToken);
            using (HttpWebResponse officeStatusResponse = (HttpWebResponse)officeStatusRequest.GetResponse())
            using (Stream officeStatusStream = officeStatusResponse.GetResponseStream())
            using (StreamReader officeStatusReader = new StreamReader(officeStatusStream))
            {
                officeStatus_jsonString = officeStatusReader.ReadToEnd();
            }

            HttpWebRequest azureStatusRequest = (HttpWebRequest)WebRequest.Create(azureStatus);
            ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);
            using (HttpWebResponse azureStatusResponse = (HttpWebResponse)azureStatusRequest.GetResponse())
            using (Stream azureStatusStream = azureStatusResponse.GetResponseStream())
            using (StreamReader azureStatusReader = new StreamReader(azureStatusStream))
            {
                azureStatus_jsonString = azureStatusReader.ReadToEnd();
            }

            model.officeStatus_healthStatus = new DataTable();
            model.officeStatus_healthStatus.Columns.Add("Id");
            model.officeStatus_healthStatus.Columns.Add("WorkloadDisplayName");
            model.officeStatus_healthStatus.Columns.Add("IncidentIds");
            model.officeStatus_healthStatus.Columns.Add("Status");
            model.officeStatus_healthStatus.Columns.Add("FeatureDisplayName");
            model.officeStatus_healthStatus.Columns.Add("FeatureName");
            model.officeStatus_healthStatus.Columns.Add("FeatureServiceStatus");
            model.officeStatus_healthStatus.Columns.Add("FeatureServiceStatusDisplayName");

            var officeStatus_jsonObj = JObject.Parse(officeStatus_jsonString);
            JArray officeStatusValues = (JArray)officeStatus_jsonObj.SelectToken("value");
            foreach (JToken value in officeStatusValues)
            {
                string Id = (string)value.SelectToken("Id");
                string WorkloadDisplayName = (string)value.SelectToken("WorkloadDisplayName");
                string IncidentIds = (string)value.SelectToken("IncidentIds").ToString();
                string Status = (string)value.SelectToken("Status");
                JArray FeatureStatus = (JArray)value.SelectToken("FeatureStatus");
                foreach (JToken feature in FeatureStatus)
                {
                    string FeatureDisplayName = (string)feature.SelectToken("FeatureDisplayName");
                    string FeatureName = (string)feature.SelectToken("FeatureName");
                    string FeatureServiceStatus = (string)feature.SelectToken("FeatureServiceStatus");
                    string FeatureServiceStatusDisplayName = (string)feature.SelectToken("FeatureServiceStatusDisplayName");
                    model.officeStatus_healthStatus.Rows.Add(Id,WorkloadDisplayName,IncidentIds,Status,FeatureDisplayName, FeatureName, FeatureServiceStatus, FeatureServiceStatusDisplayName);
                }
            }

            model.azureStatus_healthStatus = new DataTable();
            model.azureStatus_healthStatus.Columns.Add("Id");
            model.azureStatus_healthStatus.Columns.Add("ServiceName");
            model.azureStatus_healthStatus.Columns.Add("RegionId");
            model.azureStatus_healthStatus.Columns.Add("Name");
            model.azureStatus_healthStatus.Columns.Add("Status");
            model.azureStatus_healthStatus.Columns.Add("Incidents");

            var azureStatus_jsonObj = JObject.Parse(azureStatus_jsonString);
            JArray azureStatusValues = (JArray)azureStatus_jsonObj.SelectToken("value");
            foreach (JToken value in azureStatusValues)
            {
                string ID = (string)value.SelectToken("ID");
                string ServiceName = (string)value.SelectToken("Name");
                JArray Regions = (JArray)value.SelectToken("Regions");
                foreach (JToken region in Regions)
                {
                    string RegionID = (string)region.SelectToken("RegionID");
                    string Name = (string)region.SelectToken("Name");
                    string Status = (string)region.SelectToken("Status");
                    string Incidents = (string)region.SelectToken("Incidents").ToString();
                    model.azureStatus_healthStatus.Rows.Add(ID, ServiceName, RegionID, Name, Status, Incidents);
                }
            }
            return View(model);
        }

        public ActionResult MSCSPDomainAvailability()
        {
            return View(new OfferView());
        }

        [HttpPost]
        public ActionResult MSCSPDomainAvailability(string domainName)
        {
            IAggregatePartner partner = GetPartnerCenterTokenUsingAppCredentials();
            var model = new OfferView();
            if (domainName != null && domainName != "")
            {
                domainName = domainName + ".onmicrosoft.com";
                string domainCheck = MSCSPCheckDomainAvailability(partner, domainName);
                if (domainCheck == "True")
                {
                    model.domainCheck = "Domain name taken!";
                }
                else if (domainCheck == "False")
                {
                    model.domainCheck = "Domain name free!";
                }
                else
                {
                    model.domainCheck = "An error occured - " + domainCheck;
                }
            }
            else
            {
                model.domainCheck = "Enter in a domain name!";
            }

            return View (model);
        }

        public ActionResult MSCSPCustomerUsageBySub() //Needs additional work, currently doesnot display correct consumption figures
        {
            IAggregatePartner partner = GetPartnerCenterTokenWithUserCredentials();
            var model = new OfferView();
            var customers = partner.Customers.Get();

            model.SubTable = new DataTable();
            model.SubTable.Columns.Add("customerName");
            model.SubTable.Columns.Add("customerId");
            model.SubTable.Columns.Add("subscriptionId");
            model.SubTable.Columns.Add("subscriptionName");
            model.SubTable.Columns.Add("subscriptionCost");

            foreach (var customer in customers.Items)
            {
                model.SubscriptionList = GetSubscriptionUsage(partner, customer.Id);
                string customerName = customer.CompanyProfile.CompanyName.ToString();
                string customerId = customer.Id.ToString();
                foreach (var subscription in model.SubscriptionList)
                {
                    string subscriptionId = subscription.subscriptionId.ToString();
                    string subscriptionName = subscription.subscriptionName.ToString();
                    string subscriptionCost = subscription.subscriptionCost.ToString();
                    model.SubTable.Rows.Add(customerName, customerId, subscriptionId, subscriptionName, subscriptionCost);
                }
            }

            return View(model);
        }

        public ActionResult AzureCSPRISKU2SEM()
        {
            var model = new SCMProcessorView();
            return View(model);
        }

        [HttpGet]
        public ActionResult RegionViewUpdate(string selectedRegionName, SCMProcessorView model)
        {
            model.Month = DateTime.Now.ToString("MMMM");
            model.Day = DateTime.Now.Day.ToString();
            model.Year = DateTime.Now.Year.ToString();
            model.regionSelected = selectedRegionName;
            model.currencyList = new DataTable();

            if (selectedRegionName == "APAC")
            {
                model.currencyList = model.currencyList_APAC.Copy();
            }
            else if (selectedRegionName == "AMER")
            {
                model.currencyList = model.currencyList_AMER.Copy();
            }
            else if (selectedRegionName == "EMEA")
            {
                model.currencyList = model.currencyList_EMEA.Copy();
            }
            else
            {
                return View();
            }

            foreach (DataRow currencyItem in model.currencyList.Rows)
            {
                string currencyISOName = currencyItem["CurrencyNameISO"].ToString();
                var currencyRate = GetYahooCurrencyRates(currencyISOName);
                currencyItem["Rate"] = currencyRate.AsEnumerable().Select(dr => dr.Field<string>("Price")).FirstOrDefault().ToString();
            }

            return PartialView("_RegionSelectPartialView", model);
        }

        [HttpPost]
        public ActionResult AzureCSPRISKU2SEM(FormCollection form, SCMProcessorView model)
        {
            if (ModelState.IsValid)
            {
                string regionSelected = model.regionSelected;

                DataTable currencyConversionTable = new DataTable();
                DataTable dtOne = new DataTable(); //Azure RI DataTable
                DataTable dtTwo = new DataTable(); //Office DataTable
                DataTable dtThree = new DataTable(); //Azure DataTable
                DataTable dtAll = new DataTable();

                decimal currencyRate = 0;
                if (regionSelected == "APAC")
                {
                    //Custom rates from user
                    //APAC
                    decimal vnd_rate = 0;
                    try { vnd_rate = Convert.ToDecimal(form["currencyNameLookup_Vietnam"]); currencyRate = vnd_rate; } catch { vnd_rate = Convert.ToDecimal(model.currencyRate_Live); };
                }
                else if (regionSelected == "AMER")
                {
                    //Custom rates from user
                    //AMER
                    decimal usd_rate = 0;
                    decimal cad_rate = 0;
                    try { usd_rate = Convert.ToDecimal(form["currencyNameLookup_United States"]); } catch { };
                    try { cad_rate = Convert.ToDecimal(form["currencyNameLookup_Canada"]); } catch { };
                    ViewBag.Error = "Were still building this out, but expect this to be completed soon.";
                    return View(model);
                }
                else if (regionSelected == "EMEA")
                {
                    //Custom rates from user
                    //EMEA
                    decimal try_rate = 0;
                    try { try_rate = Convert.ToDecimal(form["currencyNameLookup_Turkey"]); } catch { };
                    ViewBag.Error = "Were still building this out, but expect this to be completed soon.";
                    return View(model);
                }
                else
                {
                    ViewBag.Error = "Region selected could not be identified.";
                    return View(model);
                }

                if (model.RI_file != null && model.RI_file.ContentLength > 0) //RI file not uploaded
                {
                    ViewBag.Error = "No RI file selected!";
                    dtTwo = AzureCSPOffice2SEM_Processor(model.Office_file, currencyRate);
                }
                else if (model.Office_file != null && model.Office_file.ContentLength > 0) //Office file not uploaded
                {
                    ViewBag.Error = "No Office file selected!";
                    dtOne = AzureCSPRISKU2SEM_Processor(model.RI_file, currencyRate);
                }
                else if (model.RI_file != null && model.RI_file.ContentLength > 0 && model.Office_file != null && model.Office_file.ContentLength > 0) //Both files not uploaded
                {
                    dtOne = AzureCSPRISKU2SEM_Processor(model.RI_file, currencyRate);
                    dtTwo = AzureCSPOffice2SEM_Processor(model.Office_file, currencyRate);
                }
                else
                {
                    ViewBag.Error = "No files uploaded!";
                    return View(model);
                }

                if (dtOne == null)
                {
                    dtAll = dtTwo;
                }
                else if (dtTwo == null)
                {
                    dtAll = dtOne;
                }
                else
                {
                    dtAll = dtOne.Copy();
                    dtAll.Merge(dtTwo);
                }

                if (dtAll != null)
                {
                    byte[] outputBuffer = null;
                    using (MemoryStream tempStream = new MemoryStream())
                    {
                        using (StreamWriter writer = new StreamWriter(tempStream))
                        {
                            Rfc4180Writer.WriteDataTable(dtAll, writer, true);
                        }

                        outputBuffer = tempStream.ToArray();
                    }
                    string currentDT = DateTime.Now.ToString("yyyyMMdd");
                    return File(outputBuffer, "text/csv", "SEMImport-" + currentDT + ".csv");
                }
                else
                {
                    ViewBag.Error = "The final table was not able to be generated, send an email to support@techdatacloud.com.au with the files you submitted so we can check it out.";
                }
            }
            else
            {
                ViewBag.Error = "There were issues validating the submitted information, try again and if the problem persists contact support@techdatacloud.com.au.";
            }
            return View(model);
        }

        public static DataTable GetYahooCurrencyRates(string currencyNameLookup)
        {
            string url = "http://finance.yahoo.com/webservice/" + "v1/symbols/allcurrencies/quote?format=xml";
            string search = "Name Like '" + currencyNameLookup + "'";
            DataTable currencyTable = new DataTable();
            currencyTable.Columns.Add("Name");
            currencyTable.Columns.Add("Price");
            currencyTable.Columns.Add("Inverse");
            XmlDocument xml = new XmlDocument();

            try
            {
                xml.Load(url);
                XmlNodeList xnList = xml.SelectNodes("/list/resources/resource");

                foreach (XmlNode xNode in xnList)
                {
                    var test = xNode;
                    if (test.InnerText != "")
                    {
                        const string name_query = "descendant::field[@name='name']";
                        const string price_query = "descendant::field[@name='price']";
                        string name = test.SelectSingleNode(name_query).InnerText;
                        string price = test.SelectSingleNode(price_query).InnerText;
                        decimal inverse = 1m / decimal.Parse(price);

                        currencyTable.Rows.Add(name, price, inverse.ToString("f6"));
                    }
                }

                // Sort by currency name from A to Z
                currencyTable.DefaultView.Sort = "Name ASC";

                if (currencyNameLookup == "") //Get full datatable of all currencies
                {
                    return currencyTable;
                }
                else //Get a specific currency
                {
                    DataRow firstRow = currencyTable.Select(search).FirstOrDefault();
                    DataTable currencyTable_temp = new DataTable();
                    currencyTable_temp = currencyTable.Clone();
                    currencyTable_temp.Rows.Add(firstRow.ItemArray[0], firstRow.ItemArray[1], firstRow.ItemArray[2]);
                    return currencyTable_temp;
                }
            }
            catch (Exception ex)
            {
                string error = ex.InnerException.ToString();
            }
            return currencyTable;
        }

        public DataTable AzureCSPRISKU2SEM_Processor(HttpPostedFileBase file, decimal currencyRate)
        {
            //Create the expected table format for SEM ingest within a DataTable
            DataTable SEMTableFormat = new DataTable();
            SEMTableFormat.Columns.Add("product_name");
            SEMTableFormat.Columns.Add("product_id");
            SEMTableFormat.Columns.Add("product_type");
            SEMTableFormat.Columns.Add("license_name");
            SEMTableFormat.Columns.Add("license_sku");
            SEMTableFormat.Columns.Add("part_number");
            SEMTableFormat.Columns.Add("retail_pricing");
            SEMTableFormat.Columns.Add("billing_type");
            SEMTableFormat.Columns.Add("license_type");
            SEMTableFormat.Columns.Add("prerequisite_products");
            SEMTableFormat.Columns.Add("licensing_type");
            SEMTableFormat.Columns.Add("add_on_group");
            SEMTableFormat.Columns.Add("qty_rule");
            SEMTableFormat.Columns.Add("qty_min"); //Default at 1
            SEMTableFormat.Columns.Add("qty_max");
            SEMTableFormat.Columns.Add("user_qty_count"); //Default at 1
            SEMTableFormat.Columns.Add("sku_status"); //Default at On
            SEMTableFormat.Columns.Add("listing_status"); //Defailt at Active
            SEMTableFormat.Columns.Add("renewal_sku");
            SEMTableFormat.Columns.Add("subscription_version");
            SEMTableFormat.Columns.Add("trial"); //Default at Off
            SEMTableFormat.Columns.Add("trial_days"); //Always empty
            SEMTableFormat.Columns.Add("trial_upgrade_sku");
            SEMTableFormat.Columns.Add("downloadable_app_file");
            SEMTableFormat.Columns.Add("hardware_platform");
            SEMTableFormat.Columns.Add("license_group");
            SEMTableFormat.Columns.Add("base_cost");
            SEMTableFormat.Columns.Add("use_base_cost"); //Default at TRUE
            SEMTableFormat.Columns.Add("setup_fee");
            SEMTableFormat.Columns.Add("setup_fee_part_no");
            SEMTableFormat.Columns.Add("shipping_fee");
            SEMTableFormat.Columns.Add("shipping_fee_part_no");
            SEMTableFormat.Columns.Add("shipping_description");
            SEMTableFormat.Columns.Add("sku_description");
            SEMTableFormat.Columns.Add("contract_length");
            SEMTableFormat.Columns.Add("locale_currency");
            SEMTableFormat.Columns.Add("prerequisite_license");
            SEMTableFormat.Columns.Add("allow_additional_users");
            SEMTableFormat.Columns.Add("additional_user_cost");
            SEMTableFormat.Columns.Add("additional_user_base");
            SEMTableFormat.Columns.Add("allow_additional_usage");
            SEMTableFormat.Columns.Add("additional_usage_cost");
            SEMTableFormat.Columns.Add("additional_usage_base");
            SEMTableFormat.Columns.Add("additional_use_partnumber");
            SEMTableFormat.Columns.Add("setupfee_base");
            SEMTableFormat.Columns.Add("shipping_base");
            SEMTableFormat.Columns.Add("td_sku");
            SEMTableFormat.Columns.Add("upfront_part_number");
            SEMTableFormat.Columns.Add("arrears_part_number");
            SEMTableFormat.Columns.Add("is_zser");
            SEMTableFormat.Columns.Add("td_vendor_id");
            SEMTableFormat.Columns.Add("purchase_org");
            SEMTableFormat.Columns.Add("purchase_group");
            SEMTableFormat.Columns.Add("seat_change_part_no");
            SEMTableFormat.Columns.Add("recurring_part_no");
            SEMTableFormat.Columns.Add("provisioning_part_number");
            SEMTableFormat.Columns.Add("incompatible_products");

            //File to CSV to DataTable variable
            DataTable dt = new DataTable();

            //General variables
            decimal currency = 0;
            decimal local_currency = 0;

            //Datatable variables
            string product_name = "";
            string product_id = "";
            string product_type = "";
            string license_name = "";
            string license_sku = "";
            string part_number = "";
            string retail_pricing = "";
            string billing_type = "";
            string license_type = "";
            string prerequisite_products = "";
            string licensing_type = "";
            string add_on_group = "";
            string qty_rule = "";
            string qty_min = "1"; //Default at 1
            string qty_max = "";
            string user_qty_count = "1"; //Default at 1
            string sku_status = "On"; //Default at On
            string listing_status = "Active"; //Defailt at Active
            string renewal_sku = "";
            string subscription_version = "";
            string trial = "Off"; //Default at Off
            string trial_days = ""; //Always empty
            string trial_upgrade_sku = "";
            string downloadable_app_file = "";
            string hardware_platform = "";
            string license_group = "";
            string base_cost = "";
            string use_base_cost = "TRUE"; //Default at TRUE
            string setup_fee = "";
            string setup_fee_part_no = "";
            string shipping_fee = "";
            string shipping_fee_part_no = "";
            string shipping_description = "";
            string sku_description = "";
            string contract_length = "";
            string locale_currency = "";
            string prerequisite_license = "";
            string allow_additional_users = "";
            string additional_user_cost = "";
            string additional_user_base = "";
            string allow_additional_usage = "";
            string additional_usage_cost = "";
            string additional_usage_base = "";
            string additional_use_partnumber = "";
            string setupfee_base = "";
            string shipping_base = "";
            string td_sku = "";
            string upfront_part_number = "";
            string arrears_part_number = "";
            string is_zser = "";
            string td_vendor_id = "";
            string purchase_org = "";
            string purchase_group = "";
            string seat_change_part_no = "";
            string recurring_part_no = "";
            string provisioning_part_number = "";
            string incompatible_products = "";

            if (file != null && file.ContentLength > 0)
            {
                string extension = System.IO.Path.GetExtension(file.FileName).ToLower();
                string connString = "";

                string[] validFileTypes = { ".xls", ".xlsx", ".csv" };

                var fileName = Path.GetFileName(file.FileName);
                string path1 = string.Format("{0}/{1}", Server.MapPath("~/Content/Uploads"), fileName);
                string tableName = "RI Pricelist_Partner-commercial";
                string cellRange = ""; //For example, cells a4 to c7 => a4:c7
                string processHeaders = "Yes";
                if (!Directory.Exists(path1))
                {
                    Directory.CreateDirectory(Server.MapPath("~/Content/Uploads"));
                }
                if (validFileTypes.Contains(extension))
                {
                    if (System.IO.File.Exists(path1))
                    { System.IO.File.Delete(path1); }
                    file.SaveAs(path1);
                    if (extension == ".csv")
                    {
                        dt = Utility.ConvertCSVtoDataTable(path1);
                    }
                    //Connection String to Excel Workbook  
                    else if (extension.Trim() == ".xls")
                    {
                        connString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path1 + ";Extended Properties=\"Excel 8.0;HDR=" + processHeaders + ";IMEX=2\"";
                        dt = Utility.ConvertXSLXtoDataTable(path1, connString, tableName, cellRange);
                    }
                    else if (extension.Trim() == ".xlsx")
                    {
                        connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path1 + ";Extended Properties=\"Excel 12.0;HDR=" + processHeaders + ";IMEX=2\"";
                        dt = Utility.ConvertXSLXtoDataTable(path1, connString, tableName, cellRange);
                    }
                }
                //Loop through each row in the DataTable extracting items and importing them into the SEM DataTable

                //RULES
                // 3 records for each SKU, Australia, India, Vietnam (ONLY AUSTRALIA AND VIETNAME NOW)
                // The records for India which contains base_price are calculated from the USD price, i.e. conversion is required (NOT AVAILIABLE YET)

                //MAP
                // product_type set to 'iaas'
                // license_name set to Sku Title
                // part_number set to ProductId
                // billing_type set to Duration

                //PROCESS
                // Loop throught each DataRow in the CSV datatable
                // Read item and copy valued into new datatable for new DataRow based on Map above
                // For Vietname, convert USD into Dong
                // For Austrlia copy the AUD price

                foreach (DataRow row in dt.Rows)
                {
                    //These country codes are generated from SAP
                    string[] countryIds = { "36", "38" }; //36 is Australia, 38 is Vietnam
                    string test69 = string.Empty;
                    foreach (string countryId in countryIds)
                    {
                        if (countryId == "38")
                        {
                            test69 = row["USD"].ToString();
                            currency = Convert.ToDecimal(test69);
                            local_currency = (currency * currencyRate);
                        }
                        else
                        {
                            test69 = row["AUD"].ToString();
                            local_currency = Convert.ToDecimal(test69);
                        }

                        product_name = "Azure Reserved Instance"; //Specifically for the RI report
                        product_type = "iaas"; //Specifically for the RI report
                        license_name = row["Sku Title"].ToString();
                        part_number = row["ProductId"].ToString();
                        billing_type = row["Duration"].ToString();
                        base_cost = local_currency.ToString();
                        contract_length = row["Duration"].ToString();
                        locale_currency = countryId;

                        SEMTableFormat.Rows.Add(product_name, product_id, product_type, license_name, license_sku, part_number, retail_pricing, billing_type, license_type, prerequisite_products, licensing_type, add_on_group, qty_rule, qty_min, qty_max, user_qty_count, sku_status, listing_status, renewal_sku, subscription_version, trial, trial_days, trial_upgrade_sku, downloadable_app_file, hardware_platform, license_group, base_cost, use_base_cost, setup_fee, setup_fee_part_no, shipping_fee, shipping_fee_part_no, shipping_description, sku_description, contract_length, locale_currency, prerequisite_license, allow_additional_users, additional_user_cost, additional_user_base, allow_additional_usage, additional_usage_cost, additional_usage_base, additional_use_partnumber, setupfee_base, shipping_base, td_sku, upfront_part_number, arrears_part_number, is_zser, td_vendor_id, purchase_org, purchase_group, seat_change_part_no, recurring_part_no, provisioning_part_number, incompatible_products);
                    }
                }
                //return RedirectToAction("AzureCSPRISKU2SEM");
                return SEMTableFormat;
            }

            return SEMTableFormat;
        }

        public DataTable AzureCSPOffice2SEM_Processor(HttpPostedFileBase file, decimal currencyRate)
        {
            //Create the expected table format for SEM ingest within a DataTable
            DataTable SEMTableFormat = new DataTable();
            SEMTableFormat.Columns.Add("product_name");
            SEMTableFormat.Columns.Add("product_id");
            SEMTableFormat.Columns.Add("product_type");
            SEMTableFormat.Columns.Add("license_name");
            SEMTableFormat.Columns.Add("license_sku");
            SEMTableFormat.Columns.Add("part_number");
            SEMTableFormat.Columns.Add("retail_pricing");
            SEMTableFormat.Columns.Add("billing_type");
            SEMTableFormat.Columns.Add("license_type");
            SEMTableFormat.Columns.Add("prerequisite_products");
            SEMTableFormat.Columns.Add("licensing_type");
            SEMTableFormat.Columns.Add("add_on_group");
            SEMTableFormat.Columns.Add("qty_rule");
            SEMTableFormat.Columns.Add("qty_min"); //Default at 1
            SEMTableFormat.Columns.Add("qty_max");
            SEMTableFormat.Columns.Add("user_qty_count"); //Default at 1
            SEMTableFormat.Columns.Add("sku_status"); //Default at On
            SEMTableFormat.Columns.Add("listing_status"); //Defailt at Active
            SEMTableFormat.Columns.Add("renewal_sku");
            SEMTableFormat.Columns.Add("subscription_version");
            SEMTableFormat.Columns.Add("trial"); //Default at Off
            SEMTableFormat.Columns.Add("trial_days"); //Always empty
            SEMTableFormat.Columns.Add("trial_upgrade_sku");
            SEMTableFormat.Columns.Add("downloadable_app_file");
            SEMTableFormat.Columns.Add("hardware_platform");
            SEMTableFormat.Columns.Add("license_group");
            SEMTableFormat.Columns.Add("base_cost");
            SEMTableFormat.Columns.Add("use_base_cost"); //Default at TRUE
            SEMTableFormat.Columns.Add("setup_fee");
            SEMTableFormat.Columns.Add("setup_fee_part_no");
            SEMTableFormat.Columns.Add("shipping_fee");
            SEMTableFormat.Columns.Add("shipping_fee_part_no");
            SEMTableFormat.Columns.Add("shipping_description");
            SEMTableFormat.Columns.Add("sku_description");
            SEMTableFormat.Columns.Add("contract_length");
            SEMTableFormat.Columns.Add("locale_currency");
            SEMTableFormat.Columns.Add("prerequisite_license");
            SEMTableFormat.Columns.Add("allow_additional_users");
            SEMTableFormat.Columns.Add("additional_user_cost");
            SEMTableFormat.Columns.Add("additional_user_base");
            SEMTableFormat.Columns.Add("allow_additional_usage");
            SEMTableFormat.Columns.Add("additional_usage_cost");
            SEMTableFormat.Columns.Add("additional_usage_base");
            SEMTableFormat.Columns.Add("additional_use_partnumber");
            SEMTableFormat.Columns.Add("setupfee_base");
            SEMTableFormat.Columns.Add("shipping_base");
            SEMTableFormat.Columns.Add("td_sku");
            SEMTableFormat.Columns.Add("upfront_part_number");
            SEMTableFormat.Columns.Add("arrears_part_number");
            SEMTableFormat.Columns.Add("is_zser");
            SEMTableFormat.Columns.Add("td_vendor_id");
            SEMTableFormat.Columns.Add("purchase_org");
            SEMTableFormat.Columns.Add("purchase_group");
            SEMTableFormat.Columns.Add("seat_change_part_no");
            SEMTableFormat.Columns.Add("recurring_part_no");
            SEMTableFormat.Columns.Add("provisioning_part_number");
            SEMTableFormat.Columns.Add("incompatible_products");

            //File to CSV to DataTable variable
            DataTable dt = new DataTable();

            //General variables
            decimal list_price = 0;
            decimal erp_price = 0;

            //Datatable variables
            string product_name = "";
            string product_id = "";
            string product_type = "";
            string license_name = "";
            string license_sku = "";
            string part_number = "";
            string retail_pricing = "";
            string billing_type = "";
            string license_type = "";
            string prerequisite_products = "";
            string licensing_type = "";
            string add_on_group = "";
            string qty_rule = "";
            string qty_min = "1"; //Default at 1
            string qty_max = "";
            string user_qty_count = "1"; //Default at 1
            string sku_status = "On"; //Default at On
            string listing_status = "Active"; //Defailt at Active
            string renewal_sku = "";
            string subscription_version = "";
            string trial = "Off"; //Default at Off
            string trial_days = ""; //Always empty
            string trial_upgrade_sku = "";
            string downloadable_app_file = "";
            string hardware_platform = "";
            string license_group = "";
            string base_cost = "";
            string use_base_cost = "TRUE"; //Default at TRUE
            string setup_fee = "";
            string setup_fee_part_no = "";
            string shipping_fee = "";
            string shipping_fee_part_no = "";
            string shipping_description = "";
            string sku_description = "";
            string contract_length = "";
            string locale_currency = "";
            string prerequisite_license = "";
            string allow_additional_users = "";
            string additional_user_cost = "";
            string additional_user_base = "";
            string allow_additional_usage = "";
            string additional_usage_cost = "";
            string additional_usage_base = "";
            string additional_use_partnumber = "";
            string setupfee_base = "";
            string shipping_base = "";
            string td_sku = "";
            string upfront_part_number = "";
            string arrears_part_number = "";
            string is_zser = "";
            string td_vendor_id = "";
            string purchase_org = "";
            string purchase_group = "";
            string seat_change_part_no = "";
            string recurring_part_no = "";
            string provisioning_part_number = "";
            string incompatible_products = "";

            if (file != null && file.ContentLength > 0)
            {
                string extension = System.IO.Path.GetExtension(file.FileName).ToLower();
                string connString = "";

                string[] validFileTypes = { ".xls", ".xlsx", ".csv" };

                var fileName = Path.GetFileName(file.FileName);
                string path1 = string.Format("{0}/{1}", Server.MapPath("~/Content/Uploads"), fileName);
                //string tableName = "AUD";
                string[] tableNames = { "AUD", "INR", "USD" }; //We need 3 tabled, AUD, INR and USD (USD is for Vietname)
                DataTable stageTable = new DataTable(); //This is used a temp table to concat all result tables from the tableNames array
                string cellRange = ""; //For example, cells a4 to c7 => a4:c7
                string processHeaders = "No";
                if (!Directory.Exists(path1))
                {
                    Directory.CreateDirectory(Server.MapPath("~/Content/Uploads"));
                }
                if (validFileTypes.Contains(extension))
                {
                    if (System.IO.File.Exists(path1))
                    { System.IO.File.Delete(path1); }
                    file.SaveAs(path1);
                    if (extension == ".csv")
                    {
                        dt = Utility.ConvertCSVtoDataTable(path1);
                    }
                    //Connection String to Excel Workbook  
                    else if (extension.Trim() == ".xls")
                    {
                        foreach (string tableName in tableNames)
                        {
                            connString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path1 + ";Extended Properties=\"Excel 8.0;HDR=" + processHeaders + ";IMEX=2\"";
                            dt = Utility.ConvertXSLXtoDataTable(path1, connString, tableName, cellRange);

                            //The first 2 rows in the imported file are ommitted, so lets get the first two rows and delete them
                            foreach (DataRow row in dt.AsEnumerable().Take(2).ToList())
                            {
                                row.Delete();
                            }
                            dt.AcceptChanges();

                            //Lets rename each column with the values found in the first row the delete the non required row
                            foreach (DataColumn column in dt.Columns)
                            {
                                string cName = dt.Rows[0][column.ColumnName].ToString();
                                if (!dt.Columns.Contains(cName) && cName != "")
                                {
                                    column.ColumnName = cName;
                                }
                            }
                            foreach (DataRow row in dt.AsEnumerable().Take(1).ToList())
                            {
                                row.Delete();
                            }
                            dt.AcceptChanges();

                            // Lets create a new column which contains the currency type then add it to the datatable
                            var newColumn = new DataColumn("Currency", typeof(string));
                            newColumn.DefaultValue = tableName;
                            dt.Columns.Add(newColumn);

                            // Final merge with staging datatable then clear the original datatable
                            stageTable.Merge(dt);
                            dt.Clear();
                        }
                        dt = stageTable.Copy();
                    }
                    else if (extension.Trim() == ".xlsx")
                    {
                        foreach (string tableName in tableNames)
                        {
                            connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path1 + ";Extended Properties=\"Excel 12.0;HDR=" + processHeaders + ";IMEX=2\"";
                            dt = Utility.ConvertXSLXtoDataTable(path1, connString, tableName, cellRange);

                            //The first 2 rows in the imported file are ommitted, so lets get the first two rows and delete them
                            foreach (DataRow row in dt.AsEnumerable().Take(2).ToList())
                            {
                                row.Delete();
                            }
                            dt.AcceptChanges();

                            //Lets rename each column with the values found in the first row the delete the non required row
                            foreach (DataColumn column in dt.Columns)
                            {
                                string cName = dt.Rows[0][column.ColumnName].ToString();
                                if (!dt.Columns.Contains(cName) && cName != "")
                                {
                                    column.ColumnName = cName;
                                }
                            }
                            foreach (DataRow row in dt.AsEnumerable().Take(1).ToList())
                            {
                                row.Delete();
                            }
                            dt.AcceptChanges();

                            // Lets create a new column which contains the currency type then add it to the datatable
                            var newColumn = new DataColumn("Currency", typeof(string));
                            newColumn.DefaultValue = tableName;
                            dt.Columns.Add(newColumn);

                            // Final merge with staging datatable then clear the original datatable
                            stageTable.Merge(dt);
                            dt.Clear();
                        }
                        dt = stageTable.Copy();
                    }
                    //Loop through each row in the DataTable extracting items and importing them into the SEM DataTable

                    //RULES
                    // 3 records for each SKU, Australia, India, Vietnam (ONLY AUSTRALIA AND VIETNAME NOW)
                    // The records for India which contains base_price are calculated from the USD price, i.e. conversion is required (NOT AVAILIABLE YET)

                    //MAP
                    // product_type set to 'iaas'
                    // license_name set to Sku Title
                    // part_number set to ProductId
                    // billing_type set to Duration

                    //PROCESS
                    // Loop throught each DataRow in the CSV datatable
                    // Read item and copy valued into new datatable for new DataRow based on Map above
                    // For Vietname, convert USD into Dong
                    // For Austrlia copy the AUD price

                    //We need to name columns 9 and 10, List Price and ERP Price respectivly.
                    dt.Columns[9].ColumnName = "List Price";
                    dt.Columns[10].ColumnName = "ERP Price";

                    foreach (DataRow row in dt.Rows)
                    {
                        //There are rows which have no information, so we will skip them.
                        if (row["Offer Display Name"].ToString() != "")
                        {
                            //These country codes are generated from SAP
                            string[] countryIds = { "35", "36", "38" }; //35 is India, 36 is Australia, 38 is Vietnam
                            string currencyType = string.Empty;
                            string listPrice = string.Empty;
                            string erpPrice = string.Empty;
                            foreach (string countryId in countryIds)
                            {
                                currencyType = row["Currency"].ToString();
                                if (countryId == "38" && currencyType == "USD") //Vietnam
                                {
                                    listPrice = row["List Price"].ToString();
                                    erpPrice = row["ERP Price"].ToString();
                                    list_price = Convert.ToDecimal(listPrice);
                                    erp_price = Convert.ToDecimal(erpPrice);
                                    list_price = (list_price * currencyRate);
                                    erp_price = (erp_price * currencyRate);
                                }
                                else if (countryId == "36" && currencyType == "AUD") //Australia
                                {
                                    listPrice = row["List Price"].ToString();
                                    erpPrice = row["ERP Price"].ToString();
                                    list_price = Convert.ToDecimal(listPrice);
                                    erp_price = Convert.ToDecimal(erpPrice);
                                }
                                else if (countryId == "35" && currencyType == "INR") //India
                                {
                                    listPrice = row["List Price"].ToString();
                                    erpPrice = row["ERP Price"].ToString();
                                    list_price = Convert.ToDecimal(listPrice);
                                    erp_price = Convert.ToDecimal(erpPrice);
                                }
                                else
                                {
                                    //There must be other fields here which dont match any combination, so we will skip them.
                                }

                                product_name = "Office Licenses"; //Specifically for the RI report
                                product_type = "paas"; //Specifically for the RI report
                                license_name = row["Offer Display Name"].ToString();
                                part_number = row["Offer ID"].ToString();
                                billing_type = row["Purchase Unit"].ToString();
                                base_cost = list_price.ToString();
                                contract_length = row["Purchase Unit"].ToString();
                                locale_currency = countryId;

                                SEMTableFormat.Rows.Add(product_name, product_id, product_type, license_name, license_sku, part_number, retail_pricing, billing_type, license_type, prerequisite_products, licensing_type, add_on_group, qty_rule, qty_min, qty_max, user_qty_count, sku_status, listing_status, renewal_sku, subscription_version, trial, trial_days, trial_upgrade_sku, downloadable_app_file, hardware_platform, license_group, base_cost, use_base_cost, setup_fee, setup_fee_part_no, shipping_fee, shipping_fee_part_no, shipping_description, sku_description, contract_length, locale_currency, prerequisite_license, allow_additional_users, additional_user_cost, additional_user_base, allow_additional_usage, additional_usage_cost, additional_usage_base, additional_use_partnumber, setupfee_base, shipping_base, td_sku, upfront_part_number, arrears_part_number, is_zser, td_vendor_id, purchase_org, purchase_group, seat_change_part_no, recurring_part_no, provisioning_part_number, incompatible_products);
                            }
                        }
                    }
                    //return View();
                    return SEMTableFormat;
                }
            }

            return SEMTableFormat;
        }

        public static class Rfc4180Writer
        {
            public static void WriteDataTable(DataTable sourceTable, TextWriter writer, bool includeHeaders)
            {
                if (includeHeaders)
                {
                    IEnumerable<String> headerValues = sourceTable.Columns
                        .OfType<DataColumn>()
                        .Select(column => QuoteValue(column.ColumnName));

                    writer.WriteLine(String.Join(",", headerValues));
                }

                IEnumerable<String> items = null;

                foreach (DataRow row in sourceTable.Rows)
                {
                    items = row.ItemArray.Select(o => QuoteValue(o?.ToString() ?? String.Empty));
                    writer.WriteLine(String.Join(",", items));
                }

                writer.Flush();
            }

            private static string QuoteValue(string value)
            {
                return String.Concat("\"",
                value.Replace("\"", "\"\""), "\"");
            }
        }

        public static List<SubscriptionUsageList> GetSubscriptionUsage (IPartner partner, string customerId) //Needs additional work, currently doesnot display correct consumption figures
        {
            List<SubscriptionUsageList> subList = new List<SubscriptionUsageList>();
            try
            {
                DataTable ResourceItemList = new DataTable();
                ResourceItemList.Columns.Add("ResourceName", typeof(string));
                ResourceItemList.Columns.Add("SubCatagory", typeof(string));
                ResourceItemList.Columns.Add("ResourceId", typeof(string));
                ResourceItemList.Columns.Add("TotalCost", typeof(decimal));
                //ResourceItemList.Columns.Add("test", typeof(string));
                var subs = partner.Customers.ById(customerId).Subscriptions.Get(); //Get all subscriptions for this customer
                foreach (var sub in subs.Items)
                {
                    string subName = sub.FriendlyName;
                    string subId = sub.Id; 
                    decimal totalCost = 0;
                    var usageRecords = partner.Customers.ById(customerId).Subscriptions.ById(subId).UsageRecords.Resources.Get(); //Get all billable lineitems from subscription for customer
                    foreach (var usageRecord in usageRecords.Items)
                    {
                        ResourceItemList.Rows.Add(usageRecord.ResourceName, usageRecord.Subcategory, usageRecord.ResourceId, usageRecord.TotalCost);
                    }
                    //Perform a LINQ query to get all resource names.
                    var resources = ResourceItemList.AsEnumerable()
                        .Select(row => new
                        {
                            ResourceName = row.Field<string>("ResourceName")
                        });
                    foreach (var distinctResource in resources.Distinct())
                    {
                        totalCost += (from myRow in ResourceItemList.AsEnumerable()
                                     where myRow.Field<string>("ResourceName") == distinctResource.ResourceName.ToString()
                                     select myRow.Field<decimal>("TotalCost")).FirstOrDefault();

                        subList.Add(new SubscriptionUsageList(customerId, subId, distinctResource.ResourceName.ToString(), totalCost.ToString()));
                    }
                }
            }
            catch (Exception ex)
            {
                //The following error will occur if the subscription has no usage; Microsoft.Store.PartnerCenter.Exceptions.PartnerException: 'Customer does not have usage data.'
                subList.Add(new SubscriptionUsageList(customerId, "None", "None", "$0.00"));
            }

            return subList;
        }

        public static string MSCSPCheckDomainAvailability(IAggregatePartner partner, string domain)
        {
            string result = "False";
            try
            {
                result = partner.Domains.ByDomain(domain).Exists().ToString();
            }
            catch (Exception ex)
            {
                // log4net custom variables for log output
                log4net.GlobalContext.Properties["url"] = ex.TargetSite.ToString();

                // Use error or higher to send exception to logger
                log.Error(ex.Message.ToString());

                //Catch errors here
                result = ex.Message.ToString();
            }
            return result;
        }

        public static string MSCSPServiceCostSummary(string selectedCustomerId, IAggregatePartner partner)
        {
            //IAggregatePartner partner = GetPartnerCenterTokenWithUserCredentials();
            var model = new OfferView();
            string value = "";

            try
            {
                var serviceCostsSummary = partner.Customers.ById(selectedCustomerId).ServiceCosts.ByBillingPeriod(ServiceCostsBillingPeriod.MostRecent).Summary.Get();
                value = serviceCostsSummary.PretaxTotal.ToString();
            }
            catch (Exception ex)
            {
                // log4net custom variables for log output
                log4net.GlobalContext.Properties["url"] = ex.TargetSite.ToString();

                // Use error or higher to send exception to logger
                log.Warn(ex.Message.ToString());

                //Set returned value to 0 dollars
                value = "$0.00";
            }
            return value;
        }

        public static IAggregatePartner GetPartnerCenterTokenUsingAppCredentials()
        {
            // Get a user Azure AD Token.
            PartnerService.Instance.ApiRootUrl = ConfigurationManager.AppSettings["PartnerServiceApiEndpoint"];
            var partnerCredentials =
                PartnerCredentials.Instance.GenerateByApplicationCredentials(
                    ConfigurationManager.AppSettings["ApplicationIdApp"],
                    ConfigurationManager.AppSettings["ApplicationSecret"],
                    ConfigurationManager.AppSettings["ApplicationDomain"]);

            // Create operations instance with partnerCredentials.
            return PartnerService.Instance.CreatePartnerOperations(partnerCredentials);
        }

        public static AuthenticationResult UserLogin(string resourceUrl)
        {
            var addAuthority = new UriBuilder(ConfigurationManager.AppSettings["AuthenticationAuthorityEndpoint"])
            {
                Path = ConfigurationManager.AppSettings["CommonDomain"]
            };

            UserCredential userCredentials =
                new UserCredential(ConfigurationManager.AppSettings["UserName"], ConfigurationManager.AppSettings["Password"]);
            AuthenticationContext authContext =
                new AuthenticationContext(addAuthority.Uri.AbsoluteUri);

            //string ResourceUrl = ConfigurationManager.AppSettings["ResourceUrl"];
            string ApplicationIdUser = ConfigurationManager.AppSettings["ApplicationIdUser"];

            return
                //authContext.AcquireToken(ResourceUrl, ApplicationIdUser, userCredentials);
                authContext.AcquireToken(resourceUrl, ApplicationIdUser, userCredentials);
        }

        public static IAggregatePartner GetPartnerCenterTokenWithUserCredentials()
        {
            // Get a user Azure AD Token.
            var aadAuthResult = UserLogin(ConfigurationManager.AppSettings["ResourceUrl"]);
            PartnerService.Instance.ApiRootUrl = ConfigurationManager.AppSettings["PartnerServiceApiEndpoint"];
            var partnerCredentials =
                PartnerCredentials.Instance.GenerateByUserCredentials(ConfigurationManager.AppSettings["ApplicationIdUser"],
                    new AuthenticationToken(aadAuthResult.AccessToken, aadAuthResult.ExpiresOn), async delegate
                    {
                        // Token Refresh callback.
                        aadAuthResult = UserLogin(ConfigurationManager.AppSettings["ResourceUrl"]);
                        return await Task.FromResult<AuthenticationToken>(new AuthenticationToken(aadAuthResult.AccessToken, aadAuthResult.ExpiresOn));
                    });

            // Get operations instance with partnerCredentials.
            return PartnerService.Instance.CreatePartnerOperations(partnerCredentials);
        }

        public ActionResult GAuthMFAClient(string customer)
        {
            var ntpConnection = new NtpConnection("time.google.com");
            DateTime timeNow = ntpConnection.GetUtc();

            GAuthMFAClientView model = new GAuthMFAClientView();
            string storedProc = "usp_GetGAuthIdent";
            DataTable mfaList = new DataTable();
            model.mfaList_View = new DataTable();
            model.mfaList_View.Columns.Add("Tenant Name");
            model.mfaList_View.Columns.Add("MFA Service");
            model.mfaList_View.Columns.Add("Notes");
            model.mfaList_View.Columns.Add("Secret Key");
            model.mfaList_View.Columns.Add("MFA Code");
            try
            {
                using (var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                using (var cmd = new SqlCommand(storedProc, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@companyName", customer);
                    da.Fill(mfaList);
                }

                foreach (DataRow row in mfaList.Rows)
                {
                    string secretKeyDisp = Truncate(row["secretKey"].ToString(), 10) + "...";
                    string companyName = row["companyName"].ToString();
                    string mfaService = row["mfaService"].ToString();
                    string tenancyNote = row["tenancyNote"].ToString();

                    var utcNow = ntpConnection.GetUtc().ToLocalTime();
                    var utcLocal = DateTime.UtcNow.ToLocalTime();
                    var timeDiff = utcNow.Subtract(utcLocal).TotalSeconds;

                    var Secret = Base32.ToBytes(row["secretKey"].ToString());

                    var Timestamp = Convert.ToInt64(GetUnixTimestamp(timeNow) / 30);

                    var data = BitConverter.GetBytes(Timestamp).Reverse().ToArray();
                    var Hmac = new HMACSHA1(Secret).ComputeHash(data);
                    var Offset = Hmac.Last() & 0x0F;
                    var OneTimePassword = (
                        ((Hmac[Offset + 0] & 0x7f) << 24) |
                        ((Hmac[Offset + 1] & 0xff) << 16) |
                        ((Hmac[Offset + 2] & 0xff) << 8) |
                        (Hmac[Offset + 3] & 0xff)
                            ) % 1000000;

                    string totpCode = OneTimePassword.ToString().PadLeft(6, '0');

                    model.mfaList_View.Rows.Add(companyName, mfaService, tenancyNote, secretKeyDisp, totpCode);
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = "An error occurred - " + ex.Message.ToString();
            }

            int remainingTime = (30 - Convert.ToInt32(GetUnixTimestamp(timeNow) % 30));
            model.remainingTime = DateTime.Now.AddSeconds(remainingTime).ToString("dd-MM-yyyy h:mm:ss tt");
            return View(model);
        } //Designed to either retrive all values, or return specific value base on customerName

        public static string Truncate(string str, int length)
        {
            if (length < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(length), "Length must be >= 0");
            }

            if (str == null)
            {
                return null;
            }

            int maxLength = Math.Min(str.Length, length);
            return str.Substring(0, maxLength);
        }

        private static Int64 GetUnixTimestamp(DateTime dt)
        {
            return Convert.ToInt64(Math.Round((dt - new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds));
        }

        public static class Base32
        {
            public static byte[] ToBytes(string input)
            {
                if (string.IsNullOrEmpty(input))
                {
                    throw new ArgumentNullException("input");
                }

                input = input.TrimEnd('='); //remove padding characters
                int byteCount = input.Length * 5 / 8; //this must be TRUNCATED
                byte[] returnArray = new byte[byteCount];

                byte curByte = 0, bitsRemaining = 8;
                int mask = 0, arrayIndex = 0;

                foreach (char c in input)
                {
                    int cValue = CharToValue(c);

                    if (bitsRemaining > 5)
                    {
                        mask = cValue << (bitsRemaining - 5);
                        curByte = (byte)(curByte | mask);
                        bitsRemaining -= 5;
                    }
                    else
                    {
                        mask = cValue >> (5 - bitsRemaining);
                        curByte = (byte)(curByte | mask);
                        returnArray[arrayIndex++] = curByte;
                        curByte = (byte)(cValue << (3 + bitsRemaining));
                        bitsRemaining += 3;
                    }
                }

                //if we didn't end with a full byte
                if (arrayIndex != byteCount)
                {
                    returnArray[arrayIndex] = curByte;
                }

                return returnArray;
            }

            public static string ToString(byte[] input)
            {
                if (input == null || input.Length == 0)
                {
                    throw new ArgumentNullException("input");
                }

                int charCount = (int)Math.Ceiling(input.Length / 5d) * 8;
                char[] returnArray = new char[charCount];

                byte nextChar = 0, bitsRemaining = 5;
                int arrayIndex = 0;

                foreach (byte b in input)
                {
                    nextChar = (byte)(nextChar | (b >> (8 - bitsRemaining)));
                    returnArray[arrayIndex++] = ValueToChar(nextChar);

                    if (bitsRemaining < 4)
                    {
                        nextChar = (byte)((b >> (3 - bitsRemaining)) & 31);
                        returnArray[arrayIndex++] = ValueToChar(nextChar);
                        bitsRemaining += 5;
                    }

                    bitsRemaining -= 3;
                    nextChar = (byte)((b << bitsRemaining) & 31);
                }

                //if we didn't end with a full char
                if (arrayIndex != charCount)
                {
                    returnArray[arrayIndex++] = ValueToChar(nextChar);
                    while (arrayIndex != charCount) returnArray[arrayIndex++] = '='; //padding
                }

                return new string(returnArray);
            }

            private static int CharToValue(char c)
            {
                var value = (int)c;

                //65-90 == uppercase letters
                if (value < 91 && value > 64)
                {
                    return value - 65;
                }
                //50-55 == numbers 2-7
                if (value < 56 && value > 49)
                {
                    return value - 24;
                }
                //97-122 == lowercase letters
                if (value < 123 && value > 96)
                {
                    return value - 97;
                }

                throw new ArgumentException("Character is not a Base32 character.", "c");
            }

            private static char ValueToChar(byte b)
            {
                if (b < 26)
                {
                    return (char)(b + 65);
                }

                if (b < 32)
                {
                    return (char)(b + 24);
                }

                throw new ArgumentException("Byte is not a value Base32 value.", "b");
            }
        }
    }
}