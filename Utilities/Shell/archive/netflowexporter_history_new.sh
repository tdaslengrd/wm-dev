#!/bin/bash

### NetflowBulkExporter Shell Script v1.1 ###

echo "NetflowBulkExporter Shell Script v1.1"

####### NOTES #######
#This script will run once per day, for the previous day results.
#i.e. Set a cron job to run each day at 11pm, this will run the import job for the previous day, between 00:00:00 to 23:59:59.
#####################

#Script variables, dont change unless you know what your doing
LOCALENVPATH=/usr/local/bin
ENVPATH=/usr/bin
SYSENVPATH=/bin
#DAY=`date -d '-1 day' '+%d'` #This gets yesterdays day
#MONTH=`date +"%m"`
#YEAR=`date +"%Y"`
STARTDAY=10
MONTH=09
YEAR=2017
DAYSINMONTH=`cal $(date +${MONTH}) | awk 'NF {DAYS = $NF}; END {print DAYS}'`
CURRENTDATETIME=`date +"%Y-%m-%d %H:%M:%S"`
#YESTERDAYDATETIME=`date -d '-1 day' +"%Y-%m-%d %H:%M:%S"`
#YESTERDAYDATETIME=${YEAR}-${MONTH}-${DAY}
RECORDCOUNT="40000"
SERVERNAME="1000Flanders01"

FTP_HOST="10.100.0.249"
FTP_USER="netflowftpuser"
FTP_PASSWD="Passw0rd"

#Loop through each day in the given month
for (( DAY=$STARTDAY; DAY<=$DAYSINMONTH; DAY++ ))
do
	#This will format the day correctly if the looped variable is 1 character long
	size=${#DAY}
	if [ $size -eq 1 ];
	then
		DAY="0${DAY}"
	fi

	YESTERDAYDATETIME=${YEAR}-${MONTH}-${DAY}

	echo "Starting job ${CURRENTDATETIME}"

	TRAFFICDIRECTION=all

${ENVPATH}/ftp -n $FTP_HOST << END_SCRIPT
quote USER $FTP_USER
quote PASS $FTP_PASSWD
mkdir $YESTERDAYDATETIME
quit
END_SCRIPT

	echo "Processing data for ${YEAR}/${MONTH}/${DAY} on ${SERVERNAME}"

#Start and end hour to form STARTDATE and ENDDATE variable for rwfilter
        START=00
        END=23

        #Main loop to get data from Netflow to MySQL
        for (( c=$START; c<=$END; c++ ))
        do
		#Start and end dates for rwfilter
		#STARTDATE="${YEAR}/${MONTH}/${DAY}"
	        #ENDDATE=$STARTDATE
		STARTDATE="${YEAR}/${MONTH}/${DAY}:${c}"
                ENDDATE="${YEAR}/${MONTH}/${DAY}:${c}"

		echo "Working on hour ${c}"

		TEMPFILE="netflow_${TRAFFICDIRECTION}_${DAY}_${MONTH}_${YEAR}.csv"
		REPORTPATH="/data/tmp/$TEMPFILE"
		touch $REPORTPATH

		#Get last hours transit information and save to a temp file
		#${LOCALENVPATH}/rwfilter --type=all --proto=0- --pass=stdout --start-date=$STARTDATE --end-date=$ENDDATE | ${LOCALENVPATH}/rwstats --count=$RECORDCOUNT --fields=sIP,dIP,sPort,dPort,sTime,eTime,bytes --temp-directory=/data/tmp --no-titles --no-columns --no-final-delimiter --column-separator=',' --output-path=$REPORTPATH
		#${LOCALENVPATH}/rwfilter --type=all --proto=0- --pass=stdout --start-date=$STARTDATE --end-date=$ENDDATE | ${LOCALENVPATH}/rwstats --threshold=10 --fields=sIP,dIP,sPort,dPort,sTime,eTime,bytes --temp-directory=/data/tmp --no-titles --no-columns --no-final-delimiter --column-separator=',' --output-path=$REPORTPATH
		${LOCALENVPATH}/rwfilter --type=all --proto=0- --pass=stdout --start-date=$STARTDATE --end-date=$ENDDATE | rwstats --threshold=1 --fields=sIP,dIP,sPort,dPort,sTime,eTime --values=bytes --temp-directory=/data/tmp --no-titles --no-columns --no-percents --no-final-delimiter --column-separator=',' --bin-time=3600  --output-path=$REPORTPATH
		#${LOCALENVPATH}/rwfilter --type=all --proto=0- --pass=stdout --start-date=$STARTDATE | rwstats --count=$RECORDCOUNT --fields=sIP,dIP,sPort,dPort,sTime,eTime --values=bytes --temp-directory=/data/tmp --no-titles --no-columns --no-percents --no-final-delimiter --column-separator=',' --output-path=$REPORTPATH

		#FTP_TEMPFILE="netflow_${TRAFFICDIRECTION}_${DAY}_${MONTH}_${YEAR}_${c}_ftp.csv"
		#FTP_TEMPPATH="/data/tmp/$FTP_TEMPFILE"				
		#touch $FTP_TEMPPATH

		#DATETIME=`date -d $YESTERDAYDATETIME +"%Y-%m-%d %H:%M:%S"`

		#${SYSENVPATH}/sed "s/$/,${DATETIME}/" $REPORTPATH > $FTP_TEMPPATH

		FTP_TEMPFILE2="netflow_${TRAFFICDIRECTION}_${DAY}_${MONTH}_${YEAR}_${c}_ftp2_${SERVERNAME}.csv"
		FTP_TEMPPATH2="/data/tmp/$FTP_TEMPFILE2"
		touch $FTP_TEMPPATH2

		#string="sIP,dIP,sPort,dPort,sTime,eTime,bytes,Records,PercentRecords,CumulativePercent"; ${SYSENVPATH}/awk -v n=1 -v s="$string" 'NR == n {print s} {print}' $FTP_TEMPPATH > $FTP_TEMPPATH2;
		#string="sIP,dIP,sPort,dPort,sTime,eTime,bytes,Records,PercentRecords,CumulativePercent"; ${SYSENVPATH}/awk -v n=1 -v s="$string" 'NR == n {print s} {print}' $REPORTPATH > $FTP_TEMPPATH2;
		string="sIP,dIP,sPort,dPort,sTime,eTime,bytes"; ${SYSENVPATH}/awk -v n=1 -v s="$string" 'NR == n {print s} {print}' $REPORTPATH > $FTP_TEMPPATH2;

		#Export to FTP, requires remote processing.
		FILE=$FTP_TEMPPATH2

${ENVPATH}/ftp -n $FTP_HOST << END_SCRIPT
quote USER $FTP_USER
quote PASS $FTP_PASSWD
cd $YESTERDAYDATETIME
binary
put $FILE $FTP_TEMPFILE2
quit
END_SCRIPT

		#Remove any temp files generated above
		${SYSENVPATH}/rm -f /data/tmp/netflow_*
	done
done
FINISHTIME=`date +"%Y-%m-%d %H:%M:%S"`
echo "Finished job ${FINISHTIME}"
