USE [VeeamUsageData]
GO

/****** Object:  StoredProcedure [dbo].[usp_GetVeeamOtherJobCount]    Script Date: 12/12/2017 8:23:44 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GetVeeamOtherJobCount] @tableName nvarchar(30)
AS
DECLARE @cmd_genReport nvarchar(max)
SET @cmd_genReport = '
SELECT COUNT(DISTINCT jobName) AS ''Veeam Other Job Count'' FROM VeeamUsageData.dbo.' + @tableName + ' WHERE jobType NOT LIKE ''Backup'' AND jobType NOT LIKE ''Replica'';'
EXEC sp_executesql @cmd_genReport
GO

