﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TDServicesApp.Models.AssetRegObjects
{
    public class InterfaceList
    {
        public string InterfaceName { get; set; }
        public int InterfaceID { get; set; }
        public List<IPAddressList> IPList { get; set; }
    }
}