USE [NetflowResultsData]
GO

/****** Object:  StoredProcedure [dbo].[usp_GetCustomers]    Script Date: 20/10/2017 10:38:14 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GetCustomers] @Month nvarchar(30)
AS
DECLARE @cmd_genReport nvarchar(max)
SET @cmd_genReport = '
SELECT DISTINCT D.[Customer Name]
FROM assetregisterSQL.dbo.IPAddressesNew A
inner join assetregisterSQL.dbo.Interfaces B on A.InterfaceID = B.InterfaceID
inner join assetregisterSQL.dbo.Machines C on C.MachineID = B.MachineID
inner join assetregisterSQL.dbo.Customers D on D.CustomerNumber = C.CustomerNumber
inner join NetflowResultsData.dbo.' + @Month + ' E on A.IPAddress = E.ipaddr
ORDER BY D.[Customer Name] ASC'
EXEC sp_executesql @cmd_genReport
GO

