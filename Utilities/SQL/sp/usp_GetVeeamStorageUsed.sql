USE [VeeamUsageData]
GO

/****** Object:  StoredProcedure [dbo].[usp_GetVeeamStorageUsed]    Script Date: 12/12/2017 8:24:24 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GetVeeamStorageUsed] @tableName nvarchar(30)
AS
DECLARE @cmd_genReport nvarchar(max)
SET @cmd_genReport = '
SELECT SUM(totalbackupsize)/1024 AS ''Veeam Total Storage Used (TB)'' FROM VeeamUsageData.dbo.' + @tableName + ';'
EXEC sp_executesql @cmd_genReport
GO

