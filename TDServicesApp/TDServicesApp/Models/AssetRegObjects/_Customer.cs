﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TDServicesApp.Models.AssetRegObjects
{
    public class _Customer
    {
        public string CustomerName { get; set; }
        public string CustomerNumber { get; set; }
        public List<_Machine> MachineNames { get; set; }
        public List<_User> Users { get; set; }
    }
}