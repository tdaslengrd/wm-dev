﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using TDServicesApp.Models;
using TDServicesApp.Models.AssetRegObjects;

namespace TDServicesApp.Controllers
{
    [Authorize(Roles = "Admin")]
    [LayoutInjecter("_LayoutAssetReg")] //Loads the layour file within the Shared view directory which contains customised JS modules
    public class AssetRegController : GlobalController
    {
        // GET: AssetReg
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SearchCustomer()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SearchCustomer(SearchCustomerModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var db = new assetregisterSQLEntities())
                    {
                        var records = (from customer in db.Customers
                                       where customer.Customer_Name.ToLower().Contains(model.SearchPhrase)
                                       select customer).ToList();

                        if (records != null && records.Count() != 0)
                        {
                            List<_Customer> CustomerList = new List<_Customer>();

                            foreach (var item in records)
                            {
                                CustomerList.Add(new _Customer()
                                {
                                    CustomerName = item.Customer_Name,
                                    CustomerNumber = item.CustomerNumber
                                });
                            }
                            return View(new SearchCustomerModel()
                            {
                                Customers = CustomerList,
                                NoCustomers = false
                            });

                        }
                        else
                        {
                            return View(new SearchCustomerModel()
                            {
                                IsFirstRun = false,
                                NoCustomers = true
                            });
                        }
                    }
                }
                catch (Exception ex)
                {
                    // log4net custom variables for log output
                    log4net.GlobalContext.Properties["url"] = ex.TargetSite.ToString();

                    // Use error or higher to send exception to logger
                    log.Error(ex.Message.ToString());

                    return View("Error");
                }
            }
            else
            {
                return View();
            }
        }

        public ActionResult AddEditCustomer(string customerNumber = null)
        {
            try
            {
                if (!String.IsNullOrEmpty(customerNumber))
                {
                    using (var db = new assetregisterSQLEntities())
                    {
                        var cus = (from customer in db.Customers
                                   where customer.CustomerNumber == customerNumber
                                   select customer).FirstOrDefault();

                        var records = (from user in db.Users
                                       where user.CustomerNumber == customerNumber
                                       select user).ToList();

                        if (records != null)
                        {
                            List<_User> userList = new List<_User>();

                            foreach (var item in records)
                            {
                                userList.Add(new _User()
                                {
                                    UserName = item.UserName,
                                    Password = item.Password,
                                    UserNumber = item.UserNumber
                                });
                            }

                            return View(new CustomersModel()
                            {
                                IsNew = false,
                                CustomerName = cus.Customer_Name,
                                CustomerNumber = cus.CustomerNumber,
                                Users = userList
                            });

                        }
                        else //New
                        {
                            return View(new CustomersModel());
                        }
                    }
                }
                else
                {
                    return View(new CustomersModel());
                }
            }

            catch (Exception ex)
            {
                // log4net custom variables for log output
                log4net.GlobalContext.Properties["url"] = ex.TargetSite.ToString();

                // Use error or higher to send exception to logger
                log.Error(ex.Message.ToString());

                return View("Error");
            }

        }

        [HttpPost]
        public ActionResult AddEditCustomer(CustomersModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var db = new assetregisterSQLEntities())
                    {

                        var record = (from cus in db.Customers
                                      where cus.CustomerNumber == model.CustomerNumber
                                      select cus).FirstOrDefault();

                        if (record != null)
                        {
                            record.Customer_Name = model.CustomerName;
                        }
                        db.SaveChanges();
                        return RedirectToAction("SearchCustomer", "AssetReg");

                    }
                }
                catch (Exception ex)
                {
                    // log4net custom variables for log output
                    log4net.GlobalContext.Properties["url"] = ex.TargetSite.ToString();

                    // Use error or higher to send exception to logger
                    log.Error(ex.Message.ToString());

                    return View("Error");
                }
            }
            else
            {
                return View();
            }
        }

        public ActionResult EditUser(int userNum)
        {
            try
            {
                using (var db = new assetregisterSQLEntities())
                {
                    var record = (from user in db.Users
                                  where user.UserNumber == userNum
                                  select user).FirstOrDefault();

                    if (record != null)
                    {
                        return View(new _User()
                        {
                            UserName = record.UserName,
                            Password = record.Password,
                            UserNumber = record.UserNumber
                        });
                    }
                    else
                    {
                        return View(new _User());
                    }

                }
            }
            catch (Exception ex)
            {
                // log4net custom variables for log output
                log4net.GlobalContext.Properties["url"] = ex.TargetSite.ToString();

                // Use error or higher to send exception to logger
                log.Error(ex.Message.ToString());

                return View("Error");
            }
        }

        [HttpPost]
        public ActionResult EditUser(_User model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var db = new assetregisterSQLEntities())
                    {
                        var record = (from user in db.Users
                                      where user.UserNumber == model.UserNumber
                                      select user).FirstOrDefault();

                        if (record != null)
                        {
                            record.UserName = model.UserName;
                            record.Password = model.Password;
                        }
                        db.SaveChanges();
                        return RedirectToAction("AddEditCustomer", "AssetReg", new { customerNumber = record.CustomerNumber });
                    }
                }

                catch (Exception ex)
                {
                    // log4net custom variables for log output
                    log4net.GlobalContext.Properties["url"] = ex.TargetSite.ToString();

                    // Use error or higher to send exception to logger
                    log.Error(ex.Message.ToString());

                    return View("Error");
                }
            }
            else
                return View(model);
        }

        public ActionResult DeleteUser(int userNum)
        {
            try
            {
                using (var db = new assetregisterSQLEntities())
                {
                    var record = (from user in db.Users
                                  where user.UserNumber == userNum
                                  select user).FirstOrDefault();

                    if (record != null)
                    {
                        return View(new _User()
                        {
                            UserName = record.UserName,
                            Password = record.Password,
                            UserNumber = record.UserNumber
                        });
                    }
                    else
                    {
                        return View(new _User());
                    }

                }
            }
            catch (Exception ex)
            {
                // log4net custom variables for log output
                log4net.GlobalContext.Properties["url"] = ex.TargetSite.ToString();

                // Use error or higher to send exception to logger
                log.Error(ex.Message.ToString());

                return View("Error");
            }
        }

        [HttpPost]
        public ActionResult DeleteUser(_User model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var db = new assetregisterSQLEntities())
                    {
                        var record = (from user in db.Users
                                      where user.UserNumber == model.UserNumber
                                      select user).FirstOrDefault();

                        Session["Customer"] = record.CustomerNumber;

                        if (record != null)
                        {
                            db.Users.Remove(record);
                            db.SaveChanges();
                        }
                        return RedirectToAction("AddEditCustomer", "AssetReg", new { customerNumber = Session["Customer"] });
                    }

                }
                catch (Exception ex)
                {
                    // log4net custom variables for log output
                    log4net.GlobalContext.Properties["url"] = ex.TargetSite.ToString();

                    // Use error or higher to send exception to logger
                    log.Error(ex.Message.ToString());

                    return View("Error");
                }
            }
            else
                return View(model);
        }

        public ActionResult AddNewUser(string cusNumber)
        {
            try
            {
                if (!string.IsNullOrEmpty(cusNumber))
                {
                    return View(new AddUserViewModel()
                    {
                        CustomerNumber = cusNumber
                    });
                }
                else
                    return View();
            }
            catch (Exception ex)
            {
                // log4net custom variables for log output
                log4net.GlobalContext.Properties["url"] = ex.TargetSite.ToString();

                // Use error or higher to send exception to logger
                log.Error(ex.Message.ToString());

                return View("Error");
            }

        }

        [HttpPost]
        public ActionResult AddNewUser(AddUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var db = new assetregisterSQLEntities())
                    {
                        db.Users.Add(new User()
                        {
                            UserName = model.Username,
                            Password = model.Password,
                            CustomerNumber = model.CustomerNumber
                        });
                        db.SaveChanges();
                        return RedirectToAction("AddEditCustomer", "AssetReg", new { customerNumber = model.CustomerNumber });
                    }
                }
                catch (Exception ex)
                {
                    // log4net custom variables for log output
                    log4net.GlobalContext.Properties["url"] = ex.TargetSite.ToString();

                    // Use error or higher to send exception to logger
                    log.Error(ex.Message.ToString());

                    return View("Error");
                }
            }
            else
                return View(model);
        }

        public ActionResult AddNewCustomer()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddNewCustomer(AddNewCustomerModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var db = new assetregisterSQLEntities())
                    {
                        db.Customers.Add(new Customer()
                        {
                            CustomerNumber = model.CustomerNumber,
                            Customer_Name = model.CustomerName
                        });

                        db.Users.Add(new User()
                        {
                            UserName = model.UserName,
                            Password = model.Password,
                            CustomerNumber = model.CustomerNumber
                        });
                        db.SaveChanges();
                        return RedirectToAction("SearchCustomer", "AssetReg");
                    }
                }
                catch (Exception ex)
                {
                    // log4net custom variables for log output
                    log4net.GlobalContext.Properties["url"] = ex.TargetSite.ToString();

                    // Use error or higher to send exception to logger
                    log.Error(ex.Message.ToString());

                    return View("Error");
                }

            }
            return View(model);
        }

        public ActionResult SearchMachine()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SearchMachine(SearchMachineModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var db = new assetregisterSQLEntities())
                    {
                        var records = (from machine in db.Machines
                                       where machine.Machine_Name.ToLower().Contains(model.SearchPhrase.ToLower())
                                       select machine).ToList();

                        if (records != null)
                        {
                            List<MachinList> MachinesTest = new List<MachinList>();

                            foreach (var item in records)
                            {
                                MachinesTest.Add(new MachinList()
                                {
                                    Machine_Name = item.Machine_Name
                                });
                            }
                            return View(new SearchMachineModel()
                            {
                                Machines = MachinesTest
                            });

                        }
                        else
                            return View(new SearchMachineModel()
                            {
                                IsFirstRun = false
                            });
                    }
                }
                catch (Exception ex)
                {
                    // log4net custom variables for log output
                    log4net.GlobalContext.Properties["url"] = ex.TargetSite.ToString();

                    // Use error or higher to send exception to logger
                    log.Error(ex.Message.ToString());

                    return View("Error");
                }
            }

            return View();
        }

        public ActionResult AddEditMachine(string machineName = null)
        {
            try
            {
                if (!string.IsNullOrEmpty(machineName))
                {
                    using (var db = new assetregisterSQLEntities())
                    {
                        var record = (from machine in db.Machines
                                      where machine.Machine_Name == machineName
                                      select machine).FirstOrDefault();
                        if (record != null)
                        {  //Edit

                            var Datacenter = (from dc in db.Datacenters
                                              where dc.DatacenterID == record.DataCenterID
                                              select dc.DataCenter1).FirstOrDefault();

                            List<DataCenterList> DcNames = new List<DataCenterList>();
                            DcNames = (from datac in db.Datacenters
                                       select new DataCenterList
                                       {
                                           DataCenterName = datac.DataCenter1
                                       }).ToList();


                            List<InterfaceList> InterNames = new List<InterfaceList>();
                            InterNames = (from inter in db.Interfaces
                                          where inter.MachineID == record.MachineID
                                          select new InterfaceList
                                          {
                                              InterfaceName = inter.InterfaceName,
                                              InterfaceID = inter.InterfaceID,
                                              IPList = (from ip in db.IPAddressesNews
                                                        where ip.InterfaceID == inter.InterfaceID
                                                        select new IPAddressList
                                                        {
                                                            IPAddress = ip.IPAddress
                                                        }).ToList<IPAddressList>()
                                          }).ToList<InterfaceList>();


                            List<HostList> Hosts = new List<HostList>();

                            Hosts = (from ma in db.Machines
                                     select new HostList
                                     {
                                         HostMachine = ma.HostMachine
                                     }).Distinct().ToList();



                            List<PowerRailList> Powers = new List<PowerRailList>();
                            Powers = (from m in db.Machines
                                      select new PowerRailList
                                      {
                                          PowerRail = m.PowerRail
                                      }).Distinct().ToList();

                            List<RackList> racks = new List<RackList>();
                            racks = (from mac in db.Machines
                                     select new RackList
                                     {
                                         Rack = mac.Rack
                                     }).Distinct().ToList();

                            return View(new AddEditMachineModel()
                            {
                                IsNew = false,
                                IsVirtualMachine = record.IsVirtualMachine,
                                IsVirtualServer = record.IsVirtualServer,
                                MachineID = record.MachineID,
                                Machine_Name = record.Machine_Name,
                                Comments = record.Comments,
                                HostName = record.HostMachine,
                                HostMachines = Hosts,
                                PowRail = record.PowerRail,
                                Rak = record.Rack,
                                PowerRails = Powers,
                                DataCenterName = Datacenter,
                                DataCenters = DcNames,
                                Racks = racks,
                                InterfaceNames = InterNames,
                                //VlanID = record.VlanId
                            });
                        }
                        else
                        { //Add
                            return View(new AddEditMachineModel());

                        }
                    }
                }
                else  //Add
                {
                    return View(new AddEditMachineModel());
                }
            }
            catch (Exception ex)
            {
                // log4net custom variables for log output
                log4net.GlobalContext.Properties["url"] = ex.TargetSite.ToString();

                // Use error or higher to send exception to logger
                log.Error(ex.Message.ToString());

                return View("Error");
            }

        }

        [HttpPost]
        public ActionResult AddEditMachine(AddEditMachineModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var db = new assetregisterSQLEntities())
                    {
                        if (model.IsNew) //Add
                        {
                            db.Machines.Add(new Machine()
                            {

                                Machine_Name = model.Machine_Name,
                                Comments = model.Comments,
                                //VlanId = model.VlanID
                            });
                        }
                        else  //Edit
                        {
                            var record = (from machine in db.Machines
                                          where machine.MachineID == model.MachineID
                                          select machine).FirstOrDefault();
                            if (record != null)
                            {
                                record.Machine_Name = model.Machine_Name;
                                record.Comments = model.Comments;
                                record.HostMachine = model.HostName;
                                record.PowerRail = model.PowRail;
                                record.Rack = model.Rak;
                                record.dateModified = DateTime.Now;
                                //record.VlanId = model.VlanID;
                                record.DataCenterID = (from dc in db.Datacenters
                                                       where dc.DataCenter1 == model.DataCenterName
                                                       select dc.DatacenterID).FirstOrDefault();
                                //var res = (from vl in db.VLANs
                                //           where vl.VLANID == model.VlanID
                                //           select vl).FirstOrDefault();
                                //var res = (from vl in db.VirtualNetworks
                                //           where vl.NetworkID == model.VlanID
                                //           select vl).FirstOrDefault();
                                //if (res != null)
                                //{
                                //    res.CustomerNumber = record.CustomerNumber;
                                //}
                                db.SaveChanges();
                            }
                        }
                        db.SaveChanges();
                        return RedirectToAction("SearchMachine", "AssetReg");
                    }
                }

                catch (Exception ex)
                {
                    // log4net custom variables for log output
                    log4net.GlobalContext.Properties["url"] = ex.TargetSite.ToString();

                    // Use error or higher to send exception to logger
                    log.Error(ex.Message.ToString());

                    return View("Error");
                }

            }
            else
                return View(model);
        }

        public ActionResult AddVirtual()
        {
            try
            {
                using (var db = new assetregisterSQLEntities())
                {
                    List<HostList> Hosts = new List<HostList>();

                    Hosts = (from ma in db.Machines
                             select new HostList
                             {
                                 HostMachine = ma.HostMachine
                             }).Distinct().ToList();

                    return View(new AddVirtualViewModel()
                    {
                        HostMachines = Hosts,
                        IsVirtualMachine = true
                    });
                }
            }
            catch (Exception ex)
            {
                // log4net custom variables for log output
                log4net.GlobalContext.Properties["url"] = ex.TargetSite.ToString();

                // Use error or higher to send exception to logger
                log.Error(ex.Message.ToString());

                return View("Error");
            }
        }

        [HttpPost]
        public ActionResult AddVirtual(AddVirtualViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var db = new assetregisterSQLEntities())
                    {
                        db.Machines.Add(new Machine()
                        {
                            Machine_Name = model.MachineName,
                            CustomerNumber = model.CustomerNumber,
                            Comments = model.Comments,
                            IsVirtualMachine = model.IsVirtualMachine,
                            HostMachine = model.HostName,
                            loginuser = model.LoginUser,
                            logindomain = model.LoginDomain,
                            dateCreated = DateTime.Now,
                            dateModified = DateTime.Now,
                            //VlanId = model.VlanID,
                            //Serial_Number = model.SerialNumber
                        });
                        db.SaveChanges();

                        //var res = (from vl in db.VLANs
                        //           where vl.VLANID == model.VlanID
                        //           select vl).FirstOrDefault();

                        //var res = (from vl in db.VirtualNetworks
                        //           where vl.NetworkID == model.VlanID
                        //           select vl).FirstOrDefault();

                        //if (res != null)
                        //{
                        //    res.CustomerNumber = model.CustomerNumber;
                        //}
                        //db.SaveChanges();

                        return RedirectToAction("SearchMachine", "AssetReg");
                    }
                }
                catch (Exception ex)
                {
                    // log4net custom variables for log output
                    log4net.GlobalContext.Properties["url"] = ex.TargetSite.ToString();

                    // Use error or higher to send exception to logger
                    log.Error(ex.Message.ToString());

                    return View("Error");
                }
            }
            else
                return View(new AddVirtualViewModel());
        }

        public ActionResult AddServer()
        {
            try
            {
                using (var db = new assetregisterSQLEntities())
                {
                    List<DataCenterList> DcNames = new List<DataCenterList>();
                    DcNames = (from datac in db.Datacenters
                               select new DataCenterList
                               {
                                   DataCenterName = datac.DataCenter1
                               }).ToList();

                    List<PowerRailList> Powers = new List<PowerRailList>();
                    Powers = (from m in db.Machines
                              where m.PowerRail != null
                              select new PowerRailList
                              {
                                  PowerRail = m.PowerRail
                              }).Distinct().ToList();

                    List<RackList> racks = new List<RackList>();
                    racks = (from mac in db.Machines
                             where mac.Rack != null
                             select new RackList
                             {
                                 Rack = mac.Rack
                             }).Distinct().ToList();

                    return View(new AddServerViewModel()
                    {
                        IsVirtualMachine = false,
                        DataCenters = DcNames,
                        Racks = racks,
                        PowerRails = Powers
                    });
                }
            }
            catch (Exception ex)
            {
                // log4net custom variables for log output
                log4net.GlobalContext.Properties["url"] = ex.TargetSite.ToString();

                // Use error or higher to send exception to logger
                log.Error(ex.Message.ToString());

                return View("Error");
            }
        }

        [HttpPost]
        public ActionResult AddServer(AddServerViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var db = new assetregisterSQLEntities())
                    {
                        var record = (from dc in db.Datacenters
                                      where dc.DataCenter1 == model.DataCenterName
                                      select dc).FirstOrDefault();

                        db.Machines.Add(new Machine()
                        {
                            Machine_Name = model.MachineName,
                            CustomerNumber = model.CustomerNumber,
                            Comments = model.Comments,
                            IsVirtualMachine = model.IsVirtualMachine,
                            PowerRail = model.PowRail,
                            Rack = model.Rak,
                            DataCenterID = record.DatacenterID,
                            Serial_Number = model.SerialNumber,
                            dateCreated = DateTime.Now,
                            dateModified = DateTime.Now,
                            loginuser = model.LoginUser,
                            logindomain = model.LoginDomain
                        });
                        db.SaveChanges();
                        return RedirectToAction("SearchMachine", "AssetReg");
                    }
                }
                catch (Exception ex)
                {
                    // log4net custom variables for log output
                    log4net.GlobalContext.Properties["url"] = ex.TargetSite.ToString();

                    // Use error or higher to send exception to logger
                    log.Error(ex.Message.ToString());

                    return View("Error");
                }
            }
            else
                return View(new AddServerViewModel());
        }

        public ActionResult AddInterface(int machineId)
        {
            try
            {
                using (var db = new assetregisterSQLEntities())
                {
                    List<SubnetTypeList> Subnets = new List<SubnetTypeList>();
                    Subnets = (from sub in db.IPSubnetTypes
                               select new SubnetTypeList
                               {
                                   IPSubType = sub.IPSubnetType1
                               }).Distinct().ToList();

                    List<IPSubnetList> IPs = new List<IPSubnetList>();
                    List<IPAddressList> IpAddresses = new List<IPAddressList>();



                    return View(new AddInterfaceViewModel()
                    {
                        SubnetTypes = Subnets
                    });
                }

            }
            catch (Exception ex)
            {
                // log4net custom variables for log output
                log4net.GlobalContext.Properties["url"] = ex.TargetSite.ToString();

                // Use error or higher to send exception to logger
                log.Error(ex.Message.ToString());

                return View("Error");
            }
        }

        [HttpPost]
        public ActionResult AddInterface(AddInterfaceViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var db = new assetregisterSQLEntities())
                    {
                        var record = (from mac in db.Machines
                                      where mac.MachineID == model.MachineID
                                      select mac).FirstOrDefault();

                        if (record != null)
                        {
                            db.Interfaces.Add(new Interface()
                            {
                                MachineID = record.MachineID,
                                InterfaceName = model.InterfaceName
                            });
                        }
                        db.SaveChanges();

                        var res = (from inte in db.Interfaces
                                   where inte.MachineID == record.MachineID && inte.InterfaceName == model.InterfaceName
                                   select inte).FirstOrDefault();

                        Session["InterfaceId"] = res.InterfaceID;

                        var rec = (from ip in db.IPAddressesNews
                                   where ip.IPAddress == model.UnMappedIPs
                                   select ip).FirstOrDefault();


                        if (rec != null && rec.InterfaceID == null)
                        {
                            rec.InterfaceID = (int)Session["InterfaceId"];
                        }
                        db.SaveChanges();

                        var mapIp = (from ipAdd in db.IPAddressesNews
                                     where ipAdd.InterfaceID == rec.InterfaceID
                                     select ipAdd).FirstOrDefault();

                        if (mapIp != null)
                        {
                            mapIp.MappedIP = model.MappedIP;
                        }
                        db.SaveChanges();

                        return RedirectToAction("AddEditMachine", new { machineName = record.Machine_Name });
                    }
                }
                catch (Exception ex)
                {
                    // log4net custom variables for log output
                    log4net.GlobalContext.Properties["url"] = ex.TargetSite.ToString();

                    // Use error or higher to send exception to logger
                    log.Error(ex.Message.ToString());

                    return View("Error");
                }
            }
            return View(model);
        }

        public ActionResult DeleteInterface(int interId)
        {
            try
            {
                using (var db = new assetregisterSQLEntities())
                {
                    var record = (from inter in db.Interfaces
                                  where inter.InterfaceID == interId
                                  select inter).FirstOrDefault();

                    List<IPAddressList> IPs = new List<IPAddressList>();
                    IPs = (from ip in db.IPAddressesNews
                           where ip.InterfaceID == interId
                           select new IPAddressList
                           {
                               IPAddress = ip.IPAddress
                           }).ToList();


                    if (record != null)
                    {
                        return View(new DeleteInterfaceViewModel()
                        {
                            InterfaceName = record.InterfaceName,
                            InterfaceID = record.InterfaceID,
                            MachineID = record.MachineID,
                            IpAddresses = IPs
                        });
                    }
                    else
                    {
                        return View(new DeleteInterfaceViewModel());
                    }
                }
            }
            catch (Exception ex)
            {
                // log4net custom variables for log output
                log4net.GlobalContext.Properties["url"] = ex.TargetSite.ToString();

                // Use error or higher to send exception to logger
                log.Error(ex.Message.ToString());

                return View("Error");
            }

        }

        [HttpPost]
        public ActionResult DeleteInterface(DeleteInterfaceViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var db = new assetregisterSQLEntities())
                    {
                        var machine = (from mac in db.Machines
                                       where mac.MachineID == model.MachineID
                                       select mac).FirstOrDefault();

                        var res = (from ip in db.IPAddressesNews
                                   where ip.InterfaceID == model.InterfaceID
                                   select ip).ToList();
                        if (res != null)
                        {
                            foreach (var item in res)
                            {
                                item.InterfaceID = null;
                            }
                            db.SaveChanges();
                        }

                        var record = (from inter in db.Interfaces
                                      where inter.InterfaceID == model.InterfaceID
                                      select inter).FirstOrDefault();

                        if (record != null)
                        {
                            db.Interfaces.Remove(record);
                            db.SaveChanges();
                        }



                        return RedirectToAction("AddEditMachine", "AssetReg", new { machineName = machine.Machine_Name });
                    }
                }
                catch (Exception ex)
                {
                    // log4net custom variables for log output
                    log4net.GlobalContext.Properties["url"] = ex.TargetSite.ToString();

                    // Use error or higher to send exception to logger
                    log.Error(ex.Message.ToString());

                    return View("Error");
                }
            }
            else
                return View(model);

        }

        public ActionResult EditInterface(int interId)
        {
            try
            {
                using (var db = new assetregisterSQLEntities())
                {
                    var record = (from inter in db.Interfaces
                                  where inter.InterfaceID == interId
                                  select inter).FirstOrDefault();

                    List<IPAddressList> IPs = new List<IPAddressList>();
                    IPs = (from ip in db.IPAddressesNews
                           where ip.InterfaceID == interId
                           select new IPAddressList
                           {
                               IPAddress = ip.IPAddress
                           }).ToList();


                    if (record != null)
                    {
                        return View(new EditInterfaceViewModel()
                        {
                            InterfaceName = record.InterfaceName,
                            InterfaceID = record.InterfaceID,
                            MachineID = record.MachineID,
                            IpAddresses = IPs
                        });
                    }
                    else
                    {
                        return View(new EditInterfaceViewModel());
                    }
                }
            }
            catch (Exception ex)
            {
                // log4net custom variables for log output
                log4net.GlobalContext.Properties["url"] = ex.TargetSite.ToString();

                // Use error or higher to send exception to logger
                log.Error(ex.Message.ToString());

                return View("Error");
            }
        }

        [HttpPost]
        public ActionResult EditInterface(EditInterfaceViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var db = new assetregisterSQLEntities())
                    {
                        var record = (from mac in db.Machines
                                      where mac.MachineID == model.MachineID
                                      select mac).FirstOrDefault();
                        if (record != null)
                        {
                            return RedirectToAction("AddEditMachine", new { machineName = record.Machine_Name });
                        }
                    }
                }
                catch (Exception ex)
                {
                    // log4net custom variables for log output
                    log4net.GlobalContext.Properties["url"] = ex.TargetSite.ToString();

                    // Use error or higher to send exception to logger
                    log.Error(ex.Message.ToString());

                    return View("Error");
                }

            }
            return View();
        }

        public ActionResult DeleteIP(string ipAddress)
        {
            return View();
        }

        [HttpPost]
        public ActionResult DeleteIP(DeleteIpViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var db = new assetregisterSQLEntities())
                    {
                        var record = (from ip in db.IPAddressesNews
                                      where ip.IPAddress == model.IPAddress
                                      select ip).FirstOrDefault();

                        var res = (from inter in db.Interfaces
                                   where inter.InterfaceID == record.InterfaceID
                                   select inter).FirstOrDefault();

                        if (record != null)
                        {
                            record.InterfaceID = null;
                            db.SaveChanges();
                            return RedirectToAction("EditInterface", new { interId = res.InterfaceID });
                        }
                        else
                            return View(model);

                    }
                }
                catch (Exception ex)
                {
                    // log4net custom variables for log output
                    log4net.GlobalContext.Properties["url"] = ex.TargetSite.ToString();

                    // Use error or higher to send exception to logger
                    log.Error(ex.Message.ToString());

                    return View("Error");
                }
            }
            else
                return View(model);
        }

        public ActionResult AddIP(int interID)
        {
            try
            {
                using (var db = new assetregisterSQLEntities())
                {
                    List<SubnetTypeList> Subnets = new List<SubnetTypeList>();
                    Subnets = (from sub in db.IPSubnetTypes
                               select new SubnetTypeList
                               {
                                   IPSubType = sub.IPSubnetType1
                               }).Distinct().ToList();

                    return View(new AddIPViewModel()
                    {
                        SubnetTypes = Subnets,
                        InterfaceID = interID
                    });
                }

            }
            catch (Exception ex)
            {
                // log4net custom variables for log output
                log4net.GlobalContext.Properties["url"] = ex.TargetSite.ToString();

                // Use error or higher to send exception to logger
                log.Error(ex.Message.ToString());

                return View("Error");
            }
        }

        [HttpPost]
        public ActionResult AddIP(AddIPViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var db = new assetregisterSQLEntities())
                    {
                        if (model.InterfaceID != 0)
                        {
                            var record = (from ip in db.IPAddressesNews
                                          where ip.IPAddress == model.UnMappedIPs
                                          select ip).FirstOrDefault();
                            if (record != null)
                            {
                                record.InterfaceID = model.InterfaceID;
                                record.MappedIP = model.MappedIP;
                            }
                            db.SaveChanges();
                            return RedirectToAction("EditInterface", new { interId = model.InterfaceID });
                        }
                        else
                            return View(new AddIPViewModel());
                    }

                }
                catch (Exception ex)
                {
                    // log4net custom variables for log output
                    log4net.GlobalContext.Properties["url"] = ex.TargetSite.ToString();

                    // Use error or higher to send exception to logger
                    log.Error(ex.Message.ToString());

                    return View("Error");
                }
            }
            else
                return View(model);
        }

        public JsonResult GetIPSubnet(string ipsubType)
        {
            try
            {
                using (var db = new assetregisterSQLEntities())
                {
                    var records = (from ipSub in db.IPSubnets
                                   where ipSub.IPSubnetType == ipsubType
                                   select new IPSubnetList
                                   {
                                       IPSubnetID = ipSub.IPSubnetID
                                   }).ToList();

                    return Json(records, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                // log4net custom variables for log output
                log4net.GlobalContext.Properties["url"] = ex.TargetSite.ToString();

                // Use error or higher to send exception to logger
                log.Error(ex.Message.ToString());

                var records = new IPSubnetList
                {
                    IPSubnetID = "0"
                };

                return Json(records, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetUnmappedIP(string ipsubID)
        {
            try
            {
                using (var db = new assetregisterSQLEntities())
                {
                    var records = (from ip in db.IPAddressesNews
                                   where ip.IpSubnetId == ipsubID && ip.InterfaceID == null
                                   select new IPAddressList
                                   {
                                       IPAddress = ip.IPAddress
                                   }).ToList();

                    return Json(records, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                // log4net custom variables for log output
                log4net.GlobalContext.Properties["url"] = ex.TargetSite.ToString();

                // Use error or higher to send exception to logger
                log.Error(ex.Message.ToString());

                var records = new IPAddressList
                {
                    IPAddress = "0.0.0.0"
                };

                return Json(records, JsonRequestBehavior.AllowGet);
            }
        }

        //***START - RDP Connection Controllers ***//
        public ActionResult GenerateRDPFile(RdpViewModel model)
        {
            if (string.IsNullOrEmpty(model.HostIP))
                return View();

            var content = string.Format(
                @"screen mode id:i:1
desktopwidth:i:1024
desktopheight:i:768
smart sizing:i:1
session bpp:i:16
compression:i:1
keyboardhook:i:2
audiomode:i:2
redirectdrives:i:0
redirectprinters:i:0
redirectcomports:i:0
redirectsmartcards:i:1
displayconnectionbar:i:1
autoreconnection enabled:i:1
alternate shell:s:
shell working directory:s:
disable wallpaper:i:1
disable full window drag:i:1
disable menu anims:i:1
disable themes:i:1
disable cursor setting:i:0
bitmapcachepersistenable:i:1
full address:s:{0}
username:s:{1}
domain:s:null
", model.HostIP, model.UserName);

            return new FileContentResult(Encoding.UTF8.GetBytes(content),
                "application/rdp")
            {
                FileDownloadName = model.HostIP + ".rdp"
            };

        }

        public ActionResult SearchCustomerData()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SearchCustomerData(SearchCustomerDataModel model)

        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var db = new assetregisterSQLEntities())
                    {
                        var recordes = (from customer in db.Customers
                                        where customer.CustomerNumber == model.SearchPhrase
                                        || customer.Customer_Name.ToLower().Contains(model.SearchPhrase.ToLower())
                                        select new _Customer()
                                        {
                                            CustomerName = customer.Customer_Name,
                                            CustomerNumber = customer.CustomerNumber,
                                            Users = (from user in db.Users
                                                     where user.CustomerNumber == customer.CustomerNumber
                                                     select new _User()
                                                     {
                                                         UserName = user.UserName,
                                                         Password = user.Password
                                                     }).ToList(),
                                            MachineNames = (from machine in db.Machines
                                                            where machine.CustomerNumber == customer.CustomerNumber
                                                            select new _Machine()
                                                            {
                                                                MachineName = machine.Machine_Name,
                                                                MachineID = machine.MachineID.ToString(),
                                                                InterfaceNames = (from inter in db.Interfaces
                                                                                  where inter.MachineID == machine.MachineID
                                                                                  select new _Interface()
                                                                                  {
                                                                                      InterfaceName = inter.InterfaceName,
                                                                                      InterfaceID = inter.InterfaceID,
                                                                                      IpAddress = (from ip in db.IPAddressesNews
                                                                                                   where ip.InterfaceID == inter.InterfaceID
                                                                                                   select new _IPAddress()
                                                                                                   {
                                                                                                       PublicIPAddress = ip.IPAddress,
                                                                                                       MappedIP = ip.MappedIP
                                                                                                   }).ToList<_IPAddress>()
                                                                                  }).ToList<_Interface>()
                                                            }).ToList<_Machine>()
                                        }).ToList<_Customer>();


                        if (recordes != null)
                        {
                            return View(new SearchCustomerDataModel() { Customers = recordes });
                        }
                        else
                            return View(model);
                    }
                }
                catch (Exception ex)
                {
                    // log4net custom variables for log output
                    log4net.GlobalContext.Properties["url"] = ex.TargetSite.ToString();

                    // Use error or higher to send exception to logger
                    log.Error(ex.Message.ToString());

                    return View("Error");
                }
            }
            else
                return View(model);
        }

        public JsonResult GetIpAddress(int interID)
        {
            try
            {
                using (var db = new assetregisterSQLEntities())
                {
                    var records = (from i in db.IPAddressesNews
                                   where i.InterfaceID == interID
                                   select new _IPAddress()
                                   {
                                       PublicIPAddress = i.IPAddress,
                                       MappedIP = i.MappedIP
                                   }).ToList();

                    return Json(records, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message, e.InnerException);
            }
        }

        public JsonResult GetCustomerName(string searchPhrase)
        {
            try
            {
                using (var db = new assetregisterSQLEntities())
                {
                    var CustomerNames = (from cus in db.Customers
                                         where cus.Customer_Name.ToLower().Contains(searchPhrase.ToLower())
                                         select cus.Customer_Name).ToList();

                    return Json(CustomerNames, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception e)
            {
                throw new Exception(e.Message, e.InnerException);
            }
        }

        public ActionResult GetSelectedRow()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetSelectedRow(GetSelectedRowDataModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var db = new assetregisterSQLEntities())
                    {
                        var row = (from cus in db.Customers
                                   where cus.CustomerNumber == model.SelectedCustomerNum
                                   select new SelectedCustomer()
                                   {
                                       CustomerName = cus.Customer_Name,
                                       CustomerNumber = cus.CustomerNumber,
                                       User = (from user in db.Users
                                               where user.CustomerNumber == cus.CustomerNumber &&
                                                user.UserName == model.SelectedUserName
                                               select new SelectedUser()
                                               {
                                                   UserName = user.UserName,
                                                   Password = user.Password
                                               }).FirstOrDefault(),
                                       Machine = (from machine in db.Machines
                                                  where machine.CustomerNumber == cus.CustomerNumber &&
                                                  machine.MachineID == model.SelectedMachineId
                                                  select new SelectedMachine()
                                                  {
                                                      MachineID = machine.MachineID,
                                                      MachineName = machine.Machine_Name,
                                                      Inter = (from inter in db.Interfaces
                                                               where inter.InterfaceID == model.SelectedInterfaceId
                                                               select new SelectedInterface()
                                                               {
                                                                   InterfaceID = inter.InterfaceID,
                                                                   InterfaceName = inter.InterfaceName,
                                                                   //IPAddress = (from ip in db.IPAddressesNews
                                                                   //             where ip.InterfaceID == inter.InterfaceID &&
                                                                   //             ip.InterfaceID == model.SelectedInterfaceId
                                                                   //             select new SelectedIPAddress()
                                                                   //             {
                                                                   //                 PublicIP = ip.IPAddress,
                                                                   //                 PrivateIP = ip.MappedIP
                                                                   //             }).FirstOrDefault(),
                                                               }).FirstOrDefault(),

                                                  }).FirstOrDefault(),
                                   }).FirstOrDefault();
                        if (row != null)
                        {
                            model.SelectedIPAddr = RemoveBetween(model.SelectedIPAddr, '(', ')'); //Remove the private IP address
                            model.SelectedIPAddr = model.SelectedIPAddr.Replace(" ", ""); //Remove the whitespace from the end of the address
                            return View(new GetSelectedRowDataModel() { Customer = row, SelectedIPAddr = model.SelectedIPAddr });
                        }
                        else
                            return View(model);
                    }
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message, e.InnerException);
                }
            }
            else
                return View(model);
        }

        public string RemoveBetween(string s, char begin, char end)
        {
            Regex regex = new Regex(string.Format("\\{0}.*?\\{1}", begin, end));
            return regex.Replace(s, string.Empty);
        }

        public ActionResult LoadCustomers(string custNum)
        {
            try
            {
                using (var db = new assetregisterSQLEntities())
                {
                    var records = (from customer in db.Customers
                                   where customer.CustomerNumber == custNum
                                   select new _Customer()
                                   {
                                       CustomerName = customer.Customer_Name,
                                       CustomerNumber = customer.CustomerNumber,
                                       MachineNames = (from machine in db.Machines
                                                       where machine.CustomerNumber == customer.CustomerNumber
                                                       select new _Machine()
                                                       {
                                                           MachineName = machine.Machine_Name,
                                                           MachineID = machine.MachineID.ToString(),
                                                           InterfaceNames = (from inter in db.Interfaces
                                                                             where inter.MachineID == machine.MachineID
                                                                             select new _Interface()
                                                                             {
                                                                                 InterfaceName = inter.InterfaceName,
                                                                                 IpAddress = (from ip in db.IPAddressesNews
                                                                                              where ip.InterfaceID == inter.InterfaceID
                                                                                              select new _IPAddress()
                                                                                              {
                                                                                                  PublicIPAddress = ip.IPAddress,
                                                                                                  MappedIP = ip.MappedIP
                                                                                              }).ToList<_IPAddress>()
                                                                             }).ToList<_Interface>()
                                                       }).ToList<_Machine>()
                                   }).ToList<_Customer>();

                    if (records != null)
                    {
                        return View(new LoadCustomersModel() { Customers = records });
                    }
                    else
                        return View();
                }
            }
            catch (Exception ex)
            {
                // log4net custom variables for log output
                log4net.GlobalContext.Properties["url"] = ex.TargetSite.ToString();

                // Use error or higher to send exception to logger
                log.Error(ex.Message.ToString());

                return View("Error");
            }


            //    var records = (from customer in db.Customers
            //                   join machine in db.Machines on customer.CustomerNumber equals machine.CustomerNumber
            //                   join inter in db.Interfaces on machine.MachineID equals inter.MachineID
            //                   join ipaddnew in db.IPAddressesNews on inter.InterfaceID equals ipaddnew.InterfaceID
            //                   where customer.CustomerNumber == custNum | customer.Customer_Name == custName
            //                   select new
            //                   {
            //                       CustomerName = customer.Customer_Name,
            //                       CustomerNumber = customer.CustomerNumber,
            //                       MachineID = machine.MachineID,
            //                       MachineName = machine.Machine_Name,
            //                       InterfaceName = inter.InterfaceName,
            //                       ExternalIpAdd = ipaddnew.IPAddress,
            //                       InternalIpAdd = ipaddnew.MappedIP
            //                   }).ToList();
            //    if (records != null)
            //    {
            //        LoadCustomersModel model = new Models.LoadCustomersModel();
            //        foreach (var item in records)
            //        {
            //        model.Customers.Add(new _Customer()
            //            {
            //                CustomerName = item.CustomerName,
            //                CustomerNumber = item.CustomerNumber,
            //                MachineId = item.MachineID,
            //                MachineName = item.MachineName,
            //                InterfaceName = item.InterfaceName,
            //                IpAddress = item.ExternalIpAdd,
            //                MappedIp = item.InternalIpAdd
            //            });
            //        }
            //        return View(model);
            //    }
            //    else
            //        return View();
            //}
        }
        //***END - RDP Connection Controllers ***//
    }
}