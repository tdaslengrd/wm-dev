﻿# Start Time command
$sw = [Diagnostics.Stopwatch]::StartNew()

#SQL Connection Info
$SQLServer = "localhost"
$SQLDBName = "NetflowData"
$uid ="netflowpsuser"
$pwd = "Passw0rd"
$ConnectionString = "Data Source=$SQLServer;Initial Catalog=$SQLDBName; Integrated Security = False; User ID = $uid; Password = $pwd;"

$Month = "1";
$Year = "2017";
$DaysInMonth = [datetime]::DaysInMonth($Year,$Month);
#$DaysInMonth = "5"; #To parse a specific day, change this value to be the date(day).

$FTP_Path = "E:\FileZilla Server\netflowdata"
$CleanUp = $false

function Write-Log 
{ 
    [CmdletBinding()] 
    Param 
    ( 
        [Parameter(Mandatory=$true, 
                   ValueFromPipelineByPropertyName=$true)] 
        [ValidateNotNullOrEmpty()] 
        [Alias("LogContent")] 
        [string]$Message, 
 
        [Parameter(Mandatory=$false)] 
        [Alias('LogPath')] 
        [string]$Path='C:\Logs\PowerShellLog.log', 
         
        [Parameter(Mandatory=$false)] 
        [ValidateSet("Error","Warn","Info")] 
        [string]$Level="Info", 
         
        [Parameter(Mandatory=$false)] 
        [switch]$NoClobber 
    ) 
 
    Begin 
    { 
        # Set VerbosePreference to Continue so that verbose messages are displayed. 
        $VerbosePreference = 'Continue' 
    } 
    Process 
    { 
         
        # If the file already exists and NoClobber was specified, do not write to the log. 
        if ((Test-Path $Path) -AND $NoClobber) { 
            Write-Error "Log file $Path already exists, and you specified NoClobber. Either delete the file or specify a different name." 
            Return 
            } 
 
        # If attempting to write to a log file in a folder/path that doesn't exist create the file including the path. 
        elseif (!(Test-Path $Path)) { 
            Write-Verbose "Creating $Path." 
            $NewLogFile = New-Item $Path -Force -ItemType File 
            } 
 
        else { 
            # Nothing to see here yet. 
            } 
 
        # Format Date for our Log File 
        $FormattedDate = Get-Date -Format "yyyy-MM-dd HH:mm:ss" 
 
        # Write message to error, warning, or verbose pipeline and specify $LevelText 
        switch ($Level) { 
            'Error' { 
                Write-Error $Message 
                $LevelText = 'ERROR:' 
                } 
            'Warn' { 
                Write-Warning $Message 
                $LevelText = 'WARNING:' 
                } 
            'Info' { 
                Write-Verbose $Message 
                $LevelText = 'INFO:' 
                } 
            } 
         
        # Write log entry to $Path 
        "$FormattedDate $LevelText $Message" | Out-File -FilePath $Path -Append 
    } 
    End 
    { 
    } 
}

for($i=1; $i -le $DaysInMonth; $i++) #To parse a specific day, change the value of i
{
    #$DayLength = $i.ToString().Length
    #if($DayLength -ne 2)
    #{
    #    $Day="0" + $i.ToString()
    #}
    #else
    #{
        $Day=$i
    #}

    #$YesterdayDate = [DateTime]::Today.AddDays(-1)
    $YesterdayDate = "$Year-$Month-$Day";
    $CurrentDate = Get-Date $YesterdayDate -format yyyy-MM-d
    #$Date = Get-Date -format dd-MM-yyyy
    $Date = "$Day-$Month-$Year"
    #$TableName = Get-Date $YesterdayDate -format yyyyMMdd
    #$TableName = "nfd" + $TableName
    $MonthName = (Get-Culture).DateTimeFormat.GetMonthName($Month)
    
    $BasePath = "C:\etc\Scripts\NetflowImport\Logs\$Date.txt"

    try
    {
        Write-Log -Message "Starting CSV export to SQL for Netflow data." -Path $BasePath -Level Info

        if($CleanUp -eq $false)
        {
            Write-Log -Message "Cleanup disabled! Manual cleanup of $FTP_Path will be needed." -Path $BasePath -Level Warn
        }
        else
        {
            Write-Log -Message "Cleanup enabled!" -Path $BasePath -Level Info
        }

        #Load extensions needed to perform Bulk import
        [System.Reflection.Assembly]::LoadFrom("C:\etc\Scripts\NetflowImport\CsvDataReader.dll") | Out-Null

        #Check if there is a valid data directory
        if ((Test-Path "E:\FileZilla Server\netflowdata\$CurrentDate") -eq $True)
        {
            #Loop through each CSV file in the FTP directory and load it into SQL
            get-childitem "E:\FileZilla Server\netflowdata\$CurrentDate" -recurse | where {$_.extension -eq ".csv"} | % {
                #Check if the file is for inbound or outbound data, set the tableName and create the table in the database if it does not exist
                if($_ -like "*in*")
                {
                    $DirectionIndicator = "In"
                    $TableName = "nfd_in_" + $MonthName + "_" + $Year
$createTable = @"
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[$TableName]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[$TableName]
	(
        [Id] [int] IDENTITY(1,1) NOT NULL,
		[dIP] [varchar](50) NULL,
        [bytes] [bigint] NULL,
		[dt] [datetime] NULL,
	)
END;
"@
                }
                elseif($_ -like "*out*")
                {
                    $DirectionIndicator = "Out"
                    $TableName = "nfd_out_" + $MonthName + "_" + $Year
$createTable = @"
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[$TableName]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[$TableName]
	(
        [Id] [int] IDENTITY(1,1) NOT NULL,
		[sIP] [varchar](50) NULL,
        [bytes] [bigint] NULL,
        [dt] [datetime] NULL,
	)
END;
"@
                }
                else
                {
                    Write-Log -Message "Unable to determine the traffic direction on file $_" -Path $BasePath -Level Error
                    Continue
                }
                
                #Lets create a database table for the billing day, incase this is rerun we will also check if the table already exists
                Invoke-Sqlcmd -Username $uid -Password $pwd -ServerInstance $SQLServer -Database $SQLDBName -Query $createTable

                Write-Log -Message "Processing file - $_" -Path $BasePath -Level Info

                If (($_.Length) -eq 0)
                {
                    Write-Log -Message "File $_ is blank, skipping..." -Path $BasePath -Level Warn
                }
                else
                {
                    $reader = New-Object SqlUtilities.CsvDataReader($_.FullName)

                    $bulkCopy = new-object ("Data.SqlClient.SqlBulkCopy") $ConnectionString
                    $bulkCopy.DestinationTableName = $TableName
                    $bulkCopy.BatchSize = 50000
                    $bulkCopy.BulkCopyTimeout = 0

                    if($DirectionIndicator -eq 'In')
                    {
                        #Column mapping
                        $bulkCopy.ColumnMappings.Add("dIP", "dIP") | Out-Null
                        $bulkCopy.ColumnMappings.Add("bytes", "bytes") | Out-Null
                        $bulkCopy.ColumnMappings.Add("dt", "dt") | Out-Null
                    }
                    else
                    {
                        #Column mapping
                        $bulkCopy.ColumnMappings.Add("sIP", "sIP") | Out-Null
                        $bulkCopy.ColumnMappings.Add("bytes", "bytes") | Out-Null
                        $bulkCopy.ColumnMappings.Add("dt", "dt") | Out-Null
                    }

                    $bulkCopy.WriteToServer($reader);

                    $reader.Close();
                    $reader.Dispose();
                }
                if ($_.Exception.Message -eq $null)
                {
                    Write-Log -Message "Sucessfully loaded $_ into database." -Path $BasePath -Level Info
                    if($CleanUp -eq $true)
                    {
                        Remove-Item -Force $_.FullName
                        Write-Log -Message "Removed $_" -Path $BasePath -Level Info
                    }
                }
            }
        }
        else
        {
            Write-Log -Message "No valid files to process!" -Path $BasePath -Level Warn
        }
    }
    catch
    {
        Write-Log -Message "Failed processing file $_" -Path $BasePath -Level Error
        Write-Log -Message "Error - $_.Exception.Message" -Path $BasePath -Level Error
        write-host $_.Exception | format-list -force
    }
    finally
    {
        if ($_.Exception.Message -eq $null -and $CleanUp -eq $true)
        {
            Write-Log -Message "Cleaning up..." -Path $BasePath -Level Info
            Get-ChildItem -Path "E:\FileZilla Server\netflowdata\$CurrentDate\" -Recurse | Remove-Item -force -recurse
            Remove-Item "E:\FileZilla Server\netflowdata\$CurrentDate" -Recurse -Force
        }
        # Stop Time
        $sw.Stop()
        # Store Elapsed time into desired vars
        $min = $sw.Elapsed.Minutes
        # Display timer statistics to host
        Write-Log -Message "Total elapsed time $min minutes." -Path $BasePath -Level Info

        Write-Log -Message "Job Finished." -Path $BasePath -Level Info
    }
}