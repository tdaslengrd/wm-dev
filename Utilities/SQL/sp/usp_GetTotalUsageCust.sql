USE [NetflowResultsData]
GO

/****** Object:  StoredProcedure [dbo].[usp_GetTotalUsageCust]    Script Date: 20/10/2017 10:39:11 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_GetTotalUsageCust] @Month nvarchar(30), @CustomerName nvarchar(30)
AS
DECLARE @cmd_genReport nvarchar(max)
SET @cmd_genReport = '
SELECT ROUND(CAST(SUM(E.inbytes) AS Float)/1073741824,5) AS total_inGBytes, ROUND(CAST(SUM(E.outbytes) AS Float)/1073741824,5) AS total_outGBytes, E.dt
FROM assetregisterSQL.dbo.IPAddressesNew A
inner join assetregisterSQL.dbo.Interfaces B on A.InterfaceID = B.InterfaceID
inner join assetregisterSQL.dbo.Machines C on C.MachineID = B.MachineID
inner join assetregisterSQL.dbo.Customers D on D.CustomerNumber = C.CustomerNumber
inner join NetflowResultsData.dbo.' + @Month + ' E on A.IPAddress = E.ipaddr
WHERE D.[Customer Name] LIKE ''' + @CustomerName + '''
GROUP BY E.dt
ORDER BY E.dt DESC;'
EXEC sp_executesql @cmd_genReport
GO

