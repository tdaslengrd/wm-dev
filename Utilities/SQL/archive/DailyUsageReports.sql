--PRIOR TO RUNNING THIS SCRIPT, MAKE SURE MAXDOP IS SET CORRECTLY
--FAILING TO DO SO WILL CAUSE THREAD EXECUTION EXHAUSTION WHICH WILL CAUSE THE SCRIPT TO HALT!

-- Declare the variable to be used.--
DECLARE @currentDate varchar(20);
DECLARE @yesterdayDate varchar(20);
DECLARE @MessageOutput varchar(255);
DECLARE @ipaddr varchar(50);
DECLARE @dateTime datetime;
DECLARE @NetflowRecordCount int;
DECLARE @outboundCount bigint;
DECLARE @UniqueIPs TABLE(ipaddr varchar (50));
--DECLARE @ResultTable TABLE(ip varchar(50), inbytes bigint, outbytes bigint, dt datetime);
DECLARE @cmd_createTable varchar(255);
DECLARE @cmd_fillOutTable nvarchar(max);
DECLARE @cmd_insertTable nvarchar(4000);
DECLARE @passVars nvarchar(255);
DECLARE @site_value INT;
DECLARE @t1 DATETIME;
DECLARE @t2 DATETIME;
DECLARE @tdiff varchar(100);
DECLARE @static_time datetime;
DECLARE @static_time_yesterday varchar(10);
DECLARE @time_convert datetime;

--Start the main process--
BEGIN
	SET @t1 = GETDATE();
	SET @currentDate = (SELECT CONVERT(date,getdate()));
	--The resolution of reporting is daily, as such we will take the opportunity to set the dateTime variable to a static figure for the rest of the script.
	SET @dateTime = (SELECT CONVERT(date,getdate()-1));
	SET @yesterdayDate = (SELECT CONVERT(VARCHAR(8), GETDATE()-1, 112));
	SET @yesterdayDate = 'nfd' + @yesterdayDate;

	--Create the destination table to store the processed results--
	DECLARE @yyyyMM_TableName nvarchar(50);
	DECLARE @createTable nvarchar(max);
	SET @yyyyMM_TableName = (SELECT DATENAME(month,GETDATE()) + '_' + DATENAME(year,GETDATE()));
	SET @createTable = 'IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[dbo].['+@yyyyMM_TableName+']'') AND type in (N''U''))
	BEGIN
		CREATE TABLE [NetflowResultsData].[dbo].['+@yyyyMM_TableName+'] (Id INT NOT NULL PRIMARY KEY IDENTITY(1,1),ipaddr varchar(50),inbytes bigint,outbytes bigint,dt DATETIME);
	END'
	EXEC (@createTable)

	PRINT 'Processing ' + @yesterdayDate + ' data.';

	CREATE TABLE #NetflowTable_tmp(ID int IDENTITY(1,1) NOT NULL, ipaddr varchar(50), inbytes bigint, outbytes bigint, dt datetime, UNIQUE NONCLUSTERED (ipaddr, ID));

	SET @cmd_fillOutTable = '
			select srcdata.ipaddr, srcdata.outbytes, destdata.inbytes, srcdata.dt
			from 
				(SELECT DISTINCT dest AS ipaddr, SUM(bytes) AS inbytes, [datetime] AS dt
				FROM [NetflowData].[dbo].[' + @yesterdayDate + ']
				WHERE dest LIKE ''202.191.[48-55]%'' OR dest LIKE ''119.161.[32-47]%''
				GROUP BY dest, [datetime]) destdata
			inner join
				(SELECT DISTINCT src AS ipaddr, SUM(bytes) AS outbytes, [datetime] AS dt
				FROM [NetflowData].[dbo].[' + @yesterdayDate + ']
				WHERE src LIKE ''202.191.[48-55]%'' OR src LIKE ''119.161.[32-47]%''
				GROUP BY src, [datetime]) srcdata
			on
				destdata.ipaddr = srcdata.ipaddr
			order by
				srcdata.ipaddr DESC;';

	INSERT INTO #NetflowTable_tmp EXEC (@cmd_fillOutTable);

	DECLARE @cmd_something nvarchar(max);

	SET @cmd_something = 'INSERT INTO [NetflowResultsData].[dbo].[' + @yyyyMM_TableName + '] (ipaddr, inbytes, outbytes, dt)
	SELECT T.ipaddr, T.inbytes, T.outbytes, CAST(CAST(T.dt AS DATE)AS DATETIME) AS dt
	FROM #NetflowTable_tmp T;';

	EXEC sp_executesql @cmd_something;

	DROP TABLE #NetflowTable_tmp;

	SET @t2 = GETDATE();
	SET @tdiff = DATEDIFF(MINUTE,@t1,@t2);
	PRINT 'Time elapsed - ' + @tdiff + ' minutes.';
END