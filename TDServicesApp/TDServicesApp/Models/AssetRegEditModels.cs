﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System;
using TDServicesApp.Models.AssetRegObjects;
using System.ComponentModel.DataAnnotations;

namespace TDServicesApp.Models
{
    public class EditInterfaceViewModel
    {
        public int InterfaceID { get; set; }
        public string InterfaceName { get; set; }
        public Nullable<int> MachineID { get; set; }
        public List<IPAddressList> IpAddresses { get; set; }
    }
    public class EditIpViewModel
    {
        public string CurrentIP { get; set; }
        public int InterfaceID { get; set; }
        //public string InterfaceName { get; set; }
        public int MachineID { get; set; }
        public List<SubnetTypeList> SubnetTypes { get; set; }

        [Required(ErrorMessage = "Please select a subnet type from the dropdownlist")]
        public string SelectedSubnetType { get; set; }

        [Required(ErrorMessage = "Please select an IP subnet from the dropdownlist")]
        public string IpLists { get; set; }

        [Required(ErrorMessage = "Please select an IP from the dropdownlist")]
        public string UnMappedIPs { get; set; }

        public string MappedIP { get; set; }

        public EditIpViewModel()
        {
            SubnetTypes = new List<SubnetTypeList>();

        }
    }
}