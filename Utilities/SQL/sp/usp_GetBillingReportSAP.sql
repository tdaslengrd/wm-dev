USE [NetflowResultsData]
GO

/****** Object:  StoredProcedure [dbo].[usp_GetBillingReportSAP]    Script Date: 12/12/2017 8:06:47 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GetBillingReportSAP] @Month nvarchar(30)
AS
DECLARE @cmd_genReport nvarchar(max)
SET @cmd_genReport = '
SELECT DISTINCT(Test.Date) AS Date, sum(Test.InBytes) AS OutBytes, sum(Test.OutBytes) AS InBytes, Test.Contract
FROM
	(SELECT distinct(CAST(CONVERT(datetime, SWITCHOFFSET(CONVERT(datetimeoffset, E.dt), DATENAME(TzOffset, SYSDATETIMEOFFSET()))) AS DATE)) AS Date, C.contract_num_sap AS Contract, sum(E.inbytes) AS InBytes, sum(E.outbytes) AS OutBytes
	FROM assetregisterSQL.dbo.IPAddressesNew A
	inner join assetregisterSQL.dbo.Interfaces B on A.InterfaceID = B.InterfaceID
	inner join assetregisterSQL.dbo.Machines C on C.MachineID = B.MachineID
	inner join assetregisterSQL.dbo.Customers D on D.CustomerNumber = C.CustomerNumber
	inner join NetflowResultsData.dbo.' + @Month + ' E on A.IPAddress = E.ipaddr AND C.contract_num_sap IS NOT NULL
	GROUP BY C.contract_num_sap, E.dt) Test
GROUP BY Test.Date, Test.Contract
ORDER BY Test.Contract, Test.Date ASC'
EXEC sp_executesql @cmd_genReport
GO

