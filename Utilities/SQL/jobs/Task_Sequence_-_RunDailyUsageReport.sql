--PRIOR TO RUNNING THIS SCRIPT, MAKE SURE MAXDOP IS SET CORRECTLY
--FAILING TO DO SO WILL CAUSE THREAD EXECUTION EXHAUSTION WHICH WILL CAUSE THE SCRIPT TO HALT!

--Before running this script, set Line 31 to the date from when you want to start parsing records from.
--For example,-If you set line 31 to 2017-08-01 this will parse all data for the month of August.

--Start the main process--
BEGIN
	BEGIN TRY
		-- Declare the variable to be used.--
		DECLARE @ipaddr varchar(50);
		DECLARE @dateTime datetime;
		DECLARE @cmd_createTable varchar(255);
		DECLARE @cmd_fillOutTable nvarchar(max);
		DECLARE @cmd_insertTable nvarchar(max);
		DECLARE @t1 DATETIME;
		DECLARE @t2 DATETIME;
		DECLARE @tdiff varchar(100);
		DECLARE @yyyyMM_TableName nvarchar(50);
		DECLARE @createTable nvarchar(max);
		DECLARE @inTable nvarchar(50);
		DECLARE @outTable nvarchar(50);
		DECLARE @setDate nvarchar(50);
		DECLARE @monthName nvarchar(50);
		DECLARE @year nvarchar(50);

		SET @setDate = dateadd(day,-1, CONVERT(date, getdate(), 120))
		SET @monthName = DATENAME(month, @setDate)
		SET @year = DATENAME(year, @setDate)

		SET @yyyyMM_TableName = @monthName + '_' + @year;
		SET @createTable = 'IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N''['+@yyyyMM_TableName+']'')
		BEGIN
			CREATE TABLE [NetflowResultsData].[dbo].['+@yyyyMM_TableName+'] (Id INT NOT NULL PRIMARY KEY IDENTITY(1,1),ipaddr varchar(50),inbytes bigint,outbytes bigint,dt DATETIME);
		END'

		EXEC (@createTable)

		SET @t1 = GETDATE();
		SET @inTable = 'nfd_in_' + @monthName + '_' + @year;
		SET @outTable = 'nfd_out_' + @monthName + '_' + @year;

		CREATE TABLE #NetflowTable_tmp(ID int IDENTITY(1,1) NOT NULL, ipaddr nvarchar(50), inbytes bigint, outbytes bigint, dt nvarchar(50), UNIQUE NONCLUSTERED (ipaddr, ID));

		SET @cmd_fillOutTable = '
								SELECT
								CASE
									WHEN InOutData.outipaddr IS NULL
										THEN InOutData.inipaddr
										ELSE InOutData.outipaddr
									END
								AS ipaddr,
								ISNULL(InOutData.outbytes, 0) AS outbytes,
								ISNULL(InOutData.inbytes, 0) AS inbytes,
								CASE
									WHEN InOutData.outdt IS NULL
										THEN InOutData.indt
										ELSE InOutData.outdt
									END
								AS dt
								FROM
									(SELECT sIPdata2.outipaddr as outipaddr, dIPdata2.inipaddr AS inipaddr, sIPdata2.outbytes AS outbytes, dIPdata2.inbytes AS inbytes, sIPdata2.outdt AS outdt, dIPdata2.indt AS indt
									FROM
										(SELECT distinct(dIPdata.ipaddr) AS inipaddr, dIPdata.dt AS indt, sum(dIPdata.inbytes) AS inbytes
										FROM
											(SELECT DISTINCT DATEADD(HH,DATEPART(HH,dt),CAST(CAST(dt AS DATE) AS DATETIME)) AS dt, dIP AS ipaddr, SUM(bytes) AS inbytes
											FROM [NetflowData].[dbo].[' + @inTable + ']
											WHERE CAST ([dt] as DATE) = convert(date,CONVERT(varchar(10),'''+@setDate+''',120))
												GROUP BY dIP, dt) dIPdata
											GROUP BY ipaddr, dt) dIPdata2
										full join
										(SELECT distinct(sIPdata.ipaddr) AS outipaddr, sIPdata.dt AS outdt, sum(sIPdata.outbytes) AS outbytes
										FROM
											(SELECT DISTINCT DATEADD(HH,DATEPART(HH,dt),CAST(CAST(dt AS DATE) AS DATETIME)) AS dt, sIP AS ipaddr, SUM(bytes) AS outbytes
											FROM [NetflowData].[dbo].[' + @outTable + ']
											WHERE CAST ([dt] as DATE) = convert(date,CONVERT(varchar(10),'''+@setDate+''',120))
												GROUP BY sIP, dt) sIPdata
											GROUP BY ipaddr, dt) sIPdata2
										ON sIPdata2.outipaddr = dIPdata2.inipaddr AND sIPdata2.outdt = dIPdata2.indt) InOutData
									GROUP BY inipaddr, outipaddr, inbytes, outbytes, outdt, indt
								ORDER BY dt';

		INSERT INTO #NetflowTable_tmp EXEC (@cmd_fillOutTable);

		SET @cmd_insertTable = 'INSERT INTO [NetflowResultsData].[dbo].[' + @yyyyMM_TableName + '] (ipaddr, inbytes, outbytes, dt)
		SELECT T.ipaddr, T.inbytes, T.outbytes, (CONVERT(DATETIME, T.dt, 120)) AS dt
		FROM #NetflowTable_tmp T;';

		EXEC sp_executesql @cmd_insertTable;

		DROP TABLE #NetflowTable_tmp;

		SET @t2 = GETDATE();
		SET @tdiff = DATEDIFF(MINUTE,@t1,@t2);
		PRINT 'Time elapsed - ' + @tdiff + ' minutes.';
	END TRY
	BEGIN CATCH  
		SELECT   
			ERROR_NUMBER() AS ErrorNumber  
			,ERROR_MESSAGE() AS ErrorMessage;
			DROP TABLE #NetflowTable_tmp;
	END CATCH
END;