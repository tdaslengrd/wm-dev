﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TDServicesApp.Models.AssetRegObjects
{
    public class _IPAddress
    {
        public string PublicIPAddress { get; set; }
        public string MappedIP { get; set; }
    }
}