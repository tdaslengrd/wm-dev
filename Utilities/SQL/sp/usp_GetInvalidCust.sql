USE [NetflowResultsData]
GO

/****** Object:  StoredProcedure [dbo].[usp_GetInvalidCust]    Script Date: 20/10/2017 10:38:40 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_GetInvalidCust] @Month nvarchar(30)
AS
DECLARE @cmd_genReport nvarchar(max)
SET @cmd_genReport = '
SELECT DISTINCT D.CustomerNumber, D.[Customer Name], C.[Machine Name], ROUND(CAST(SUM(E.inbytes) AS Float)/1073741824,5) AS total_inGBytes, ROUND(CAST(SUM(E.outbytes) AS Float)/1073741824,5) AS total_outGBytes, (ROUND(CAST(SUM(E.inbytes) AS Float)/1073741824,5) + ROUND(CAST(SUM(E.outbytes) AS Float)/1073741824,5)) AS total_GBytes
FROM assetregisterSQL.dbo.IPAddressesNew A
inner join assetregisterSQL.dbo.Interfaces B on A.InterfaceID = B.InterfaceID
inner join assetregisterSQL.dbo.Machines C on C.MachineID = B.MachineID
inner join assetregisterSQL.dbo.Customers D on D.CustomerNumber = C.CustomerNumber
inner join NetflowResultsData.dbo.' + @Month + ' E on A.IPAddress = E.ipaddr AND C.contract_num_sap IS NULL AND D.[Customer Name] NOT LIKE ''ICO%''
GROUP BY D.CustomerNumber, D.[Customer Name], C.[Machine Name]
ORDER BY total_inGBytes DESC;'
EXEC sp_executesql @cmd_genReport
GO

