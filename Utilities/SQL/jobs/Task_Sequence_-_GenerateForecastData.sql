--SQL Job to automatically generate forecast usage stats and store within a table

--Start the main process--
BEGIN
	BEGIN TRY
		-- Declare the variable to be used.--
		DECLARE @ipaddr varchar(50);
		DECLARE @dateTime datetime;
		DECLARE @cmd_createTable varchar(255);
		DECLARE @cmd_fillOutTable nvarchar(max);
		DECLARE @cmd_insertTable nvarchar(max);
		DECLARE @t1 DATETIME;
		DECLARE @t2 DATETIME;
		DECLARE @tdiff varchar(100);
		DECLARE @Table_Name nvarchar(50);
		DECLARE @createTable nvarchar(max);
		DECLARE @removeTable nvarchar(max);
		DECLARE @inTable nvarchar(50);
		DECLARE @outTable nvarchar(50);
		DECLARE @setDate nvarchar(50);
		DECLARE @monthName nvarchar(50);
		DECLARE @year nvarchar(50);
		DECLARE @DaysInMonth bigint

		SET @setDate = GETDATE();

		SET @monthName = DATENAME(month, @setDate)
		SET @year = DATENAME(year, @setDate)
		SET @Table_Name = @monthName + '_' + @year;
		SET @DaysInMonth = (DAY(EOMONTH(@setDate)))

		SET @removeTable = 'IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[dbo].[qs_ForecastUsage]'') AND type in (N''U''))
		BEGIN
			DROP TABLE qs_ForecastUsage;
		END'
		EXEC (@removeTable)

		SET @createTable = 'IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[dbo].[qs_ForecastUsage]'') AND type in (N''U''))
		BEGIN
			CREATE TABLE [NetflowResultsData].[dbo].[qs_ForecastUsage] (Id INT NOT NULL PRIMARY KEY IDENTITY(1,1), averageDaily_inGBytes bigint, averageDaily_outGBytes bigint, MonthForecast bigint);
		END'
		EXEC (@createTable)

		SET @t1 = GETDATE();

		SET @cmd_fillOutTable = 'SELECT AVG(AverageUsage.total_inGBytes) AS averageDaily_inGBytes, AVG(AverageUsage.total_outGBytes) AS averageDaily_outGBytes, (AVG(AverageUsage.total_inGBytes) + AVG(AverageUsage.total_outGBytes))*' + CAST(@DaysinMonth AS nvarchar(30)) + ' AS MonthForecast
								FROM
								(SELECT DISTINCT DailyUsage.dt, ROUND(CAST(SUM(DailyUsage.inbytes) AS Float)/1073741824,2) AS total_inGBytes, ROUND(CAST(SUM(DailyUsage.outbytes) AS Float)/1073741824,2) AS total_outGBytes
								FROM
									(SELECT	DISTINCT (FORMAT(dt, ''yyyy-MM-dd'')) AS dt, inbytes,	outbytes
									FROM NetflowResultsData.dbo.' + @Table_Name + '
									GROUP BY dt, inbytes, outbytes) DailyUsage
								GROUP BY dt) AverageUsage;';

		INSERT INTO qs_ForecastUsage EXEC (@cmd_fillOutTable);

		SET @t2 = GETDATE();
		SET @tdiff = DATEDIFF(MINUTE,@t1,@t2);
		PRINT 'Time elapsed - ' + @tdiff + ' minutes.';
	END TRY
	BEGIN CATCH  
		SELECT   
			ERROR_NUMBER() AS ErrorNumber  
			,ERROR_MESSAGE() AS ErrorMessage;
			DROP TABLE qs_ForecastUsage;
	END CATCH
END;