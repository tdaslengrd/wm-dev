﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System;
using System.ComponentModel.DataAnnotations;
using TDServicesApp.Models.AssetRegObjects;

namespace TDServicesApp.Models
{
    public class SearchCustomerDataModel
    {
        //[Required]
        //[StringLength(100, ErrorMessage = "It must be 4 or 5 digits", MinimumLength = 4)]
        public string CustomerNumber { get; set; }
        [Required]
        public string SearchPhrase { get; set; }
        public List<_Customer> Customers { get; set; }


        public SearchCustomerDataModel()
        {
            Customers = new List<_Customer>();
        }
    }
    public class SearchCustomerModel
    {
        [Required(ErrorMessage = "Please type in the search box")]
        public string SearchPhrase { get; set; }
        public string CustomerNumber { get; set; }
        public string CustomerName { get; set; }
        public List<_Customer> Customers { get; set; }

        public Boolean IsFirstRun { get; set; }

        public SearchCustomerModel() { }

        public SearchCustomerModel(Boolean isFirstRun = true)
        {
            IsFirstRun = isFirstRun;
        }
        public bool NoCustomers { get; set; }
    }
    public class SearchDeleteMachineModel
    {
        [Required(ErrorMessage = "Please type in the search box")]
        public int SearchPhrase { get; set; }
        public int MachineID { get; set; }
        public string MachineName { get; set; }
        public string Type { get; set; }
        public string Cage { get; set; }
        public Boolean IsFirstRun { get; set; }

        public SearchDeleteMachineModel() { }
        public SearchDeleteMachineModel(Boolean isFirstRun = true)
        {
            IsFirstRun = isFirstRun;
        }
    }
    public class SearchMachineModel
    {
        [Required(ErrorMessage = "Please type in the search box")]
        public string SearchPhrase { get; set; }
        public string MachineName { get; set; }
        public int MachineID { get; set; }
        public Boolean IsFirstRun { get; set; }
        public List<MachinList> Machines { get; set; }
        public SearchMachineModel()
        {

        }

        public SearchMachineModel(Boolean isFirstRun = true)
        {
            IsFirstRun = isFirstRun;
        }
    }
}