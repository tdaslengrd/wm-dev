﻿using System.Web.Mvc;

namespace TDServicesApp.Controllers
{
    //public class HomeController : Controller
    public class HomeController : GlobalController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "General Application Information";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Contact us...";

            return View();
        }

        public ActionResult ChangeLog()
        {
            ViewBag.Message = "Development changelog tracking page";

            return View();
        }
    }
}