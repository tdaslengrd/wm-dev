﻿using System; // For system functions like Console.
using System.Collections.Generic; // For generic collections like List
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using TDServicesApp.Models;
using System.Diagnostics;
using System.Data.SqlClient;      // For the database connections and objects
using System.Threading.Tasks;
using System.Configuration;
using System.IO;
using Newtonsoft.Json;
using PagedList;

namespace TDServicesApp.Controllers
{
    public class LayoutInjecterAttribute : ActionFilterAttribute
    {
        private readonly string _masterName;
        public LayoutInjecterAttribute(string masterName)
        {
            _masterName = masterName;
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
            var result = filterContext.Result as ViewResult;
            if (result != null)
            {
                result.MasterName = _masterName;
            }
        }
    }
    //public class DataMeteringController : Controller
    [Authorize(Roles = "Admin,Reseller,Customer")]
    public class DataMeteringController : GlobalController
    {
        // GET: DataMetering
        [LayoutInjecter("_LayoutSideMenu")]
        public ActionResult Index()
        {
            int WAN_Bandwidth = 45; //This is in MB/sec
            string tableName = DateTime.Now.ToString("MMMM");
            ViewBag.tableName = tableName;

            //Average throughput usage
            DataTable GetBandwidthUsage = new DataTable();
                try
                {
                    string query = "select * from qs_BandwidthUsage";
                    SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["NetflowDataConnectionString"].ConnectionString);
                    SqlCommand cmd = new SqlCommand(query, conn);
                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(GetBandwidthUsage);
                    conn.Close();
                    da.Dispose();
                if (GetBandwidthUsage.Rows.Count > 0)
                    {
                        DataRow row = GetBandwidthUsage.Rows[0];

                        double average_inMbps = Convert.ToInt32(row["average_inMbps"]);
                        ViewBag.average_inMbps = average_inMbps;
                        double average_outMbps = Convert.ToInt32(row["average_outMbps"]);
                        ViewBag.average_outMbps = average_outMbps;
                        double total_Mbps = average_inMbps + average_outMbps;
                        ViewBag.total_Mbps = total_Mbps;
                        ViewBag.WAN_Bandwidth = WAN_Bandwidth;
                        double CapacityAvaliable = ((double)total_Mbps / (double)WAN_Bandwidth)*100;
                        CapacityAvaliable = (100 - CapacityAvaliable);
                        ViewBag.CapacityAvaliable = Math.Round(CapacityAvaliable);
                    }
                }
                catch
                {
                    ViewBag.average_inMbps = 0;
                    ViewBag.average_outMbps = 0;
                }

            //Average daily and forecast usage
            DataTable GetForecastUsage = new DataTable();
                try
                {
                    string query = "select * from qs_ForecastUsage";
                    SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["NetflowDataConnectionString"].ConnectionString);
                    SqlCommand cmd = new SqlCommand(query, conn);
                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(GetForecastUsage);
                    conn.Close();
                    da.Dispose();
                    if (GetForecastUsage.Rows.Count > 0)
                    {
                        DataRow row = GetForecastUsage.Rows[0];
                        ViewBag.MonthForecast = row["MonthForecast"].ToString();
                        ViewBag.averageDaily_inGBytes = row["averageDaily_outGBytes"].ToString();
                        ViewBag.averageDaily_outGBytes = row["averageDaily_inGBytes"].ToString();
                    }
                }
                catch
                {
                    ViewBag.MonthForecast = "No data for this month!";
                }
            return View();
        }

        public List<CustomerModel> GetCustomers(string tableName)
        {
            List<CustomerModel> objList = new List<CustomerModel>();

            //Daily customer usage
            DataTable GetCustomers = new DataTable();
            using (var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["NetflowDataConnectionString"].ConnectionString))
            using (var cmd = new SqlCommand("usp_GetTotalUsageCust", con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Month", tableName);
                da.Fill(GetCustomers);
            }

            objList.Add(new CustomerModel { Id = 0, CustomerName = "Select Customer" });

            int listId = 0;
            foreach (DataRow row in GetCustomers.Rows)
            {
                listId++;
                objList.Add(new CustomerModel { Id = listId, CustomerName = row["Customer Name"].ToString() });
            }

            return objList;
        } //Not tested as yet, can be used as initial customer filter for DataUsageReport. Uses CustomerModel class from DataMeteringModel.

        [LayoutInjecter("_LayoutSideMenu")]
        public ActionResult UsageCustomerDaily(string tableName, string customerName)
        {
            Stopwatch maintimer = new Stopwatch();
            maintimer.Start();

            if (!Request.Path.ToString().Contains("UsageCustomerDaily"))
            {
                return View("UsageCustomer");
            }

            //Initialise a new instance of an object for each model class
            var model = new DataMeteringModel();

            //Reassociate the model.CustomerName so we can use it in the View
            model.CustomerName = customerName;
            model.tableName = tableName;

            //Check if the table exists, else exit
            string tableCheck = "";
            using (var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["NetflowDataConnectionString"].ConnectionString))
            using (var cmd = new SqlCommand("usp_TableCheck", con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@tableName", tableName);
                con.Open();
                var result = cmd.ExecuteScalar();
                con.Close();
                tableCheck = result.ToString();
            }
            if (tableCheck == "No")
            {
                ViewBag.Error = "Table does not exist!";
                return View();
            }

            //Daily customer usage
            DataTable GetTotalUsageCust = new DataTable();
            using (var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["NetflowDataConnectionString"].ConnectionString))
            using (var cmd = new SqlCommand("usp_GetTotalUsageCust", con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Month", tableName);
                cmd.Parameters.AddWithValue("@CustomerName", customerName);
                da.Fill(GetTotalUsageCust);
            }
            model.totalUsageCustPerDay = GetTotalUsageCust;

            //Device based usage for customer
            DataTable totalUsagePerDevice = new DataTable();
            using (var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["NetflowDataConnectionString"].ConnectionString))
            using (var cmd = new SqlCommand("usp_GetDeviceTotalCust", con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Month", tableName);
                cmd.Parameters.AddWithValue("@CustomerName", customerName);
                da.Fill(totalUsagePerDevice);
            }
            model.totalUsagePerDevice = totalUsagePerDevice;

            //The portion of code is used to separate and serialise the data into JSON for CanvasJS
            DataTable inboundData = new DataTable();
            inboundData = GetTotalUsageCust.Copy();
            inboundData.Columns.Add("FormattedDT");
            foreach (DataRow row in inboundData.Rows)
            {
                row["FormattedDT"] = (Convert.ToDateTime(row["dt"]).ToString("yyyy, MM, dd, HH"));
            }
            inboundData.Columns.Remove("dt");
            inboundData.Columns.Remove("total_outGBytes");
            ViewBag.DataPoints1 = JsonConvert.SerializeObject(inboundData);
            DataTable outboundData = new DataTable();
            outboundData = GetTotalUsageCust.Copy();
            outboundData.Columns.Add("FormattedDT");
            foreach (DataRow row in outboundData.Rows)
            {
                row["FormattedDT"] = (Convert.ToDateTime(row["dt"]).ToString("yyyy, MM, dd, HH"));
            }
            outboundData.Columns.Remove("dt");
            outboundData.Columns.Remove("total_inGBytes");
            ViewBag.DataPoints2 = JsonConvert.SerializeObject(outboundData);

            maintimer.Stop();
            model.maintimerduration = maintimer.Elapsed.Milliseconds;

            return View(model);
        }

        [LayoutInjecter("_LayoutSideMenu")]
        public ActionResult UsageDeviceDaily(string tableName, string deviceName, string customerName)
        {
            Stopwatch maintimer = new Stopwatch();
            maintimer.Start();

            //if (!Request.Path.ToString().Contains("UsageCustomerDaily"))
            //{
            //    return View("UsageCustomer");
            //}

            //Initialise a new instance of an object for each model class
            var model = new DataMeteringModel();

            //Reassociate the model.CustomerName so we can use it in the View
            model.CustomerName = customerName;
            model.DeviceName = deviceName;

            //Check if the table exists, else exit
            string tableCheck = "";
            using (var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["NetflowDataConnectionString"].ConnectionString))
            using (var cmd = new SqlCommand("usp_TableCheck", con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@tableName", tableName);
                con.Open();
                var result = cmd.ExecuteScalar();
                con.Close();
                tableCheck = result.ToString();
            }
            if (tableCheck == "No")
            {
                ViewBag.Error = "Table does not exist!";
                return View();
            }

            //Daily device usage
            DataTable GetDeviceDailyUsage = new DataTable();
            using (var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["NetflowDataConnectionString"].ConnectionString))
            using (var cmd = new SqlCommand("usp_GetDeviceDailyUsage", con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@tableName", tableName);
                cmd.Parameters.AddWithValue("@deviceName", deviceName);
                da.Fill(GetDeviceDailyUsage);
            }
            model.deviceDailyUsage = GetDeviceDailyUsage;

            //The portion of code is used to separate and serialise the data into JSON for CanvasJS
            DataTable inboundData = new DataTable();
            inboundData = GetDeviceDailyUsage.Copy();
            inboundData.Columns.Add("FormattedDT");
            foreach (DataRow row in inboundData.Rows)
            {
                row["FormattedDT"] = (Convert.ToDateTime(row["dt"]).ToString("yyyy, MM, dd, HH"));
            }
            inboundData.Columns.Remove("dt");
            inboundData.Columns.Remove("total_outGBytes");
            ViewBag.DataPoints1 = JsonConvert.SerializeObject(inboundData);
            DataTable outboundData = new DataTable();
            outboundData = GetDeviceDailyUsage.Copy();
            outboundData.Columns.Add("FormattedDT");
            foreach (DataRow row in outboundData.Rows)
            {
                row["FormattedDT"] = (Convert.ToDateTime(row["dt"]).ToString("yyyy, MM, dd, HH"));
            }
            outboundData.Columns.Remove("dt");
            outboundData.Columns.Remove("total_inGBytes");
            ViewBag.DataPoints2 = JsonConvert.SerializeObject(outboundData);

            maintimer.Stop();
            model.maintimerduration = maintimer.Elapsed.Milliseconds;

            return View(model);
        }

        [LayoutInjecter("_LayoutSideMenu")]
        public ActionResult DataUsageReport()
        {
            return View();
        }

        [HttpPost]
        [LayoutInjecter("_LayoutSideMenu")]
        public ActionResult DataUsageReport(string Month, string Year)
        {
            Stopwatch maintimer = new Stopwatch();
            maintimer.Start();

            //Initialise a new instance of an object for each model class
            var model = new DataMeteringModel();
            DataTable GetBillingReport = new DataTable();
            DataTable GetTotalUsage = new DataTable();

            //Format the received data to generate the tableName
            string tableName = Month + "_" + Year;
            model.tableName = tableName;

            //Lets check if the received month and year are current, if they are we will look at the quick stats tables for information otherwise we will continue on
            string currentMonthName = DateTime.Now.ToString("MMMM");
            string currentYear = DateTime.Now.Year.ToString();
            if (Month == currentMonthName && Year == currentYear)
            {
                //Lets get the information from the quick stats tables
                //Total data transferred
                string query = "select * from qs_BillingReport";
                SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["NetflowDataConnectionString"].ConnectionString);
                SqlCommand cmd = new SqlCommand(query, conn);
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(GetBillingReport);
                conn.Close();
                da.Dispose();
            }
            else
            {
                //We need to get database information not for the current month
                //Check if the table exists, else exit
                string tableCheck = "";
                using (var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["NetflowDataConnectionString"].ConnectionString))
                using (var cmd = new SqlCommand("usp_TableCheck", con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@tableName", tableName);
                    con.Open();
                    var result = cmd.ExecuteScalar();
                    con.Close();
                    tableCheck = result.ToString();
                }
                if (tableCheck == "No")
                {
                    ViewBag.Error = "Table does not exist!";
                    return View();
                }

                //Get general billing report
                using (var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["NetflowDataConnectionString"].ConnectionString))
                using (var cmd = new SqlCommand("usp_GetBillingReport", con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Month", tableName);
                    da.Fill(GetBillingReport);
                }
                //model.generalUsageReport = GetBillingReport;
            }

            //Format UTC data/time in dt column to Australia/Sydney timezone - This is a memory hog, so the context of the function is within SQL
            //var offset = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow);
            //int offsetHours = offset.Hours;
            //foreach (DataRow row in GetBillingReport.Rows)
            //{
            //    DateTime odt = row.Field<DateTime>("dt").AddHours(offsetHours);
            //    row["dt"] = odt;
            //}
            //model.generalUsageReport = GetBillingReport;

            //Total data transferred
            using (var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["NetflowDataConnectionString"].ConnectionString))
            using (var cmd = new SqlCommand("usp_GetTotalUsage", con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Month", tableName);
                da.Fill(GetTotalUsage);
            }

            //Get the details for data usage and save to model data
            if (GetTotalUsage.Rows.Count > 0)
            {
                DataRow row = GetTotalUsage.Rows[0];
                model.totalindata = row["total_inTBytes"].ToString();
                model.totaloutdata = row["total_outTBytes"].ToString();
                model.totaldata = row["total_TBytes"].ToString();
            }

            //Calculate the total number of records in the final table
            model.netflowrecords = GetBillingReport.Rows.Count;

            //Get total number of days in report
            model.resultsTableDaysCount = (GetBillingReport.AsEnumerable().Select(x => x.Field<DateTime>("dt")).Distinct()).Count();

            maintimer.Stop();
            model.maintimerduration = maintimer.Elapsed.Milliseconds;

            //return View(model);
            return RedirectToAction("DataUsageReportView", model);
        }

        [LayoutInjecter("_LayoutSideMenu")]
        public ActionResult DataUsageReportView(DataMeteringModel passedmodel, string sortOrder, string currentFilter, string searchString, int? page, string tableName)
        {
            var model = new DataMeteringModel();
            if (passedmodel.tableName != null) //This is when the refferer is from DataUsageReport
            {
                model = passedmodel;
                tableName = model.tableName;
            }
            else if (ViewBag.TableName != null) //This is for when the referrer is DataUsageeReportView
            {
                ViewBag.TableName = tableName;
            }
            else
            {
                return RedirectToAction("DataUsageReport"); //This means there is no table name, so we forward the user to the select year and month view for DataUsageReport.
            }

            //string tableName = "January_2017";
            //Get general billing report
            DataTable GetBillingReport = new DataTable();
            using (var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["NetflowDataConnectionString"].ConnectionString))
            using (var cmd = new SqlCommand("usp_GetBillingReport", con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Month", tableName);
                da.Fill(GetBillingReport);
            }

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "Customer Name" : "";
            ViewBag.TableName = tableName;

            var students = from s in GetBillingReport.AsEnumerable()
                           select s;

            if (!String.IsNullOrEmpty(searchString))
            {
                students = students.Where(s => s.Field<string>("Customer Name").Contains(searchString) || s.Field<string>("Machine Name").Contains(searchString));
            }

            switch (sortOrder)
            {
                case "Customer Name":
                    students = students.OrderByDescending(s => s.Field<string>("Customer Name"));
                    break;
            }

            int pageSize = 25;
            int pageNumber = (page ?? 1);

            model.mypagedList = students.ToPagedList(pageNumber, pageSize);

            return View(model);
        }

        [LayoutInjecter("_LayoutSideMenu")]
        public ActionResult UsageDaily()
        {
            return View();
        }

        private DataMeteringModel UsageDaily(string Month, string Year)
        {
            // The below should contain your generate report code
            Stopwatch maintimer = new Stopwatch();
            maintimer.Start();

            //Initialise a new instance of an object for each model class
            var model = new DataMeteringModel();

            //Format the received data to generate the tableName
            string tableName = Month + "_" + Year;

            //Check if the table exists, else exit
            string tableCheck = "";
            using (var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["NetflowDataConnectionString"].ConnectionString))
            using (var cmd = new SqlCommand("usp_TableCheck", con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@tableName", tableName);
                con.Open();
                var result = cmd.ExecuteScalar();
                con.Close();
                tableCheck = result.ToString();
            }
            if (tableCheck == "No")
            {
                model.error_message = "Table does not exist!";
                return model;
            }

            DataTable GetTotalUsagePerDay = new DataTable();

            //Lets check if the received month and year are current, if they are we will look at the quick stats tables for information otherwise we will continue on
            string currentMonthName = DateTime.Now.ToString("MMMM");
            string currentYear = DateTime.Now.Year.ToString();
            if (Month == currentMonthName && Year == currentYear)
            {
                //Lets get the information from the quick stats tables
                //Total data transferred
                string query = "select * from qs_TotalUsagePerDay";
                SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["NetflowDataConnectionString"].ConnectionString);
                SqlCommand cmd = new SqlCommand(query, conn);
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(GetTotalUsagePerDay);
                conn.Close();
                da.Dispose();
            }
            else
            {
                //We need to get database information not for the current month
                //Get total usage per day
                using (var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["NetflowDataConnectionString"].ConnectionString))
                using (var cmd = new SqlCommand("usp_GetTotalUsagePerDay", con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandTimeout = 300;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Month", tableName);
                    da.Fill(GetTotalUsagePerDay);
                }
            }

            model.totalUsagePerDay = GetTotalUsagePerDay;

            //The portion of code is used to separate and serialise the data into JSON for CanvasJS
            DataTable inboundData = new DataTable();
            inboundData = GetTotalUsagePerDay.Copy();
            inboundData.Columns.Add("FormattedDT");
            foreach (DataRow row in inboundData.Rows)
            {
                row["FormattedDT"] = (Convert.ToDateTime(row["dt"]).ToString("yyyy, MM, dd"));
            }
            inboundData.Columns.Remove("dt");
            inboundData.Columns.Remove("total_outGBytes");
            model.DataPoints1 = JsonConvert.SerializeObject(inboundData);

            DataTable outboundData = new DataTable();
            outboundData = GetTotalUsagePerDay.Copy();
            outboundData.Columns.Add("FormattedDT");
            foreach (DataRow row in outboundData.Rows)
            {
                row["FormattedDT"] = (Convert.ToDateTime(row["dt"]).ToString("yyyy, MM, dd"));
            }
            outboundData.Columns.Remove("dt");
            outboundData.Columns.Remove("total_inGBytes");
            model.DataPoints2 = JsonConvert.SerializeObject(outboundData);

            maintimer.Stop();
            model.maintimerduration = maintimer.Elapsed.Milliseconds;

            return model;
        }

        [HttpPost]
        public async Task<ActionResult> UsageDailyReportGen(string Month, string Year)
        {
            //Initialise a new instance of an object for each model class
            var result = new DataMeteringModel();

            result = await Task.FromResult(UsageDaily(Month, Year));

            return PartialView("_UsageDailyResults", result);
        }

        [LayoutInjecter("_LayoutSideMenu")]
        public ActionResult UsageCustomer()
        {
            return View();
        }

        [HttpPost]
        [LayoutInjecter("_LayoutSideMenuNoLoad")] //There are issues running MVC Ajax and JQuery Unobtrusive together, so we use a separate Layout without JQuery Unobtrusive included.
        public ActionResult UsageCustomer(string Month, string Year)
        {
            Stopwatch maintimer = new Stopwatch();
            maintimer.Start();

            //Initialise a new instance of an object for each model class
            var model = new DataMeteringModel();

            //Format the received data to generate the tableName
            string tableName = Month + "_" + Year;
            model.tableName = tableName;

            //Check if the table exists, else exit
            string tableCheck = "";
            using (var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["NetflowDataConnectionString"].ConnectionString))
            using (var cmd = new SqlCommand("usp_TableCheck", con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@tableName", tableName);
                con.Open();
                var result = cmd.ExecuteScalar();
                con.Close();
                tableCheck = result.ToString();
            }
            if (tableCheck == "No")
            {
                ViewBag.Error = "Table does not exist!";
                return View();
            }

            //Generate summary stats to show usage per customer, this routine may be better suited as a SQL function rather then calculated here at runtime.
            DataTable GetTotalUsagePerCust = new DataTable();
            using (var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["NetflowDataConnectionString"].ConnectionString))
            using (var cmd = new SqlCommand("usp_GetTotalUsagePerCust", con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Month", tableName);
                da.Fill(GetTotalUsagePerCust);
            }
            model.totalUsagePerCust = GetTotalUsagePerCust;
            //Get total customers for report stats
            model.resultsTableCustCount = GetTotalUsagePerCust.Rows.Count;

            maintimer.Stop();
            model.maintimerduration = maintimer.Elapsed.Milliseconds;

            return View(model);
        }

        [LayoutInjecter("_LayoutSideMenu")]
        public ActionResult InvalidCustomer()
        {
            return View();
        }

        [HttpPost]
        [LayoutInjecter("_LayoutSideMenuNoLoad")] //There are issues running MVC Ajax and JQuery Unobtrusive together, so we use a separate Layout without JQuery Unobtrusive included.
        public ActionResult InvalidCustomer(string Month, string Year)
        {
            Stopwatch maintimer = new Stopwatch();
            maintimer.Start();

            //Initialise a new instance of an object for each model class
            var model = new DataMeteringModel();

            //Format the received data to generate the tableName
            string tableName = Month + "_" + Year;
            model.tableName = tableName;

            //Check if the table exists, else exit
            string tableCheck = "";
            using (var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["NetflowDataConnectionString"].ConnectionString))
            using (var cmd = new SqlCommand("usp_TableCheck", con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@tableName", tableName);
                con.Open();
                var result = cmd.ExecuteScalar();
                con.Close();
                tableCheck = result.ToString();
            }
            if (tableCheck == "No")
            {
                ViewBag.Error = "Table does not exist!";
                return View();
            }

            DataTable GetTotalUsagePerCust = new DataTable();

            //Lets check if the received month and year are current, if they are we will look at the quick stats tables for information otherwise we will continue on
            string currentMonthName = DateTime.Now.ToString("MMMM");
            string currentYear = DateTime.Now.Year.ToString();
            if (Month == currentMonthName && Year == currentYear)
            {
                //Lets get the information from the quick stats tables
                //Total data transferred
                string query = "select * from qs_InvalidCust";
                SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["NetflowDataConnectionString"].ConnectionString);
                SqlCommand cmd = new SqlCommand(query, conn);
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(GetTotalUsagePerCust);
                conn.Close();
                da.Dispose();
            }
            else
            {
                //We need to get database information not for the current month
                //Generate summary stats to show usage per customer, this routine may be better suited as a SQL function rather then calculated here at runtime.
                using (var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["NetflowDataConnectionString"].ConnectionString))
                using (var cmd = new SqlCommand("usp_GetInvalidCust", con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandTimeout = 300;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Month", tableName);
                    da.Fill(GetTotalUsagePerCust);
                }
            }
            model.totalUsagePerCust = GetTotalUsagePerCust;

            maintimer.Stop();
            model.maintimerduration = maintimer.Elapsed.Milliseconds;

            return View(model);
        }

        [LayoutInjecter("_LayoutSideMenu")]
        public ActionResult DataUsageSAPExport()
        {
            return View();
        }

        [HttpPost]
        [LayoutInjecter("_LayoutSideMenu")]
        public ActionResult DataUsageSAPExport(string Month, string Year)
        {
            //Format the received data to generate the tableName
            string tableName = Month + "_" + Year;

            //Check if the table exists, else exit
            string tableCheck = "";
            using (var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["NetflowDataConnectionString"].ConnectionString))
            using (var cmd = new SqlCommand("usp_TableCheck", con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@tableName", tableName);
                con.Open();
                var result = cmd.ExecuteScalar();
                con.Close();
                tableCheck = result.ToString();
            }
            if(tableCheck == "No")
            {
                ViewBag.Error = "Table does not exist!";
                return View();
            }

            //Generate summary stats to show usage per customer, this routine may be better suited as a SQL function rather then calculated here at runtime.
            DataTable GetBillingReport = new DataTable();
            using (var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["NetflowDataConnectionString"].ConnectionString))
            using (var cmd = new SqlCommand("usp_GetBillingReportSAP", con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Month", tableName);
                GetBillingReport.Columns.Add("Date", typeof(String));
                da.Fill(GetBillingReport);
            }

            //Add 2 columns, the first one used for a SAP Code, and the second for the Output field
            GetBillingReport.Columns.Add("status", typeof(String)).SetOrdinal(0);
            GetBillingReport.Columns.Add("Message", typeof(String)).SetOrdinal(5);

            //Set the value of the new colums based on data in datatable
            foreach (DataRow row in GetBillingReport.Rows)
            {
                if (row["Contract"].ToString() != "")
                {
                    row["status"] = "'@08@";
                    row["Message"] = "Success";
                    row["Contract"] = row["Contract"].ToString().PadLeft(10, '0');
                    row["Date"] = (Convert.ToDateTime(row["Date"]).ToString("dd/MM/yyyy"));
                }
                else
                {
                    row["status"] = "'@0A@";
                    row["Message"] = "Contract is blank";
                    row["Date"] = (Convert.ToDateTime(row["Date"]).ToString("dd/MM/yyyy"));
                }
            }

            //Initialise sequence to generate CSV file
            byte[] outputBuffer = null;
            using (MemoryStream tempStream = new MemoryStream())
            {
                using (StreamWriter writer = new StreamWriter(tempStream))
                {
                    Rfc4180Writer.WriteDataTable(GetBillingReport, writer, true);
                }
                outputBuffer = tempStream.ToArray();
            }

            //Return binary stream to View
            return File(outputBuffer, "text/csv", "export.csv");
        }

        [LayoutInjecter("_LayoutSideMenu")]
        public ActionResult NetflowDBStats()
        {
            var model = new NetflowDBStatsModel();

            try
            {
                //Get data from SQL for reported data
                DataTable netflowTable = new DataTable();
                using (var sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["NetflowDataConnectionString"].ConnectionString))
                {
                    // Build your query (an example insert)
                    var query = @"
                            SELECT
                                D.name AS 'DB Name',
                                F.Name AS 'File Type',
                                CAST((F.size*8)/1024 AS VARCHAR(26)) + ' GBytes' AS 'Size in GBytes'
                            FROM 
                                sys.master_files F
                                INNER JOIN sys.databases D ON D.database_id = F.database_id
                            WHERE
	                            D.name != 'master' AND
	                            D.name != 'model' AND
	                            D.name != 'msdb' AND
	                            D.name != 'tempdb'
                            ORDER BY
                                D.name DESC;
                            ";
                    SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
                    sqlConnection.Open();
                    SqlDataReader reader = sqlCommand.ExecuteReader();
                    netflowTable.Load(reader);
                    sqlConnection.Close();
                }
                model.sqltablesize = netflowTable;
            }
            catch (Exception ex)
            {
                // log4net custom variables for log output
                log4net.GlobalContext.Properties["url"] = ex.TargetSite.ToString();

                // Use error or higher to send exception to logger
                log.Error(ex.Message.ToString());

                model.queryErrorMessage = ex.ToString();
            }

            return View(model);
        }

        public static class Rfc4180Writer
        {
            public static void WriteDataTable(DataTable sourceTable, TextWriter writer, bool includeHeaders)
            {
                if (includeHeaders)
                {
                    IEnumerable<String> headerValues = sourceTable.Columns
                        .OfType<DataColumn>()
                        .Select(column => QuoteValue(column.ColumnName));

                    writer.WriteLine(String.Join(",", headerValues));
                }

                IEnumerable<String> items = null;

                foreach (DataRow row in sourceTable.Rows)
                {
                    items = row.ItemArray.Select(o => QuoteValue(o.ToString()));
                    writer.WriteLine(String.Join(",", items));
                }

                writer.Flush();
            }

            private static string QuoteValue(string value)
            {
                return String.Concat("\"",
                value.Replace("\"", "\"\""), "\"");
            }
        }
    }
}