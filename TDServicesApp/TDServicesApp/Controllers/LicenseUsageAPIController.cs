﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TDServicesApp.Models;

namespace TDServicesApp.Controllers
{
    public class LicenseUsageAPIController : ApiController
    {
        // GET api/LicenseUsageAPI/Test
        [Route("api/LicenseUsageAPI/Test")]
        [HttpGet]
        public IHttpActionResult Test()
        {
            return Ok("Test sucessful!");
        }

        // GET api/LicenseUsageAPI/TestAuth
        [Route("api/LicenseUsageAPI/TestAuth")]
        [Authorize]
        [HttpGet]
        public IHttpActionResult TestAuth()
        {
            return Ok("Authorization test sucessful!");
        }

        // PUT api/LicenseUsageAPI/SubmitLicenseUsage
        [Authorize(Roles = "Admin,LicenseAuditAPI")]
        [HttpPut]
        public IHttpActionResult SubmitLicenseUsage([FromBody]LicenseUsageJSONData data)
        {
            string monthName = DateTime.Now.ToString("MMMM");
            string year = DateTime.Now.Year.ToString();
            string tableName = "lu_" + monthName + "_" + year;

            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["LicenseAuditConnectionString"].ConnectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.Text;
                    command.CommandText = "INSERT into " + tableName + " " +
                        "               (dt, Computer, Caption, Version, CPUs, Cores, TotalCores, ProductKeyWindows, SQLInstalled, SQLVersion, ProductKeySQL, RDSInstalled, InstalledRDSLicenses, UsedRDSLicenses, AvailableRDSLicenses, ExchangeInstalled, ProvisionedMailboxCount, ExchangeEdition) " +
                                        "VALUES " +
                                        "(@dt, @Computer, @Caption, @Version, @CPUs, @Cores, @TotalCores, @ProductKeyWindows, @SQLInstalled, @SQLVersion, @ProductKeySQL, @RDSInstalled, @InstalledRDSLicenses, @UsedRDSLicenses, @AvailableRDSLicenses, @ExchangeInstalled, @ProvisionedMailboxCount, @ExchangeEdition)";

                    command.Parameters.AddWithValue("@dt", DateTime.Now);
                    command.Parameters.AddWithValue("@Computer", data.Computer);
                    command.Parameters.AddWithValue("@Caption", data.Caption);
                    command.Parameters.AddWithValue("@Version", data.Version);
                    command.Parameters.AddWithValue("@CPUs", data.CPUs);
                    command.Parameters.AddWithValue("@Cores", data.Cores);
                    command.Parameters.AddWithValue("@TotalCores", data.TotalCores);
                    command.Parameters.AddWithValue("@ProductKeyWindows", data.ProductKeyWindows);
                    command.Parameters.AddWithValue("@SQLInstalled", data.SQLInstalled);
                    command.Parameters.AddWithValue("@SQLVersion", data.SQLVersion);
                    command.Parameters.AddWithValue("@ProductKeySQL", data.ProductKeySQL);
                    command.Parameters.AddWithValue("@RDSInstalled", data.RDSInstalled);
                    command.Parameters.AddWithValue("@InstalledRDSLicenses", data.InstalledRDSLicenses);
                    command.Parameters.AddWithValue("@UsedRDSLicenses", data.UsedRDSLicenses);
                    command.Parameters.AddWithValue("@AvailableRDSLicenses", data.AvailableRDSLicenses);
                    command.Parameters.AddWithValue("@ExchangeInstalled", data.ExchangeEdition);
                    command.Parameters.AddWithValue("@ProvisionedMailboxCount", data.ProvisionedMailboxCount);
                    command.Parameters.AddWithValue("@ExchangeEdition", data.ExchangeEdition);

                    try
                    {
                        connection.Open();
                        int recordsAffected = command.ExecuteNonQuery();
                        return Ok("Data submitted sucessfully.");
                    }
                    catch (SqlException e)
                    {
                        string error = e.Message;
                        return InternalServerError();
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }
    }
}
