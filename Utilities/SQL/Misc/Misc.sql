/****** Script for SelectTopNRows command from SSMS  ******/
SELECT DISTINCT [dt] FROM [NetflowResultsData].[dbo].[May_2017] ORDER BY [dt] DESC;

SELECT count(*) FROM [NetflowResultsData].[dbo].[July_2017];

SELECT count(*) from [NetflowData].[dbo].[nfd20170716] U WHERE U.src LIKE '202.191.[4-5][5-8].%' OR U.src LIKE '119.161.[3-4][2-7].%' AND U.bytes >= '5000000';
SELECT count(*) from [NetflowData].[dbo].[nfd20170716] U WHERE U.dest LIKE '202.191.[4-5][5-8].%' OR U.dest LIKE '119.161.[3-4][2-7].%' AND U.bytes >= '5000000';

SELECT
    D.name AS 'DB Name',
    F.Name AS 'File Type',
    CAST((F.size*8)/1024 AS VARCHAR(26)) + ' MB' AS 'Size in MB'
FROM 
    sys.master_files F
    INNER JOIN sys.databases D ON D.database_id = F.database_id
WHERE
	D.name != 'master' AND
	D.name != 'model' AND
	D.name != 'msdb' AND
	D.name != 'tempdb'
ORDER BY
    D.name DESC;

exec sp_spaceused

USE [NetflowData]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [Dest_Index]    Script Date: 19/07/2017 12:43:32 PM ******/
CREATE NONCLUSTERED INDEX [Dest_Index] ON [dbo].[nfd20170717]
(
	[dest] ASC
)
INCLUDE ([bytes],[datetime]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [Src_Index]    Script Date: 19/07/2017 12:43:37 PM ******/
CREATE NONCLUSTERED INDEX [Src_Index] ON [dbo].[nfd20170717]
(
	[src] ASC
)
INCLUDE ([bytes],[datetime]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

---------------------------------------------
EXEC NetflowResultsData.dbo.usp_GetBillingReportSAP @Month = 'July_2017';
EXEC NetflowResultsData.dbo.usp_GetBillingReportCust @Month = 'August_2017', @CustomerName = 'Ozsale';
EXEC NetflowResultsData.dbo.usp_GetBillingReport @Month = 'March_2017';
EXEC NetflowResultsData.dbo.usp_GetCustomers @Month = 'August_2017';
EXEC NetflowResultsData.dbo.usp_GetTotalUsage @Month = 'January_2017';
EXEC NetflowResultsData.dbo.usp_GetTotalUsageCust @Month = 'August_2017', @CustomerName = 'Ozsale';
EXEC NetflowResultsData.dbo.usp_GetTotalUsagePerCust @Month = 'January_2017';
EXEC NetflowResultsData.dbo.usp_GetTotalUsagePerDay @Month = 'January_2017';
EXEC NetflowResultsData.dbo.usp_TableCheck @tableName = 'May_2017';
EXEC NetflowResultsData.dbo.usp_GetForecastUsage;
EXEC NetflowResultsData.dbo.usp_GetBandwidthUsage @Month = 'February';
------------------------------------------------------------------------------------------------------------
USE [NetflowResultsData]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetBillingReport]    Script Date: 24/07/2017 10:53:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GetBillingReportSAP] @Month nvarchar(30)
AS
DECLARE @cmd_genReport nvarchar(max)
SET @cmd_genReport = '
SELECT c.contract_num_sap, E.dt, E.inbytes, E.outbytes
FROM assetregisterSQL.dbo.IPAddressesNew A
inner join assetregisterSQL.dbo.Interfaces B on A.InterfaceID = B.InterfaceID
inner join assetregisterSQL.dbo.Machines C on C.MachineID = B.MachineID
inner join assetregisterSQL.dbo.Customers D on D.CustomerNumber = C.CustomerNumber
inner join NetflowResultsData.dbo.' + @Month + ' E on A.IPAddress = E.ipaddr
ORDER BY E.dt'
EXEC sp_executesql @cmd_genReport
------------------------------------------------------------------------------------------------------------
SELECT DISTINCT [dt] FROM [NetflowResultsData].[dbo].[May_2017] ORDER BY [dt] DESC;

SELECT [src],[dest],[bytes],[datetime] from [NetflowData].[dbo].[nfd20170501] U WHERE U.dest LIKE '202.191.[48-55]%' OR U.dest LIKE '119.161.[32-47]%' AND U.bytes > 1000000 ORDER BY U.bytes DESC;
------------------------------------------------------------------------------------------------------------

/****** Script for SelectTopNRows command from SSMS  ******/
SELECT DISTINCT [src], SUM([Bytes])
  FROM [NetflowData].[dbo].[nfd20170511] GROUP BY [src] ORDER BY [bytes] DESC;

SELECT DISTINCT A.src, SUM(A.bytes)/1000000000000 AS total_TB
FROM [NetflowData].[dbo].[nfd20170511] A
WHERE A.src LIKE '202.191.[48-55]%' OR A.src LIKE '119.161.[32-47]%'
GROUP BY A.src
ORDER BY total_TB DESC;

SELECT DISTINCT A.dest, SUM(A.bytes)/1000000000000 AS total_TB
FROM [NetflowData].[dbo].[nfd20170511] A
WHERE A.dest LIKE '202.191.[48-55]%' OR A.dest LIKE '119.161.[32-47]%'
GROUP BY A.dest
ORDER BY total_TB DESC;

SELECT SUM(A.bytes)/1000000000000 AS total_TB
FROM [NetflowData].[dbo].[nfd20170511] A
WHERE A.src LIKE '202.191.[48-55]%' OR A.src LIKE '119.161.[32-47]%';

SELECT SUM(A.bytes)/1000000000000 AS total_TB
FROM [NetflowData].[dbo].[nfd20170511] A
WHERE A.dest LIKE '202.191.[48-55]%' OR A.dest LIKE '119.161.[32-47]%';

SELECT SUM(A.bytes)/1000000000000 AS total_TB
FROM [NetflowData].[dbo].[nfd20170511] A;


SELECT DISTINCT A.src, SUM(A.bytes) AS bytes
FROM [NetflowData].[dbo].[nfd20170511] A
WHERE A.src LIKE '202.191.[48-55]%' OR A.src LIKE '119.161.[32-47]%'
GROUP BY A.src
ORDER BY bytes DESC;

SELECT DISTINCT A.dest, SUM(A.bytes) AS bytes
FROM [NetflowData].[dbo].[nfd20170511] A
WHERE A.dest LIKE '202.191.[48-55]%' OR A.dest LIKE '119.161.[32-47]%'
GROUP BY A.dest
ORDER BY bytes DESC;

select srcdata.ipaddr, srcdata.outbytes, destdata.inbytes, srcdata.dt
from 
    --(SELECT ks, COUNT(*) AS '# Tasks' FROM Table GROUP BY ks) t1
	(SELECT DISTINCT dest AS ipaddr, SUM(bytes) AS inbytes, [datetime] AS dt
	FROM [NetflowData].[dbo].[nfd20170511]
	WHERE dest LIKE '202.191.[48-55]%' OR dest LIKE '119.161.[32-47]%'
	GROUP BY dest, [datetime]) destdata
inner join
    --(SELECT ks, COUNT(*) AS '# Late' FROM Table WHERE Age > Palt GROUP BY ks) t2
	(SELECT DISTINCT src AS ipaddr, SUM(bytes) AS outbytes, [datetime] AS dt
	FROM [NetflowData].[dbo].[nfd20170511]
	WHERE src LIKE '202.191.[48-55]%' OR src LIKE '119.161.[32-47]%'
	GROUP BY src, [datetime]) srcdata
on
    destdata.ipaddr = srcdata.ipaddr
order by
	srcdata.ipaddr DESC;


CREATE TABLE #NetflowTable2_tmp(ID int IDENTITY(1,1) NOT NULL, ipaddr varchar(50), inbytes bigint, outbytes bigint, dt datetime, UNIQUE NONCLUSTERED (ipaddr, ID));
DECLARE @cmd_fillOutTable nvarchar(max);

SET @cmd_fillOutTable = '
		select srcdata.ipaddr, srcdata.outbytes, destdata.inbytes, srcdata.dt
		from 
			(SELECT DISTINCT dest AS ipaddr, SUM(bytes) AS inbytes, [datetime] AS dt
			FROM [NetflowData].[dbo].[nfd20170511]
			WHERE dest LIKE ''202.191.[48-55]%'' OR dest LIKE ''119.161.[32-47]%''
			GROUP BY dest, [datetime]) destdata
		inner join
			(SELECT DISTINCT src AS ipaddr, SUM(bytes) AS outbytes, [datetime] AS dt
			FROM [NetflowData].[dbo].[nfd20170511]
			WHERE src LIKE ''202.191.[48-55]%'' OR src LIKE ''119.161.[32-47]%''
			GROUP BY src, [datetime]) srcdata
		on
			destdata.ipaddr = srcdata.ipaddr
		order by
			srcdata.ipaddr DESC;';

INSERT INTO #NetflowTable2_tmp EXEC (@cmd_fillOutTable);

DECLARE @yyyyMM_TableName nvarchar(50);
SET @yyyyMM_TableName = 'June_2017';

DECLARE @cmd_something nvarchar(max);

SET @cmd_something = 'INSERT INTO [NetflowResultsData].[dbo].[' + @yyyyMM_TableName + '] (ipaddr, inbytes, outbytes, dt)
SELECT T.ipaddr, T.inbytes, T.outbytes, T.dt
FROM #NetflowTable2_tmp T;';

EXEC sp_executesql @cmd_something;

DROP TABLE #NetflowTable2_tmp;
------------------------------------------------------------------
IF EXISTS (SELECT 1 
           FROM NetflowResultsData.INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_TYPE='BASE TABLE' 
           AND TABLE_NAME=June_2017) 
   SELECT 'Yes' AS IsExists ELSE SELECT 'No' AS IsExists;

----------------------------------------------------------------------------

SELECT count(*) FROM [NetflowData].[dbo].[nfd_January_2017]

SELECT DISTINCT (CONVERT(date, sTime)) FROM [NetflowData].[dbo].[nfd_January_2017]

SELECT DISTINCT (FORMAT(sTime, 'yyyy-MM-dd:HH')) AS FD FROM [NetflowData].[dbo].[nfd_January_2017] ORDER BY FD DESC;

SELECT DISTINCT
CASE
	WHEN InOutData.SourceIP IS NULL
	THEN InOutData.DestinationIP
	ELSE InOutData.SourceIP
	END AS IPAddress,
CASE
	WHEN SUM(InOutData.DesctinationBytes) IS NULL
	THEN 0
	ELSE SUM(InOutData.DesctinationBytes)
	END AS OutboundData,
CASE
	WHEN SUM(InOutData.SourceBytes) IS NULL
	THEN 0
	ELSE SUM(InOutData.SourceBytes)
	END AS InboundData,
InOutData.DT AS DTInfo
FROM
	(SELECT sIPdata.ipaddr AS SourceIP, sIPdata.outbytes AS SourceBytes, dIPdata.ipaddr AS DestinationIP, dIPdata.inbytes AS DesctinationBytes, 
	CASE WHEN sIPdata.dt IS NULL THEN dIPdata.dt ELSE sIPdata.dt END AS DT
	FROM 
		(SELECT DISTINCT sTime AS dt, dIP AS ipaddr, SUM(bytes) AS inbytes
		FROM [NetflowData].[dbo].[nfd20170217]
		WHERE dIP LIKE '202.191.[48-55]%' OR dIP LIKE '119.161.[32-47]%'
		GROUP BY dIP, sTime) dIPdata
	full join 
		(SELECT DISTINCT sTime AS dt, sIP AS ipaddr, SUM(bytes) AS outbytes
		FROM [NetflowData].[dbo].[nfd20170217]
		WHERE sIP LIKE '202.191.[48-55]%' OR sIP LIKE '119.161.[32-47]%'
		GROUP BY sIP, sTime) sIPdata
	ON dIPdata.ipaddr = sIPdata.ipaddr) InOutData
GROUP BY InOutData.SourceIP, InOutData.DestinationIP, InOutData.DT
ORDER BY InOutData.DT ASC;

SELECT
    D.name AS 'DB Name',
    F.Name AS 'File Type',
    CAST((F.size*8)/1024 AS VARCHAR(26)) + ' GBytes' AS 'Size in GBytes'
FROM 
    sys.master_files F
    INNER JOIN sys.databases D ON D.database_id = F.database_id
WHERE
	D.name != 'master' AND
	D.name != 'model' AND
	D.name != 'msdb' AND
	D.name != 'tempdb'
ORDER BY
    D.name DESC;

USE NetflowResultsData
GO
SELECT DISTINCT Test.dt AS dt, sum(Test.InBytes) AS InBytes, sum(Test.OutBytes) AS OutBytes
FROM
(
	SELECT DISTINCT(CAST(E.dt AS DATE)) AS dt, C.contract_num_sap AS Contract, sum(E.inbytes) AS InBytes, sum(E.outbytes) AS OutBytes, D.[Customer Name] AS CustomerName
	FROM assetregisterSQL.dbo.IPAddressesNew A
	inner join assetregisterSQL.dbo.Interfaces B on A.InterfaceID = B.InterfaceID
	inner join assetregisterSQL.dbo.Machines C on C.MachineID = B.MachineID
	inner join assetregisterSQL.dbo.Customers D on D.CustomerNumber = C.CustomerNumber
	inner join NetflowResultsData.dbo.July_2017 E on A.IPAddress = E.ipaddr
	WHERE C.contract_num_sap IS NOT NULL
	--GROUP BY C.contract_num_sap, dt, D.[Customer Name]
	--ORDER BY C.contract_num_sap, dt ASC
) Test
GROUP BY dt, InBytes, OutBytes

SELECT
CASE
	WHEN InOutData.outipaddr IS NULL
		THEN InOutData.inipaddr
		ELSE InOutData.outipaddr
	END
AS ipaddr,
ISNULL(InOutData.outbytes, 0) AS outbytes,
ISNULL(InOutData.inbytes, 0) AS inbytes,
CASE
	WHEN InOutData.outdt IS NULL
		THEN InOutData.indt
		ELSE InOutData.outdt
	END
AS dt
FROM
	(SELECT sIPdata2.outipaddr as outipaddr, dIPdata2.inipaddr AS inipaddr, sIPdata2.outbytes AS outbytes, dIPdata2.inbytes AS inbytes, sIPdata2.outdt AS outdt, dIPdata2.indt AS indt
	FROM
		(SELECT distinct(dIPdata.ipaddr) AS inipaddr, dIPdata.dt AS indt, sum(dIPdata.inbytes) AS inbytes
		FROM
			(SELECT DISTINCT DATEADD(HH,DATEPART(HH,sTime),CAST(CAST(sTime AS DATE) AS DATETIME)) AS dt, dIP AS ipaddr, SUM(bytes) AS inbytes
				FROM [NetflowData].[dbo].[nfd_in_August_2017]
				--WHERE dIP LIKE ''202.191.[48-55]%'' OR dIP LIKE ''119.161.[32-47]%''
				GROUP BY dIP, sTime) dIPdata
			GROUP BY ipaddr, dt) dIPdata2
		full join
		(SELECT distinct(sIPdata.ipaddr) AS outipaddr, sIPdata.dt AS outdt, sum(sIPdata.outbytes) AS outbytes
		FROM
			(SELECT DISTINCT DATEADD(HH,DATEPART(HH,sTime),CAST(CAST(sTime AS DATE) AS DATETIME)) AS dt, sIP AS ipaddr, SUM(bytes) AS outbytes
			FROM [NetflowData].[dbo].[nfd_out_August_2017]
				--WHERE sIP LIKE ''202.191.[48-55]%'' OR sIP LIKE ''119.161.[32-47]%''
				GROUP BY sIP, sTime) sIPdata
			GROUP BY ipaddr, dt) sIPdata2
		ON sIPdata2.outipaddr = dIPdata2.inipaddr AND sIPdata2.outdt = dIPdata2.indt) InOutData
	GROUP BY inipaddr, outipaddr, inbytes, outbytes, outdt, indt
ORDER BY dt

SELECT
CASE
	WHEN InOutData.outipaddr IS NULL
		THEN InOutData.inipaddr
		ELSE InOutData.outipaddr
	END
AS ipaddr,
ISNULL(InOutData.outbytes, 0) AS outbytes,
ISNULL(InOutData.inbytes, 0) AS inbytes,
CASE
	WHEN InOutData.outdt IS NULL
		THEN InOutData.indt
		ELSE InOutData.outdt
	END
AS dt
FROM
	(SELECT sIPdata2.outipaddr as outipaddr, dIPdata2.inipaddr AS inipaddr, sIPdata2.outbytes AS outbytes, dIPdata2.inbytes AS inbytes, sIPdata2.outdt AS outdt, dIPdata2.indt AS indt
	FROM
		(SELECT distinct(dIPdata.ipaddr) AS inipaddr, dIPdata.dt AS indt, sum(dIPdata.inbytes) AS inbytes
		FROM
			(SELECT DISTINCT DATEADD(HH,DATEPART(HH,sTime),CAST(CAST(sTime AS DATE) AS DATETIME)) AS dt, dIP AS ipaddr, SUM(bytes) AS inbytes
				FROM [NetflowData].[dbo].[' + @yesterdayDate + ']
				--WHERE dIP LIKE ''202.191.[48-55]%'' OR dIP LIKE ''119.161.[32-47]%''
				GROUP BY dIP, sTime) dIPdata
			GROUP BY ipaddr, dt) dIPdata2
		full join
		(SELECT distinct(sIPdata.ipaddr) AS outipaddr, sIPdata.dt AS outdt, sum(sIPdata.outbytes) AS outbytes
		FROM
			(SELECT DISTINCT DATEADD(HH,DATEPART(HH,sTime),CAST(CAST(sTime AS DATE) AS DATETIME)) AS dt, sIP AS ipaddr, SUM(bytes) AS outbytes
			FROM [NetflowData].[dbo].[' + @yesterdayDate + ']
				--WHERE sIP LIKE ''202.191.[48-55]%'' OR sIP LIKE ''119.161.[32-47]%''
				GROUP BY sIP, sTime) sIPdata
			GROUP BY ipaddr, dt) sIPdata2
		ON sIPdata2.outipaddr = dIPdata2.inipaddr AND sIPdata2.outdt = dIPdata2.indt) InOutData
	GROUP BY inipaddr, outipaddr, inbytes, outbytes, outdt, indt
ORDER BY dt

SELECT DATENAME(month, '2017-08-01') AS 'Month Name'
SELECT DATENAME(year, '2017-08-01') AS 'Year'

SELECT SUM(Bytes)/1000000000000 AS TotalInTBytes FROM [NetflowData].[dbo].[nfd_in_August_2017];

SELECT SUM(Bytes)/1000000000000 AS TotalOutTBytes FROM [NetflowData].[dbo].[nfd_out_August_2017];


SELECT DISTINCT A.IPAddress AS IPAddr, ROUND(CAST(SUM(E.inbytes) AS Float)/1073741824,5) AS total_inGBytes, ROUND(CAST(SUM(E.outbytes) AS Float)/1073741824,5) AS total_outGBytes, (ROUND(CAST(SUM(E.inbytes) AS Float)/1073741824,5) + ROUND(CAST(SUM(E.outbytes) AS Float)/1073741824,5)) AS total_GBytes
FROM assetregisterSQL.dbo.IPAddressesNew A
inner join assetregisterSQL.dbo.Interfaces B on A.InterfaceID = B.InterfaceID
inner join assetregisterSQL.dbo.Machines C on C.MachineID = B.MachineID
inner join assetregisterSQL.dbo.Customers D on D.CustomerNumber = C.CustomerNumber
inner join NetflowResultsData.dbo.August_2017 E on A.IPAddress = E.ipaddr AND D.[Customer Name] = 'Ozsale'
GROUP BY A.IPAddress
ORDER BY IPAddr ASC