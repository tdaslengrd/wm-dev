﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using TDServicesApp.Models.AssetRegObjects;

namespace TDServicesApp.Models
{
    public class GetSelectedRowDataModel
    {
        // these property names must be as same as hidden filed's name in html form
        public string SelectedCustomerNum { get; set; }
        public int SelectedMachineId { get; set; }
        public int SelectedInterfaceId { get; set; }
        public string SelectedUserName { get; set; }
        public string SelectedIPAddr { get; set; }
        public SelectedCustomer Customer { get; set; }
        public GetSelectedRowDataModel()
        {
            Customer = new SelectedCustomer();
        }
    }
    public class LoadCustomersModel
    {
        public List<_Customer> Customers { get; set; }
    }


}