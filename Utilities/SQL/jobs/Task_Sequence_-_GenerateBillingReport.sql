--SQL Job to automatically generate billing report data and store within a table

--Start the main process--
BEGIN
	BEGIN TRY
		-- Declare the variable to be used.--
		DECLARE @t1 DATETIME;
		DECLARE @t2 DATETIME;
		DECLARE @tdiff varchar(100);
		DECLARE @createTable nvarchar(max)
		DECLARE @removeTable nvarchar(max)
		DECLARE @monthName nvarchar(30)
		DECLARE @cmd_genReport nvarchar(max)
		declare @Year nvarchar(max)
		DECLARE @Table_Name nvarchar(max)
		
		SET @t1 = GETDATE();

		SET @monthName = DATENAME(month, @t1)
		SET @year = DATENAME(year, @t1)
		SET @Table_Name = @monthName + '_' + @year;
		
		SET @cmd_genReport = '
		SELECT A.IPAddress, C.MachineID, C.[Machine Name], D.CustomerNumber, D.[Customer Name], c.contract_num_sap, E.inbytes, E.outbytes, CONVERT(datetime, SWITCHOFFSET(CONVERT(datetimeoffset, E.dt), DATENAME(TzOffset, SYSDATETIMEOFFSET()))) AS dt
		FROM assetregisterSQL.dbo.IPAddressesNew A
		inner join assetregisterSQL.dbo.Interfaces B on A.InterfaceID = B.InterfaceID
		inner join assetregisterSQL.dbo.Machines C on C.MachineID = B.MachineID
		inner join assetregisterSQL.dbo.Customers D on D.CustomerNumber = C.CustomerNumber
		inner join NetflowResultsData.dbo.' + @Table_Name + ' E on A.IPAddress = E.ipaddr
		ORDER BY dt ASC'
		
		SET @removeTable = 'IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[dbo].[qs_BillingReport]'') AND type in (N''U''))
		BEGIN
			DROP TABLE qs_BillingReport;
		END'
		EXEC (@removeTable)

		SET @createTable = 'IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[dbo].[qs_BillingReport]'') AND type in (N''U''))
		BEGIN
			CREATE TABLE [NetflowResultsData].[dbo].[qs_BillingReport] (Id INT NOT NULL PRIMARY KEY IDENTITY(1,1), IPAddrsss nvarchar(20), MachineID nvarchar(10), MachineName nvarchar(max), CustomerNumber nvarchar(10), CustomerName nvarchar(max), ContractNumber nvarchar(20), BytesInbound bigint, BytedOutbound bigint, dt datetime);
		END'
		EXEC (@createTable)

		--EXEC sp_executesql @cmd_genReport
		INSERT INTO qs_BillingReport EXEC (@cmd_genReport);

		SET @t2 = GETDATE();
		SET @tdiff = DATEDIFF(MINUTE,@t1,@t2);
		PRINT 'Time elapsed - ' + @tdiff + ' minutes.';
	END TRY
	BEGIN CATCH  
		SELECT   
			ERROR_NUMBER() AS ErrorNumber  
			,ERROR_MESSAGE() AS ErrorMessage;
			DROP TABLE qs_BillingReport;
	END CATCH
END;