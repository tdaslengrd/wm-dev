﻿using System.Data; //Used for datatables
using System.Web.Mvc; //Used for SelectListItem
using System.Collections.Generic; //Used for IEnumerable objects
using PagedList;

namespace TDServicesApp.Models
{
    public class DataMeteringModel
    {
        public string error_message { get; set; }
        public int resultsTableCustCount { get; set; }
        public int resultsTableDaysCount { get; set; }
        public int netflowrecords { get; set; }
        public string totaldata { get; set; }
        public string totalindata { get; set; }
        public string totaloutdata { get; set; }
        public int maintimerduration { get; set; }
        public DataTable totalUsagePerDay { get; set; }
        public DataTable totalUsagePerCust { get; set; }
        public DataTable deviceDailyUsage { get; set; }
        public DataTable generalUsageReport { get; set; }
        public DataTable totalUsageCustPerDay { get; set; }
        public DataTable totalUsagePerDevice { get; set; }
        public string CustomerName { get; set; }

        public string DataPoints1 { get; set; }
        public string DataPoints2 { get; set; }

        public IPagedList<DataRow> mypagedList { get; set; }
        public string tableName { get; set; }
        public string DeviceName { get; internal set; }
    }

    public class NetflowDBStatsModel
    {
        public DataTable sqltablesize { get; set; }

        public string queryErrorMessage { get; set; }
    }

    public class DataUsageSAPExportModel
    {
        public IEnumerable<SelectListItem> Months { get; set; }
        public int SelectedMonth { get; set; }
        public DataTable exportData { get; set; }
    }

    public class CustomerModel
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
    } //Not currently used

}