File Description & Usage
========================

NOTES - All scripts here are designed to be run daily.

NetFlowCSV2SQL.ps1 (Run via Task Scheduler)
	Requires CsvDataReader.dll
	Requires PowerShell v4 or better
	Requires Microsoft SQL Managemente Objects
	Both the ps1 and dll file need to be kept in the same directory to function.
	Modifitions to the script in the header section need to be implmented for the script to function.
	Logging to text file is enabled by default, ensure a Logs directory exists in the root of the directory that the ps1 file is kept in.

netflowexporter.sh (Run via crontab)
	Requires rwfilter
	Requires rwstats
	Requires to be run at root level as a cronjob
	Logging data stored in nfelog.txt
	Options to export data to MySQL or FTP, default option is FTP.
	Modifitions to the script in the header section need to be implmented for the script to function.

DailyUsageReports.sql (Run via Server Agent as a Job)
	Requires Microsoft SQL 2012 or better
	Can function as a stored proceadure
	Intended usage is as a Server Agent job which runs every day after the NetFlowCSV2SQL.ps1 job
