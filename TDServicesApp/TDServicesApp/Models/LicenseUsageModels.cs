﻿using Newtonsoft.Json;
using System;

namespace TDServicesApp.Models
{
    public class LicenseUsageModel
    {

    }
    public class LicenseUsageJSONData
    {
        [JsonProperty("Computer")]
        public string Computer { get; set; }

        [JsonProperty("Caption")]
        public string Caption { get; set; }

        [JsonProperty("Version")]
        public string Version { get; set; }

        [JsonProperty("CPUs")]
        public int CPUs { get; set; }

        [JsonProperty("Cores")]
        public int Cores { get; set; }

        [JsonProperty("TotalCores")]
        public int TotalCores { get; set; }

        [JsonProperty("ProductKeyWindows")]
        public string ProductKeyWindows { get; set; }

        [JsonProperty("SQLInstalled")]
        public string SQLInstalled { get; set; }

        [JsonProperty("SQLVersion")]
        public string SQLVersion { get; set; }

        [JsonProperty("ProductKeySQL")]
        public string ProductKeySQL { get; set; }

        [JsonProperty("RDSInstalled")]
        public string RDSInstalled { get; set; }

        [JsonProperty("InstalledRDSLicenses")]
        public int InstalledRDSLicenses { get; set; }

        [JsonProperty("UsedRDSLicenses")]
        public int UsedRDSLicenses { get; set; }

        [JsonProperty("AvailableRDSLicenses")]
        public int AvailableRDSLicenses { get; set; }

        [JsonProperty("ExchangeInstalled")]
        public string ExchangeInstalled { get; set; }

        [JsonProperty("ProvisionedMailboxCount")]
        public int ProvisionedMailboxCount { get; set; }

        [JsonProperty("ExchangeEdition")]
        public string ExchangeEdition { get; set; }
    }
}