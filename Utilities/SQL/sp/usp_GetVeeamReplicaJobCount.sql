USE [VeeamUsageData]
GO

/****** Object:  StoredProcedure [dbo].[usp_GetVeeamReplicaJobCount]    Script Date: 12/12/2017 8:24:09 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GetVeeamReplicaJobCount] @tableName nvarchar(30)
AS
DECLARE @cmd_genReport nvarchar(max)
SET @cmd_genReport = '
SELECT COUNT(DISTINCT jobName) AS ''Veeam Replica Job Count'' FROM VeeamUsageData.dbo.' + @tableName + ' WHERE jobType = ''Replica'';'
EXEC sp_executesql @cmd_genReport
GO

