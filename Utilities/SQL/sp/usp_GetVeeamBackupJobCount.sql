USE [VeeamUsageData]
GO

/****** Object:  StoredProcedure [dbo].[usp_GetVeeamBackupJobCount]    Script Date: 12/12/2017 8:21:54 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GetVeeamBackupJobCount] @tableName nvarchar(30)
AS
DECLARE @cmd_genReport nvarchar(max)
SET @cmd_genReport = '
SELECT COUNT(DISTINCT jobName) AS ''Veeam Backup Job Count'' FROM VeeamUsageData.dbo.' + @tableName + ' WHERE jobType = ''Backup'';'
EXEC sp_executesql @cmd_genReport
GO

